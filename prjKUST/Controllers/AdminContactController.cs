﻿using prjKUST.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVMLib.Controllers;
using TVMLib.dl;

namespace prjKUST.Controllers
{
    [AuthorizeAdminFilter]
    public class AdminContactController : BaseController
    {
        // GET: AdminContact
        public ActionResult AdminContact()
        {
            return View(_PageContext.ServiceManager.TB_CONTACTService.Search());
        }

        public ActionResult ViewContact(string p_CONTACR_GUID)
        {
            return PartialView("_View", _PageContext.ServiceManager.TB_CONTACTService.SearchByGUID(p_CONTACR_GUID));
        }

        public ActionResult Search(string p_KeyWord)
        {
            List<TB_CONTACT> l_TB_CONTACT = _PageContext.ServiceManager.TB_CONTACTService.SearchByKeyWord(p_KeyWord);
            if (l_TB_CONTACT.Count > 0)
            {
                SetResultMessage("查詢成功");
            }
            else {
                SetResultMessage("查無資料");
            }
            return PartialView("AdminContact", l_TB_CONTACT);
        }
    }
}