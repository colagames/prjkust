﻿using prjKUST.Filters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVMLib;
using TVMLib.Controllers;
using TVMLib.dl;

namespace prjKUST.Controllers
{
    [AuthorizeAdminFilter]
    public class AdminNewsController : BaseController
    {
        // GET: AdminNews
    
        public ActionResult AdminNews()
        {
            return View(_PageContext.ServiceManager.TB_NEWSService.Search());
        }
        public ActionResult Add()
        {
            return PartialView("Edit");
        }
        public ActionResult Edit(string p_NEWS_GUID)
        {
            TB_NEWS l_Data = _PageContext.ServiceManager.TB_NEWSService.SearchByGUID(p_NEWS_GUID);
            return PartialView("Edit", l_Data);
        }
        public ActionResult Delete(string p_NEWS_GUID)
        {
            TB_NEWS l_Data = _PageContext.ServiceManager.TB_NEWSService.SearchByGUID(p_NEWS_GUID);
            if (l_Data != null)
            {
                return PartialView("_Delete", l_Data);
            }
            else
            {
                SetResultMessage("查無資訊，請重新確認");
                return RedirectToAction("AdminNews");
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirm(string NEWS_GUID)
        {
            TB_NEWS l_Data = _PageContext.ServiceManager.TB_NEWSService.SearchByGUID(NEWS_GUID);
            if (l_Data != null)
            {
                _PageContext.ServiceManager.TB_NEWSService.Delete(l_Data);
                SetResultMessage(l_Data.TITLE + " 刪除完成");
                return RedirectToAction("AdminNews");
            }
            else
            {
                SetResultMessage("查無資訊，請重新確認");
                return RedirectToAction("AdminNews");
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TB_NEWS p_Data)
        {
            if (ModelState.IsValidField("TITLE") && ModelState.IsValidField("MAIN") && ModelState.IsValidField("IS_ACTIVE"))
            {

                if (!string.IsNullOrEmpty(p_Data.NEWS_GUID))
                {
                    p_Data.MAIN = HttpUtility.HtmlDecode(p_Data.MAIN);
                    p_Data.EDIT_TIME = DateTime.Now;
                    p_Data.EDIT_USER = Utility.UserData.USER_GUID;
                    _PageContext.ServiceManager.TB_NEWSService.Update(p_Data);
                    SetResultMessage("最新消息：" + p_Data.TITLE + " 修改完成");
                    return RedirectToAction("AdminNews");
                }
                else
                {
                    p_Data.MAIN = HttpUtility.HtmlDecode(p_Data.MAIN);
                    p_Data.CREATE_TIME = DateTime.Now;
                    p_Data.CREATE_USER = Utility.UserData.USER_GUID;
                    p_Data.EDIT_TIME = DateTime.Now;
                    p_Data.EDIT_USER = Utility.UserData.USER_GUID;
                    p_Data.NEWS_GUID = Guid.NewGuid().ToString();
                    _PageContext.ServiceManager.TB_NEWSService.Create(p_Data);
                    SetResultMessage("最新消息：" + p_Data.TITLE + " 新增完成");
                    return RedirectToAction("AdminNews");
                }
            }
            else
            {
                SetResultMessage("資料修改有誤，請確認。");
                return RedirectToAction("AdminNews");
            }
        }
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit(TB_NEWS p_Data)
        //{


        //}


        /// <summary>
        /// ckeditor上傳圖片
        /// </summary>
        /// <param name="upload">預設參數叫upload</param>
        /// <param name="CKEditorFuncNum"></param>
        /// <param name="CKEditor"></param>
        /// <param name="langCode"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UploadPicture(HttpPostedFileBase upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            string result = "";
            if (upload != null && upload.ContentLength > 0)
            {
                DateTime l_DtNow = DateTime.Now;
                string l_FileExtName = Path.GetExtension(upload.FileName).ToLower();
                string l_BasePath = Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["FilesDirectory"].ToString().ToUpper()) + "ckeditorFiles\\" + l_DtNow.ToString("yyyy") + "\\" + l_DtNow.ToString("MM") + "\\" + l_DtNow.ToString("dd") + "\\";
                if (!Directory.Exists(l_BasePath))
                    Directory.CreateDirectory(l_BasePath);
                string l_FullFilePath = Path.Combine(l_BasePath, upload.FileName + l_FileExtName);
                //儲存圖片至Server
                upload.SaveAs(l_FullFilePath);


                var imageUrl = Url.Content(System.Configuration.ConfigurationManager.AppSettings["FilesDirectory"].ToString().ToUpper() + "ckeditorFiles\\" + l_DtNow.ToString("yyyy") + "\\" + l_DtNow.ToString("MM") + "\\" + l_DtNow.ToString("dd") + "\\" + upload.FileName + l_FileExtName);

                var vMessage = string.Empty;

                result = @"<html><body><script>window.parent.CKEDITOR.tools.callFunction(" + CKEditorFuncNum + ", \"" + imageUrl + "\", \"" + vMessage + "\");</script></body></html>";

            }

            return Content(result);
        }

    }
}