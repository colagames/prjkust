﻿using prjKUST.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVMLib.Controllers;
using TVMLib.dl;

namespace prjKUST.Controllers
{
    [AuthorizeAdminFilter]
    public class AdminAboutusController : BaseController
    {
        // GET: AdminAboutus
        public ActionResult AdminAboutus()
        {
            return View(_PageContext.ServiceManager.TB_ABOUTUSService.SearchByGUID("admin_init"));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TB_ABOUTUS p_Data)
        {
            TB_ABOUTUS l_Data = _PageContext.ServiceManager.TB_ABOUTUSService.SearchByGUID(p_Data.ABOUTUS_GUID);
            if (l_Data != null)
            {
                p_Data.ABOUTUS_MAIN1 = HttpUtility.HtmlDecode(p_Data.ABOUTUS_MAIN1);
                p_Data.ABOUTUS_MAIN2 = HttpUtility.HtmlDecode(p_Data.ABOUTUS_MAIN2);     
                _PageContext.ServiceManager.TB_ABOUTUSService.Update(p_Data);
                SetResultMessage("關於我們資料修改完成");
                return RedirectToAction("AdminAboutus");
            }
            else {
                SetResultMessage("資料有誤，請確認");
                return RedirectToAction("AdminAboutus");
                
            }


          


        }
    }
}