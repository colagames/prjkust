﻿using prjKUST.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVMLib;
using TVMLib.Controllers;
using TVMLib.dl;
using TVMLib.dl.AdminProfile;

namespace prjKUST.Controllers
{
    [AuthorizeAdminFilter]
    public class AdminProfileController : BaseController
    {
        // GET: AdminProfile
        public ActionResult AdminProfile()
        {
            List<TB_PROFILE> l_TB_PROFILE = _PageContext.ServiceManager.TB_PROFILEService.Search();
            return View(l_TB_PROFILE);
        }

        public ActionResult Delete(string p_PROFILE_GUID)
        {
            TB_PROFILE l_Data = _PageContext.ServiceManager.TB_PROFILEService.SearchByGUID(p_PROFILE_GUID);
            if (l_Data != null)
            {
                return PartialView("_Delete", l_Data);
            }
            else
            {
                SetResultMessage("查無資訊，請重新確認");
                return RedirectToAction("AdminProfile");
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirm(string PROFILE_GUID)
        {
            TB_PROFILE l_Data = _PageContext.ServiceManager.TB_PROFILEService.SearchByGUID(PROFILE_GUID);

            if (l_Data != null)
            {

                _PageContext.ServiceManager.TB_PROFILEService.Delete(l_Data);
                SetResultMessage(l_Data.TITLE + " 刪除完成");
                return RedirectToAction("AdminProfile");
            }
            else
            {
                SetResultMessage("查無資訊，請重新確認");
                return RedirectToAction("AdminProfile");
            }

        }

        public ActionResult Add()
        {
            return PartialView("_Add");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(AdminProfileVM p_Data)
        {
            string l_FullFilePath1 = "";

            if (ModelState.IsValidField("PATH_UPLOAD") && ModelState.IsValidField("TITLE"))
            {
                string l_Msg = "上傳失敗";

                if (p_Data.PATH_UPLOAD != null)
                {
                    if (!Utility.SaveFile(p_Data.PATH_UPLOAD, out l_FullFilePath1, p_Data.PATH, p_PreName: p_Data.PATH_UPLOAD.FileName))
                    {
                        SetResultMessage(l_Msg); return View(p_Data);
                    }
                }

                TB_PROFILE l_TB_PROFILE = new TB_PROFILE();
                l_TB_PROFILE.PATH = l_FullFilePath1 == "" ? (p_Data.PATH == null ? "" : p_Data.PATH) : l_FullFilePath1;
                l_TB_PROFILE.TITLE = p_Data.TITLE;
                l_TB_PROFILE.CREATE_TIME = System.DateTime.Now;
                l_TB_PROFILE.CREATE_USER = Utility.UserData.USER_GUID;
                l_TB_PROFILE.PROFILE_GUID = Guid.NewGuid().ToString();
                l_TB_PROFILE.IS_ACTIVE = p_Data.IS_ACTIVE;
                _PageContext.ServiceManager.TB_PROFILEService.Create(l_TB_PROFILE);
                SetResultMessage("技術規格：" + p_Data.TITLE + " 新增完成");
                return RedirectToAction("AdminProfile");
            }

            SetResultMessage("上傳檔案有誤，請重新確認");
            return View(p_Data);
        }


        public ActionResult Edit(string p_PROFILE_GUID)
        {
            TB_PROFILE l_Data = _PageContext.ServiceManager.TB_PROFILEService.SearchByGUID(p_PROFILE_GUID);
            if (l_Data != null)
            {
                AdminProfileVM l_AdminProfileVM = new AdminProfileVM();
                l_AdminProfileVM.IS_ACTIVE = l_Data.IS_ACTIVE;
                l_AdminProfileVM.PATH = l_Data.PATH;
                l_AdminProfileVM.PROFILE_GUID = l_Data.PROFILE_GUID;
                l_AdminProfileVM.TITLE = l_Data.TITLE;
                return PartialView("_Edit", l_AdminProfileVM);
            }
            SetResultMessage("資料有誤，請重新確認");
            return RedirectToAction("AdminProfile");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AdminProfileVM p_Data)
        {
            string l_FullFilePath1 = "";

            if (ModelState.IsValidField("PATH_UPLOAD") && ModelState.IsValidField("TITLE"))
            {
                string l_Msg = "上傳失敗";

                if (p_Data.PATH_UPLOAD != null)
                {
                    if (!Utility.SaveFile(p_Data.PATH_UPLOAD, out l_FullFilePath1, p_Data.PATH, p_PreName: p_Data.PATH_UPLOAD.FileName))
                    {
                        SetResultMessage(l_Msg); return View(p_Data);
                    }
                }

                TB_PROFILE l_TB_PROFILE = new TB_PROFILE();
                l_TB_PROFILE.PATH = l_FullFilePath1 == "" ? p_Data.PATH : l_FullFilePath1;
                l_TB_PROFILE.TITLE = p_Data.TITLE;
                l_TB_PROFILE.IS_ACTIVE = p_Data.IS_ACTIVE;
                l_TB_PROFILE.PROFILE_GUID = p_Data.PROFILE_GUID;
                _PageContext.ServiceManager.TB_PROFILEService.Update(l_TB_PROFILE);
                SetResultMessage("技術規格：" + p_Data.TITLE + "修改完成");
                return RedirectToAction("AdminProfile");
            }

            SetResultMessage("資料有誤，請重新確認");
            return RedirectToAction("AdminProfile");
        }
    }
}