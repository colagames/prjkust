﻿using prjKUST.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVMLib;
using TVMLib.Controllers;
using TVMLib.dl;
using TVMLib.dl.Login;

namespace prjKUST.Controllers
{
    public class MemberController : BaseController
    {
        // GET: Member
        public ActionResult Login(string p_PRODUCT_GUID = "")
        {
            ViewBag.PRODUCT_GUID = p_PRODUCT_GUID;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SkipAuthorizeFilterAttribute]
        public ActionResult Login(LoginVM p_Data,string p_PRODUCT_GUID="")
        {
            if (ModelState.IsValidField("ACCOUNT") && ModelState.IsValidField("PASSWORD"))
            {
                TB_USER l_TB_USER = _PageContext.ServiceManager.TB_USERService.SearchByAccAndPwd(p_Data.ACCOUNT, p_Data.PASSWORD);
                if (l_TB_USER != null)
                {
                    //登入成功
                    Utility.GenLoginUserCookieAndSessionInfo(l_TB_USER);

                    SetResultMessage("使用者：" + l_TB_USER.NAME + "登入成功");

                    //有PRODUCT_GUID = 從 商品明細頁 沒有登入 進來的 所以導回去
                    if (p_PRODUCT_GUID != "")
                    {
                        return RedirectToAction("ProductDetail", "Product",new { p_RRODUCT_GUID  = p_PRODUCT_GUID });
                    }
                    else {
                        return RedirectToAction("Index", "Index");
                    }
                }
                else
                {
                    SetResultMessage("帳號或密碼有誤，請重新確認。");
                }
            }

            SetResultMessage("帳號或密碼有誤，請確認。");
            return View(p_Data);
        }
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SkipAuthorizeFilterAttribute]
        public ActionResult Register(RegisteredVM p_Data)
        {
            if (ModelState.IsValid)
            {

                TB_USER l_TB_USER = new TB_USER();
                l_TB_USER.NAME = p_Data.NAME;
                l_TB_USER.ACCOUNT = p_Data.ACCOUNT;
                l_TB_USER.PASSWORD = p_Data.PASSWORD;
                l_TB_USER.EMAIL = p_Data.EMAIL;
                l_TB_USER.ADDRESS = p_Data.ADDRESS;
                l_TB_USER.AREA = p_Data.AREA;
                l_TB_USER.CITY = p_Data.CITY;
                l_TB_USER.ZIP = p_Data.ZIP;
                l_TB_USER.IS_ACTIVE = true;
                l_TB_USER.IS_ADMIN = false;
                l_TB_USER.TEL = p_Data.TEL;
                l_TB_USER.USER_GUID = Guid.NewGuid().ToString();
                l_TB_USER.CREATE_TIME = DateTime.Now;

                l_TB_USER.CREATE_USER = l_TB_USER.USER_GUID;
                l_TB_USER.EDIT_TIME = DateTime.Now;
                l_TB_USER.EDIT_USER = l_TB_USER.USER_GUID;



                _PageContext.ServiceManager.TB_USERService.Create(l_TB_USER);
                SetResultMessage("會員：" + p_Data.NAME + " 註冊成功。");
                return RedirectToAction("Login");
            }
            SetResultMessage("註冊失敗，請確認資料內容");
            return Register(p_Data);
        }


        public ActionResult Forget()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [SkipAuthorizeFilterAttribute]
        public ActionResult Forget(ForgetVM p_Data)
        {
            if (ModelState.IsValidField("EMAIL"))
            {
                if (_PageContext.ServiceManager.TB_USERService.GetNewPwdByEmail(p_Data.EMAIL))
                    SetResultMessage("已發送新密碼至您的信箱！");
                else
                    SetResultMessage("發送新密碼至您的信箱時失敗，請聯絡系統管理員！");

                return View();
            }
            else
            {
                SetResultMessage("輸入資料有誤，請確認。");
                return View(p_Data);
            }
          
        }
    }
}