﻿using prjKUST.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVMLib;
using TVMLib.Controllers;
using TVMLib.dl;
using TVMLib.dl.AdminInquiry;
using TVMLib.dl.Product;

namespace prjKUST.Controllers
{
    [AuthorizeAdminFilter]
    public class AdminInquiryController : BaseController
    {
        // GET: AdminInquiry
        public ActionResult AdminInquiry()
        {

            List<InquiryVM> l_InquiryVM = _PageContext.ServiceManager.TB_INQUIRYService.SearchInquiryVM();
            foreach (var item in l_InquiryVM)
            {
                item.l_TB_PRODUCT = _PageContext.ServiceManager.TB_PRODUCTService.SearchByGUID(item.PRODUCT_GUID);
            }
            return View(l_InquiryVM);
        }

        public ActionResult Edit(string p_INQUIRY_GUID)
        {
            AdminInquiryVM l_AdminInquiryVM = _PageContext.ServiceManager.TB_INQUIRYService.SearchAdminInquiryVMByGUID(p_INQUIRY_GUID);

            return PartialView("_Edit", l_AdminInquiryVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AdminInquiryVM p_InquiryVM)
        {
            if (ModelState.IsValid)
            {
                TB_INQUIRY l_TB_INQUIRY = new TB_INQUIRY();
                l_TB_INQUIRY.INQUIRY_GUID = p_InquiryVM.INQUIRY_GUID;
                l_TB_INQUIRY.RE_COUNT = p_InquiryVM.RE_COUNT;
                l_TB_INQUIRY.RE_PRICE = p_InquiryVM.RE_PRICE;
                l_TB_INQUIRY.RE_PROCESS = p_InquiryVM.RE_PROCESS;
                l_TB_INQUIRY.RE_TOTAL = p_InquiryVM.RE_TOTAL;
                l_TB_INQUIRY.EDIT_TIME = DateTime.Now;
                l_TB_INQUIRY.EDIT_USER = Utility.UserData.USER_GUID;
                _PageContext.ServiceManager.TB_INQUIRYService.Update(l_TB_INQUIRY);
                SetResultMessage("詢價商品修改完成");
                return RedirectToAction("AdminInquiry");
            }
            SetResultMessage("詢價商品修改資料有誤，請確認。");
            return RedirectToAction("AdminInquiry");
        }


        //public ActionResult Delete(string p_INQUIRY_GUID)
        //{
        //    AdminInquiryVM l_AdminInquiryVM = _PageContext.ServiceManager.TB_INQUIRYService.SearchAdminInquiryVMByGUID(p_INQUIRY_GUID);

        //    if (l_AdminInquiryVM != null)
        //    {
        //        return PartialView("_Delete", l_AdminInquiryVM);
        //    }
        //    else
        //    {
        //        SetResultMessage("查無資訊，請重新確認");
        //        return RedirectToAction("AdminInquiry");
        //    }

        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirm(string INQUIRY_GUID)
        //{
        //    AdminInquiryVM l_AdminInquiryVM = _PageContext.ServiceManager.TB_INQUIRYService.SearchAdminInquiryVMByGUID(p_INQUIRY_GUID);

        //    if (l_Data != null)
        //    {
        //        l_Data.IS_DELETE = true;
        //        _PageContext.ServiceManager.TB_PRODUCTService.Delete(l_Data);
        //        SetResultMessage(l_Data.NAME + " 刪除完成");
        //        return RedirectToAction("AdminProduct");
        //    }
        //    else
        //    {
        //        SetResultMessage("查無資訊，請重新確認");
        //        return RedirectToAction("AdminProduct");
        //    }

        //}
    }
}