﻿using prjKUST.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVMLib.Controllers;

namespace prjKUST.Controllers
{
    [AuthorizeAdminFilter]
    public class AdminSliderController : BaseController
    {
        // GET: AdminSlider
        public ActionResult AdminSlider()
        {
            return View(_PageContext.ServiceManager.TB_SLIDERService.Search());
        }
    }
}