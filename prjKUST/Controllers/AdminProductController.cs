﻿using prjKUST.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVMLib;
using TVMLib.Controllers;
using TVMLib.dl;
using TVMLib.dl.AdminProduct;

namespace prjKUST.Controllers
{
    [AuthorizeAdminFilter]
    public class AdminProductController : BaseController
    {
        // GET: AdminProduct
        public ActionResult AdminProduct()
        {
            List<AdminProductVM> l_Data = _PageContext.ServiceManager.TB_PRODUCTService.SearchAllAdminProductVMByGUID();
            l_Data = l_Data.Where(m => m.IS_DELETE == false).ToList();
            return View(l_Data);
        }


        public ActionResult Add()
        {
            ViewBag.TB_CATEGORY_DDL = Utility.GenProductCategoryDDL(_PageContext.ServiceManager.TB_PRODUCT_CATEGORYService.Search(), "");
            return View("Edit");

        }
        public ActionResult Edit(string p_PRODUCT_GUID)
        {
            AdminProductVM l_Data = _PageContext.ServiceManager.TB_PRODUCTService.SearchAdminProductVMByGUID(p_PRODUCT_GUID);
            ViewBag.TB_CATEGORY_DDL = Utility.GenProductCategoryDDL(_PageContext.ServiceManager.TB_PRODUCT_CATEGORYService.Search(), l_Data.CATEGORY_GUID);
            return View(l_Data);
        }
        public ActionResult Delete(string p_PRODUCT_GUID)
        {
            TB_PRODUCT l_Data = _PageContext.ServiceManager.TB_PRODUCTService.SearchByGUID(p_PRODUCT_GUID);
            if (l_Data != null)
            {
                return PartialView("_Delete", l_Data);
            }
            else
            {
                SetResultMessage("查無資訊，請重新確認");
                return RedirectToAction("AdminProduct");
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirm(string PRODUCT_GUID)
        {
            TB_PRODUCT l_Data = _PageContext.ServiceManager.TB_PRODUCTService.SearchByGUID(PRODUCT_GUID);
            if (l_Data != null)
            {
                l_Data.IS_DELETE = true;
                _PageContext.ServiceManager.TB_PRODUCTService.Delete(l_Data);
                SetResultMessage(l_Data.NAME + " 刪除完成");
                return RedirectToAction("AdminProduct");
            }
            else
            {
                SetResultMessage("查無資訊，請重新確認");
                return RedirectToAction("AdminProduct");
            }

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AdminProductVM p_Data)
        {
            string l_FullFilePath1 = "";
            string l_FullFilePath2 = "";
            string l_FullFilePath3 = "";
            string l_FullFilePath4 = "";
            string l_FullFilePath5 = "";
            string l_FullFilePath6 = "";
            if (
                    ModelState.IsValidField("PIC1_UPLOAD") &&
                    ModelState.IsValidField("PIC2_UPLOAD") &&
                    ModelState.IsValidField("PIC3_UPLOAD") &&
                    ModelState.IsValidField("PIC4_UPLOAD") &&
                    ModelState.IsValidField("PIC5_UPLOAD") &&
                    ModelState.IsValidField("PIC6_UPLOAD")
                    )
            {
                string l_Msg = "上傳失敗";

                #region 檔案1 ~ 6 存檔處理
                if (p_Data.PIC1_UPLOAD != null)
                {

                    if (!Utility.SaveFile(p_Data.PIC1_UPLOAD, out l_FullFilePath1, p_Data.PIC1, p_PreName: p_Data.PIC1_UPLOAD.FileName))
                    {

                        SetResultMessage(l_Msg); return View(p_Data);
                    }

                }
                if (p_Data.PIC2_UPLOAD != null)
                {

                    if (!Utility.SaveFile(p_Data.PIC2_UPLOAD, out l_FullFilePath2, p_Data.PIC1, p_PreName: p_Data.PIC2_UPLOAD.FileName))
                    {

                        SetResultMessage(l_Msg); return View(p_Data);
                    }

                }
                if (p_Data.PIC3_UPLOAD != null)
                {

                    if (!Utility.SaveFile(p_Data.PIC3_UPLOAD, out l_FullFilePath3, p_Data.PIC1, p_PreName: p_Data.PIC3_UPLOAD.FileName))
                    {

                        SetResultMessage(l_Msg); return View(p_Data);
                    }

                }
                if (p_Data.PIC4_UPLOAD != null)
                {

                    if (!Utility.SaveFile(p_Data.PIC4_UPLOAD, out l_FullFilePath4, p_Data.PIC1, p_PreName: p_Data.PIC4_UPLOAD.FileName))
                    {

                        SetResultMessage(l_Msg); return View(p_Data);
                    }

                }
                if (p_Data.PIC5_UPLOAD != null)
                {

                    if (!Utility.SaveFile(p_Data.PIC5_UPLOAD, out l_FullFilePath5, p_Data.PIC1, p_PreName: p_Data.PIC5_UPLOAD.FileName))
                    {

                        SetResultMessage(l_Msg); return View(p_Data);
                    }

                }
                if (p_Data.PIC6_UPLOAD != null)
                {

                    if (!Utility.SaveFile(p_Data.PIC6_UPLOAD, out l_FullFilePath6, p_Data.PIC1, p_PreName: p_Data.PIC6_UPLOAD.FileName))
                    {

                        SetResultMessage(l_Msg); return View(p_Data);
                    }

                }
                #endregion


                TB_PRODUCT l_TB_PRODUCT = new TB_PRODUCT();

                l_TB_PRODUCT.PIC1 = l_FullFilePath1 == "" ? p_Data.PIC1 : l_FullFilePath1;
                l_TB_PRODUCT.PIC2 = l_FullFilePath2 == "" ? p_Data.PIC2 : l_FullFilePath2;
                l_TB_PRODUCT.PIC3 = l_FullFilePath3 == "" ? p_Data.PIC3 : l_FullFilePath3;
                l_TB_PRODUCT.PIC4 = l_FullFilePath4 == "" ? p_Data.PIC4 : l_FullFilePath4;
                l_TB_PRODUCT.PIC5 = l_FullFilePath5 == "" ? p_Data.PIC5 : l_FullFilePath5;
                l_TB_PRODUCT.PIC6 = l_FullFilePath6 == "" ? p_Data.PIC6 : l_FullFilePath6;
                l_TB_PRODUCT.CATEGORY_GUID = p_Data.CATEGORY_GUID;
                l_TB_PRODUCT.DESCRIPTION = p_Data.DESCRIPTION;
                l_TB_PRODUCT.EDIT_TIME = System.DateTime.Now;
                l_TB_PRODUCT.EDIT_USER = Utility.UserData.USER_GUID;
                l_TB_PRODUCT.IS_INQUIRY = p_Data.IS_INQUIRY;
                l_TB_PRODUCT.PRICE = p_Data.PRICE;
                l_TB_PRODUCT.NAME = p_Data.NAME;
                l_TB_PRODUCT.PRODUCT_GUID = p_Data.PRODUCT_GUID;
                p_Data.EDIT_TIME = DateTime.Now;
                p_Data.EDIT_USER = Utility.UserData.USER_GUID;

                if (!string.IsNullOrEmpty( p_Data.PRODUCT_GUID))
                {
                    _PageContext.ServiceManager.TB_PRODUCTService.Update(l_TB_PRODUCT);
                    SetResultMessage("產品：" + p_Data.NAME + " 修改完成");
                   
                }
                else {
                    l_TB_PRODUCT.CREATE_TIME = System.DateTime.Now;
                    l_TB_PRODUCT.CREATE_USER = Utility.UserData.USER_GUID;
                    l_TB_PRODUCT.PRODUCT_GUID = Guid.NewGuid().ToString();
                    l_TB_PRODUCT.IS_DELETE = false;
                    _PageContext.ServiceManager.TB_PRODUCTService.Create(l_TB_PRODUCT);
                    SetResultMessage("產品：" + p_Data.NAME + " 新增完成");
                }
             
                return RedirectToAction("AdminProduct");
            }
            ViewBag.TB_CATEGORY_DDL = Utility.GenProductCategoryDDL(_PageContext.ServiceManager.TB_PRODUCT_CATEGORYService.Search(), p_Data.CATEGORY_GUID);
            SetResultMessage("上傳檔案有誤，請重新確認");
            return View(p_Data);

        }
    }
}