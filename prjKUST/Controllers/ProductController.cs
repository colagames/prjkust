﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVMLib;
using TVMLib.Controllers;
using TVMLib.dl;
using TVMLib.dl.Order;
using TVMLib.dl.Product;

namespace prjKUST.Controllers
{
    public class ProductController : BaseController
    {
        // GET: Product

        public ActionResult Product(string p_Type, string p_CATEGORY)
        {
            if (!string.IsNullOrEmpty(p_Type) && !string.IsNullOrEmpty(p_CATEGORY))
            {
                if (p_Type == "ALL")
                {
                    ViewBag.Product = "所有";
                }
                else
                {
                    ViewBag.Product = p_Type;
                }

                List<TB_PRODUCT> l_Data = _PageContext.ServiceManager.TB_PRODUCTService.SearchByCategoryGUID(p_CATEGORY, false);
                return View(l_Data);
            }
            SetResultMessage("資訊有誤，請重新操作。");
            return RedirectToAction("index", "index");
        }

        public ActionResult ProductDetail(string p_RRODUCT_GUID)
        {
            ProductVM l_ProductVM = _PageContext.ServiceManager.TB_PRODUCTService.SearchProductVMByGUID(p_RRODUCT_GUID);


            if (l_ProductVM != null)
            {
                l_ProductVM.l_ProductImageVM = new List<ProductImageVM>();

                if (!String.IsNullOrEmpty(l_ProductVM.PIC1))
                {
                    ProductImageVM l_ProductImageVM = new ProductImageVM();
                    l_ProductImageVM.URL = l_ProductVM.PIC1;
                    l_ProductImageVM.NAME = l_ProductVM.PIC1;
                    l_ProductVM.l_ProductImageVM.Add(l_ProductImageVM);
                }
                if (!String.IsNullOrEmpty(l_ProductVM.PIC2))
                {
                    ProductImageVM l_ProductImageVM = new ProductImageVM();
                    l_ProductImageVM.URL = l_ProductVM.PIC2;
                    l_ProductImageVM.NAME = l_ProductVM.PIC2;
                    l_ProductVM.l_ProductImageVM.Add(l_ProductImageVM);
                }
                if (!String.IsNullOrEmpty(l_ProductVM.PIC3))
                {
                    ProductImageVM l_ProductImageVM = new ProductImageVM();
                    l_ProductImageVM.URL = l_ProductVM.PIC3;
                    l_ProductImageVM.NAME = l_ProductVM.PIC3;
                    l_ProductVM.l_ProductImageVM.Add(l_ProductImageVM);
                }
                if (!String.IsNullOrEmpty(l_ProductVM.PIC4))
                {
                    ProductImageVM l_ProductImageVM = new ProductImageVM();
                    l_ProductImageVM.URL = l_ProductVM.PIC4;
                    l_ProductImageVM.NAME = l_ProductVM.PIC4;
                    l_ProductVM.l_ProductImageVM.Add(l_ProductImageVM);
                }
                if (!String.IsNullOrEmpty(l_ProductVM.PIC5))
                {
                    ProductImageVM l_ProductImageVM = new ProductImageVM();
                    l_ProductImageVM.URL = l_ProductVM.PIC5;
                    l_ProductImageVM.NAME = l_ProductVM.PIC5;
                    l_ProductVM.l_ProductImageVM.Add(l_ProductImageVM);
                }
                if (!String.IsNullOrEmpty(l_ProductVM.PIC6))
                {
                    ProductImageVM l_ProductImageVM = new ProductImageVM();
                    l_ProductImageVM.URL = l_ProductVM.PIC6;
                    l_ProductImageVM.NAME = l_ProductVM.PIC6;
                    l_ProductVM.l_ProductImageVM.Add(l_ProductImageVM);
                }
                return View(l_ProductVM);
            }
            return RedirectToAction("Product");
        }
        public ActionResult Checkout(ProductVM p_Data)
        {
            if (p_Data.PRODUCT_GUID != null)
            {
                TB_PRODUCT l_TB_PRODUCT = _PageContext.ServiceManager.TB_PRODUCTService.SearchByGUID(p_Data.PRODUCT_GUID);

                if (l_TB_PRODUCT != null)
                {
                    TB_USER l_TB_USER = Utility.UserData;
                    if (!string.IsNullOrEmpty(l_TB_USER.USER_GUID))
                    {
                        if (Utility.OrderAndDetail == null)
                        {
                            OrderAndDetailVM l_OrderAndDetailVM = new OrderAndDetailVM();
                            l_OrderAndDetailVM.l_TB_ORDER_DETAIL = new List<TB_ORDER_DETAIL>();

                            TB_ORDER_DETAIL l_TB_ORDER_DETAIL = new TB_ORDER_DETAIL();
                            l_TB_ORDER_DETAIL.PRICE = l_TB_PRODUCT.PRICE;
                            l_TB_ORDER_DETAIL.PRODUCT_GUID = l_TB_PRODUCT.PRODUCT_GUID;
                            l_TB_ORDER_DETAIL.SUB_TOTAL = p_Data.UNIT * l_TB_PRODUCT.PRICE;
                            l_TB_ORDER_DETAIL.UNIT = p_Data.UNIT;
                            l_TB_ORDER_DETAIL.l_TB_PRODUCT = l_TB_PRODUCT;
                            l_TB_ORDER_DETAIL.ORDER_DETAIL_GUID = Guid.NewGuid().ToString();
                            l_OrderAndDetailVM.l_TB_ORDER_DETAIL.Add(l_TB_ORDER_DETAIL);
                            Utility.GenOrderAndOrderDetailCookieAndSessionInfo(l_OrderAndDetailVM);
                        }
                        else
                        {
                            OrderAndDetailVM l_OrderAndDetailVM = Utility.OrderAndDetail;
                            if (l_OrderAndDetailVM.l_TB_ORDER_DETAIL != null)
                            {
                                if (l_OrderAndDetailVM.l_TB_ORDER_DETAIL.Where(m => m.PRODUCT_GUID == l_TB_PRODUCT.PRODUCT_GUID).ToList().Count > 0)
                                {
                                    l_OrderAndDetailVM.l_TB_ORDER_DETAIL.Where(m => m.PRODUCT_GUID == l_TB_PRODUCT.PRODUCT_GUID).ToList().ForEach(s => s.UNIT += p_Data.UNIT);
                                    l_OrderAndDetailVM.l_TB_ORDER_DETAIL.Where(m => m.PRODUCT_GUID == l_TB_PRODUCT.PRODUCT_GUID).ToList().ForEach(s => s.SUB_TOTAL = s.PRICE * s.UNIT);
                                }
                                else
                                {
                                    TB_ORDER_DETAIL l_TB_ORDER_DETAIL = new TB_ORDER_DETAIL();
                                    l_TB_ORDER_DETAIL.PRICE = l_TB_PRODUCT.PRICE;
                                    l_TB_ORDER_DETAIL.PRODUCT_GUID = l_TB_PRODUCT.PRODUCT_GUID;
                                    l_TB_ORDER_DETAIL.SUB_TOTAL = p_Data.UNIT * l_TB_PRODUCT.PRICE;
                                    l_TB_ORDER_DETAIL.UNIT = p_Data.UNIT;
                                    l_TB_ORDER_DETAIL.l_TB_PRODUCT = l_TB_PRODUCT;
                                    l_TB_ORDER_DETAIL.ORDER_DETAIL_GUID = Guid.NewGuid().ToString();
                                    l_OrderAndDetailVM.l_TB_ORDER_DETAIL.Add(l_TB_ORDER_DETAIL);
                                }

                                Utility.GenOrderAndOrderDetailCookieAndSessionInfo(l_OrderAndDetailVM);
                            }
                        }
                        return View(Utility.OrderAndDetail);
                    }
                    else
                    {
                        SetResultMessage("請先登入會員。");
                        return RedirectToAction("Login", "Member", new { p_PRODUCT_GUID = l_TB_PRODUCT.PRODUCT_GUID });
                    }
                }
                SetResultMessage("商品資訊有誤，請重新確認。");
                return RedirectToAction("Index", "Index");
            }
            else
            {
                if (Utility.OrderAndDetail == null)
                {
                    SetResultMessage("商品資訊有誤，請重新確認。");
                    return RedirectToAction("Index", "Index");


                }
                else if (Utility.OrderAndDetail.l_TB_ORDER_DETAIL == null)
                {
                    SetResultMessage("商品資訊有誤，請重新確認。");
                    return RedirectToAction("Index", "Index");
                }
                else if (Utility.OrderAndDetail.l_TB_ORDER_DETAIL.Count <= 0)
                {
                    SetResultMessage("查無商品，請重新確認。");
                    return RedirectToAction("Index", "Index");
                }
                else {
                    return View(Utility.OrderAndDetail);
                }

            }

        }


        public ActionResult Inquiry(string p_RRODUCT_GUID)
        {
            InquiryVM inquiryVM = new InquiryVM();
            inquiryVM.l_TB_PRODUCT = _PageContext.ServiceManager.TB_PRODUCTService.SearchByGUID(p_RRODUCT_GUID);



            if (inquiryVM.l_TB_PRODUCT != null)
            {
                inquiryVM.l_ProductImageVM = new List<ProductImageVM>();

                if (!String.IsNullOrEmpty(inquiryVM.l_TB_PRODUCT.PIC1))
                {
                    ProductImageVM l_ProductImageVM = new ProductImageVM();
                    l_ProductImageVM.URL = inquiryVM.l_TB_PRODUCT.PIC1;
                    l_ProductImageVM.NAME = inquiryVM.l_TB_PRODUCT.PIC1;
                    inquiryVM.l_ProductImageVM.Add(l_ProductImageVM);
                }
                if (!String.IsNullOrEmpty(inquiryVM.l_TB_PRODUCT.PIC2))
                {
                    ProductImageVM l_ProductImageVM = new ProductImageVM();
                    l_ProductImageVM.URL = inquiryVM.l_TB_PRODUCT.PIC2;
                    l_ProductImageVM.NAME = inquiryVM.l_TB_PRODUCT.PIC2;
                    inquiryVM.l_ProductImageVM.Add(l_ProductImageVM);
                }
                if (!String.IsNullOrEmpty(inquiryVM.l_TB_PRODUCT.PIC3))
                {
                    ProductImageVM l_ProductImageVM = new ProductImageVM();
                    l_ProductImageVM.URL = inquiryVM.l_TB_PRODUCT.PIC3;
                    l_ProductImageVM.NAME = inquiryVM.l_TB_PRODUCT.PIC3;
                    inquiryVM.l_ProductImageVM.Add(l_ProductImageVM);
                }
                if (!String.IsNullOrEmpty(inquiryVM.l_TB_PRODUCT.PIC4))
                {
                    ProductImageVM l_ProductImageVM = new ProductImageVM();
                    l_ProductImageVM.URL = inquiryVM.l_TB_PRODUCT.PIC4;
                    l_ProductImageVM.NAME = inquiryVM.l_TB_PRODUCT.PIC4;
                    inquiryVM.l_ProductImageVM.Add(l_ProductImageVM);
                }
                if (!String.IsNullOrEmpty(inquiryVM.l_TB_PRODUCT.PIC5))
                {
                    ProductImageVM l_ProductImageVM = new ProductImageVM();
                    l_ProductImageVM.URL = inquiryVM.l_TB_PRODUCT.PIC5;
                    l_ProductImageVM.NAME = inquiryVM.l_TB_PRODUCT.PIC5;
                    inquiryVM.l_ProductImageVM.Add(l_ProductImageVM);
                }
                if (!String.IsNullOrEmpty(inquiryVM.l_TB_PRODUCT.PIC6))
                {
                    ProductImageVM l_ProductImageVM = new ProductImageVM();
                    l_ProductImageVM.URL = inquiryVM.l_TB_PRODUCT.PIC6;
                    l_ProductImageVM.NAME = inquiryVM.l_TB_PRODUCT.PIC6;
                    inquiryVM.l_ProductImageVM.Add(l_ProductImageVM);
                }
                return View(inquiryVM);
            }
            return RedirectToAction("Product");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Inquiry(InquiryVM p_Data)
        {
            if (
                ModelState.IsValidField("NAME") &&
                ModelState.IsValidField("EMAIL") &&
                ModelState.IsValidField("MAIN") &&
                ModelState.IsValidField("TEL")
                )
            {

                TB_INQUIRY l_TB_INQUIRY = new TB_INQUIRY();
                l_TB_INQUIRY.PRODUCT_GUID = p_Data.PRODUCT_GUID;
                l_TB_INQUIRY.NAME = p_Data.NAME;
                l_TB_INQUIRY.TEL = p_Data.TEL;
                l_TB_INQUIRY.MAIN = p_Data.MAIN;
                l_TB_INQUIRY.EMAIL = p_Data.EMAIL;
                l_TB_INQUIRY.INQUIRY_GUID = Guid.NewGuid().ToString();
                l_TB_INQUIRY.USER_GUID = string.IsNullOrEmpty(Utility.UserData.USER_GUID) ? "WEB" : Utility.UserData.USER_GUID;
                l_TB_INQUIRY.CREATE_USER = "WEB";
                l_TB_INQUIRY.EDIT_USER = "WEB";
                l_TB_INQUIRY.CREATE_TIME = DateTime.Now;
                l_TB_INQUIRY.EDIT_TIME = DateTime.Now;


                _PageContext.ServiceManager.TB_INQUIRYService.Create(l_TB_INQUIRY);
                SetResultMessage(l_TB_INQUIRY.NAME + " 詢價單 處理完成");

                return RedirectToAction("ProductDetail", "Product", new { p_RRODUCT_GUID = p_Data.PRODUCT_GUID });
            }
            else
            {
                SetResultMessage("資料驗證有誤，請再確認");
                return RedirectToAction("ProductDetail", "Product", new { p_RRODUCT_GUID = p_Data.PRODUCT_GUID });
            }

        }

        public ActionResult RemoveShopItem(string p_PRODUCT_GUID)
        {
            OrderAndDetailVM l_OrderAndDetailVM = Utility.OrderAndDetail;
            if (l_OrderAndDetailVM != null)
            {
                if (l_OrderAndDetailVM.l_TB_ORDER_DETAIL.Where(m => m.PRODUCT_GUID == p_PRODUCT_GUID).ToList().Count > 0)
                {
                    var itemToRemove = l_OrderAndDetailVM.l_TB_ORDER_DETAIL.Single(r => r.PRODUCT_GUID == p_PRODUCT_GUID);
                    l_OrderAndDetailVM.l_TB_ORDER_DETAIL.Remove(itemToRemove);
                    Utility.GenOrderAndOrderDetailCookieAndSessionInfo(l_OrderAndDetailVM);
                    SetResultMessage("移除完成");
                }
                else
                {
                    SetResultMessage("查無該商品可移除，請確認");
                }
            }
            else
            {
                SetResultMessage("查無該商品可移除，請確認");
            }


            return View("Checkout", Utility.OrderAndDetail);
        }

        public ActionResult Hot()
        {
            return View();
        }


        public ActionResult CheckoutDetail()
        {

            OrderAndDetailVM l_OrderAndDetailVM = Utility.OrderAndDetail;
            if (l_OrderAndDetailVM != null)
            {
                if (l_OrderAndDetailVM.l_TB_ORDER_DETAIL != null)
                {
                    if (l_OrderAndDetailVM.l_TB_ORDER_DETAIL.Count > 0)
                    {
                        return View(l_OrderAndDetailVM);
                    }
                    else
                    {
                        SetResultMessage("查無產品資訊");
                        return RedirectToAction("Index", "Index");
                    }

                }
                else
                {
                    SetResultMessage("查無產品資訊");
                    return RedirectToAction("Index", "Index");
                }
            }
            else
            {
                SetResultMessage("查無產品資訊");
                return RedirectToAction("Index", "Index");
            }




        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CheckoutDetail(string county, string district, string zipcode, string ADDRESS)
        {

            OrderAndDetailVM l_OrderAndDetailVM = Utility.OrderAndDetail;
            if (l_OrderAndDetailVM != null)
            {

                if (l_OrderAndDetailVM.l_TB_ORDER_DETAIL != null)
                {
                    if (l_OrderAndDetailVM.l_TB_ORDER_DETAIL.Count > 0)
                    {
                        l_OrderAndDetailVM.ADDRESS = ADDRESS;
                        l_OrderAndDetailVM.AREA = district;
                        l_OrderAndDetailVM.CITY = county;
                        l_OrderAndDetailVM.CREATE_TIME = DateTime.Now;
                        l_OrderAndDetailVM.CREATE_USER = Utility.UserData.USER_GUID;
                        l_OrderAndDetailVM.EDIT_TIME = DateTime.Now;
                        l_OrderAndDetailVM.EDIT_USER = Utility.UserData.USER_GUID;
                        l_OrderAndDetailVM.ORDER_GUID = Guid.NewGuid().ToString();
                        l_OrderAndDetailVM.ORDER_NO = Utility.getOrderNo(_PageContext);
                        l_OrderAndDetailVM.TOTAL_AMOUNT = l_OrderAndDetailVM.l_TB_ORDER_DETAIL.Sum(m => m.SUB_TOTAL);
                        l_OrderAndDetailVM.USER_GUID = Utility.UserData.USER_GUID;
                        l_OrderAndDetailVM.ZIPCODE = zipcode;
                        _PageContext.ServiceManager.TB_ORDERService.CreateOrderAndDetail(l_OrderAndDetailVM);
                        l_OrderAndDetailVM = null;
                        Utility.GenOrderAndOrderDetailCookieAndSessionInfo(l_OrderAndDetailVM);
                        SetResultMessage("訂單完成，後續我們將會與您聯絡。");
                        return RedirectToAction("Index", "Index");
                    }
                    else
                    {
                        SetResultMessage("查無產品資訊");
                        return RedirectToAction("Index", "Index");
                    }

                }
                else
                {
                    SetResultMessage("查無產品資訊");
                    return RedirectToAction("Index", "Index");
                }

            }
            else
            {
                SetResultMessage("查無產品資訊");
                return RedirectToAction("Index", "Index");
            }
        }
    }
}