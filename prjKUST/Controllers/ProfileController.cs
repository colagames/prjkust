﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVMLib.Controllers;
using TVMLib.dl;

namespace prjKUST.Controllers
{
    public class ProfileController : BaseController
    {
        // GET: Profile
        public new ActionResult Profile()
        {

            return View(_PageContext.ServiceManager.TB_PROFILEService.Search());
        }

        public ActionResult Download(string p_PROFILE_GUID)
        {
            TB_PROFILE l_data = _PageContext.ServiceManager.TB_PROFILEService.SearchByGUID(p_PROFILE_GUID);

            if (l_data != null)
            {
                //我要下載的檔案位置
                string filepath = l_data.PATH;
                //取得檔案名稱
                string filename = System.IO.Path.GetFileName(filepath);
                //讀成串流
                Stream iStream = new FileStream(filepath, FileMode.Open, FileAccess.Read, FileShare.Read);
                //SetResultMessage("檔案："+l_data.TITLE+"下載成功。");
                //回傳出檔案
                return File(iStream, "application/zip", filename);
            }
            else {
                SetResultMessage("資訊錯誤，請確認。");
                return RedirectToAction("Profile");
            }
           
        }
    }
}