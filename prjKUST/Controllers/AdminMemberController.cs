﻿using prjKUST.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVMLib.Controllers;
using TVMLib.dl;

namespace prjKUST.Controllers
{
    [AuthorizeAdminFilter]
    public class AdminMemberController : BaseController
    {
        // GET: AdminMember
        public ActionResult AdminMember()
        {
            return View(_PageContext.ServiceManager.TB_USERService.Search().Where(m => m.IS_ACTIVE = true).ToList());
        }
        public ActionResult Search(string p_KeyWork)
        {
            List<TB_USER> l_TB_USER = _PageContext.ServiceManager.TB_USERService.Search().Where(m => m.NAME.Contains(p_KeyWork)).ToList();
            SetResultMessage("搜尋關鍵字:「" + p_KeyWork + "」 成功");
            return View("AdminMember", l_TB_USER);
        }

        public ActionResult Delete(string p_USER_GUID)
        {
            TB_USER l_Data = _PageContext.ServiceManager.TB_USERService.SearchByGUID(p_USER_GUID);
            if (l_Data != null)
            {
                return PartialView("_Delete", l_Data);
            }
            else
            {
                SetResultMessage("查無資訊，請重新確認");
                return RedirectToAction("AdminMember");
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirm(string USER_GUID)
        {
            TB_USER l_Data = _PageContext.ServiceManager.TB_USERService.SearchByGUID(USER_GUID);
            if (l_Data != null)
            {
                _PageContext.ServiceManager.TB_USERService.Delete(l_Data);
                SetResultMessage(l_Data.NAME + " 刪除完成");
                return RedirectToAction("AdminMember");
            }
            else
            {
                SetResultMessage("查無資訊，請重新確認");
                return RedirectToAction("AdminMember");
            }

        }

        public ActionResult Edit(string p_USER_GUID)
        {
            TB_USER l_Data = _PageContext.ServiceManager.TB_USERService.SearchByGUID(p_USER_GUID);
            if (l_Data != null)
            {
                return PartialView("_Edit", l_Data);
            }
            else
            {
                SetResultMessage("查無資訊，請重新確認");
                return RedirectToAction("AdminMember");
            }

        }

    }
}