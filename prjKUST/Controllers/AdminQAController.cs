﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVMLib;
using TVMLib.Controllers;
using TVMLib.dl;

namespace prjKUST.Controllers
{
    public class AdminQAController : BaseController
    {
        // GET: AdminQA
        public ActionResult AdminQA()
        {
           ViewBag.SearchCategoryGroup = SearchCategoryGroup();
            return View(_PageContext.ServiceManager.TB_QAService.Search());
        }

        public ActionResult Add()
        {
            return PartialView("_Add");
        }

        public ActionResult Edit(string p_QA_GUID)
        {
            TB_QA l_Data = _PageContext.ServiceManager.TB_QAService.SearchByGUID(p_QA_GUID);
            if (l_Data != null)
            {
                return PartialView("_Edit", l_Data);
            }
            else
            {
                SetResultMessage("資料有誤，請再確認");
                return RedirectToAction("AdminQA");
            }

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(TB_QA p_Data)
        {
            if (ModelState.IsValidField("CATEGORY") && ModelState.IsValidField("TITLE") && ModelState.IsValidField("MAIN"))
            {


                p_Data.MAIN = HttpUtility.HtmlDecode(p_Data.MAIN);
                p_Data.CREATE_TIME = DateTime.Now;
                p_Data.CREATE_USER = Utility.UserData.USER_GUID;
                p_Data.EDIT_TIME = DateTime.Now;
                p_Data.EDIT_USER = Utility.UserData.USER_GUID;
                p_Data.QA_GUID = Guid.NewGuid().ToString();
                _PageContext.ServiceManager.TB_QAService.Create(p_Data);
                SetResultMessage("Q&A：" + p_Data.TITLE + " 新增完成");
                return RedirectToAction("AdminQA");

            }
            else
            {
                SetResultMessage("資料修改有誤，請確認。");
                return RedirectToAction("AdminQA");
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TB_QA p_Data)
        {
            if (ModelState.IsValidField("CATEGORY") && ModelState.IsValidField("TITLE") && ModelState.IsValidField("QA_GUID") && ModelState.IsValidField("MAIN"))
            {


                p_Data.EDIT_TIME = DateTime.Now;
                p_Data.EDIT_USER = Utility.UserData.USER_GUID;
                _PageContext.ServiceManager.TB_QAService.Update(p_Data);
                SetResultMessage("Q&A：" + p_Data.TITLE + " 修改完成");
                return RedirectToAction("AdminQA");

            }
            else
            {
                SetResultMessage("資料修改有誤，請確認。");
                return RedirectToAction("AdminQA");
            }
        }

        public ActionResult Delete(string p_QA_GUID)
        {
            TB_QA l_Data = _PageContext.ServiceManager.TB_QAService.SearchByGUID(p_QA_GUID);
            if (l_Data != null)
            {
                return PartialView("_Delete", l_Data);
            }
            else
            {
                SetResultMessage("查無資訊，請重新確認");
                return RedirectToAction("AdminNews");
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirm(string QA_GUID)
        {
            TB_QA l_Data = _PageContext.ServiceManager.TB_QAService.SearchByGUID(QA_GUID);
            if (l_Data != null)
            {
                _PageContext.ServiceManager.TB_QAService.Delete(l_Data);
                SetResultMessage(l_Data.TITLE + " 刪除完成");
                return RedirectToAction("AdminQA");
            }
            else
            {
                SetResultMessage("查無資訊，請重新確認");
                return RedirectToAction("AdminQA");
            }

        }

        public JsonResult SearchCategoryGroup()
        {
            List<TB_QA> l_data = _PageContext.ServiceManager.TB_QAService.SearchCategoryGroup();

            List<string> l_string = new List<string>();
            foreach (var item in l_data) {
                l_string.Add(item.CATEGORY);
            }
        
            return Json(l_string.DefaultIfEmpty(), JsonRequestBehavior.AllowGet);
        }
    }
}