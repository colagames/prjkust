﻿using prjKUST.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TVMLib.Controllers;

namespace prjKUST.Controllers
{
    [AuthorizeAdminFilter]
    public class AdminIndexController : BaseController
    {
        // GET: AdminIndex
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Logout()
        {
            this.ControllerContext.HttpContext.Session.RemoveAll();
            var l_Cookie = new HttpCookie(FormsAuthentication.FormsCookieName)
            {
                Expires = DateTime.Now.AddDays(-1),
                HttpOnly = true
            };
            Response.Cookies.Add(l_Cookie);
            SetResultMessage("登出成功");
            return RedirectToAction("AdminLogin", "AdminLogin");
        }
    }
}