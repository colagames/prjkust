﻿using prjKUST.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVMLib;
using TVMLib.Controllers;
using TVMLib.dl;
using TVMLib.dl.AdminProCategory;

namespace prjKUST.Controllers
{
    [AuthorizeAdminFilter]
    public class AdminProCategoryController : BaseController
    {
        // GET: AdminProCategory
        public ActionResult AdminProCategory()
        {
            return View(_PageContext.ServiceManager.TB_PRODUCT_CATEGORYService.Search());
        }


        public ActionResult Add()
        {
            return PartialView("_Add");
        }
        public ActionResult Edit(string p_CATEGORY_GUID)
        {
            TB_PRODUCT_CATEGORY l_Data = _PageContext.ServiceManager.TB_PRODUCT_CATEGORYService.SearchByGUID(p_CATEGORY_GUID);

            if (l_Data != null)
            {
                AdminProCategoryVM l_AdminProCategoryVM = new AdminProCategoryVM();
                l_AdminProCategoryVM.CATEGORY_GUID = l_Data.CATEGORY_GUID;
                l_AdminProCategoryVM.CREATE_TIME = l_Data.CREATE_TIME;
                l_AdminProCategoryVM.CREATE_USER = l_Data.CREATE_USER;
                l_AdminProCategoryVM.EDIT_TIME = l_Data.EDIT_TIME;
                l_AdminProCategoryVM.EDIT_USER = l_Data.EDIT_USER;
                l_AdminProCategoryVM.IS_DELETE = l_Data.IS_DELETE;
                l_AdminProCategoryVM.NAME = l_Data.NAME;
                l_AdminProCategoryVM.NOTE = l_Data.NOTE;
                l_AdminProCategoryVM.PIC_PATH = l_Data.PIC_PATH;
    
           
                return PartialView("_Edit", l_AdminProCategoryVM);
            }
            SetResultMessage("資料有誤，請重新確認");
            return RedirectToAction("AdminProfile");
        }
        public ActionResult Delete(string p_CATEGORY_GUID)
        {
            TB_PRODUCT_CATEGORY l_Data = _PageContext.ServiceManager.TB_PRODUCT_CATEGORYService.SearchByGUID(p_CATEGORY_GUID);
            if (l_Data != null)
            {
                return PartialView("_Delete", l_Data);
            }
            else
            {
                SetResultMessage("查無資訊，請重新確認");
                return RedirectToAction("AdminProCategory");
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirm(string CATEGORY_GUID)
        {
            TB_PRODUCT_CATEGORY l_Data = _PageContext.ServiceManager.TB_PRODUCT_CATEGORYService.SearchByGUID(CATEGORY_GUID);
            if (l_Data != null)
            {
                l_Data.IS_DELETE = true;
                _PageContext.ServiceManager.TB_PRODUCT_CATEGORYService.Delete(l_Data);
                SetResultMessage(l_Data.NAME + " 刪除完成");
                return RedirectToAction("AdminProCategory");
            }
            else
            {
                SetResultMessage("查無資訊，請重新確認");
                return RedirectToAction("AdminProCategory");
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(AdminProCategoryVM p_Data)
        {
            string l_FullFilePath1 = "";

            if (ModelState.IsValidField("PATH_UPLOAD") && ModelState.IsValidField("NAME"))
            {
                string l_Msg = "上傳失敗";

                if (p_Data.PIC_PATH_UPLOAD != null)
                {
                    if (!Utility.SaveFile(p_Data.PIC_PATH_UPLOAD, out l_FullFilePath1, p_Data.PIC_PATH, p_PreName: p_Data.PIC_PATH_UPLOAD.FileName))
                    {
                        SetResultMessage(l_Msg); return View(p_Data);
                    }
                }

                TB_PRODUCT_CATEGORY l_TB_PRODUCT_CATEGORY = new TB_PRODUCT_CATEGORY();

                l_TB_PRODUCT_CATEGORY.IS_DELETE = false;
                l_TB_PRODUCT_CATEGORY.NOTE = p_Data.NOTE;
                l_TB_PRODUCT_CATEGORY.CREATE_TIME = DateTime.Now;
                l_TB_PRODUCT_CATEGORY.CREATE_USER = Utility.UserData.USER_GUID;
                l_TB_PRODUCT_CATEGORY.EDIT_TIME = DateTime.Now;
                l_TB_PRODUCT_CATEGORY.EDIT_USER = Utility.UserData.USER_GUID;
                l_TB_PRODUCT_CATEGORY.CATEGORY_GUID = Guid.NewGuid().ToString();
                l_TB_PRODUCT_CATEGORY.PIC_PATH = l_FullFilePath1 == "" ? p_Data.PIC_PATH : l_FullFilePath1;
                _PageContext.ServiceManager.TB_PRODUCT_CATEGORYService.Create(l_TB_PRODUCT_CATEGORY);
                SetResultMessage("產品類別：" + p_Data.NAME + " 新增完成");
                return RedirectToAction("AdminProCategory");
            }
            SetResultMessage("上傳檔案或資料有誤有誤，請重新確認");
            return View(p_Data);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AdminProCategoryVM p_Data)
        {
            string l_FullFilePath1 = "";

            if (ModelState.IsValidField("PATH_UPLOAD") && ModelState.IsValidField("TITLE"))
            {
                string l_Msg = "上傳失敗";

                if (p_Data.PIC_PATH_UPLOAD != null)
                {
                    if (!Utility.SaveFile(p_Data.PIC_PATH_UPLOAD, out l_FullFilePath1, p_Data.PIC_PATH, p_PreName: p_Data.PIC_PATH_UPLOAD.FileName))
                    {
                        SetResultMessage(l_Msg); return View(p_Data);
                    }
                }
                TB_PRODUCT_CATEGORY l_TB_PRODUCT_CATEGORY = new TB_PRODUCT_CATEGORY();
                l_TB_PRODUCT_CATEGORY.EDIT_TIME = DateTime.Now;
                l_TB_PRODUCT_CATEGORY.EDIT_USER = Utility.UserData.USER_GUID;
                l_TB_PRODUCT_CATEGORY.IS_DELETE = p_Data.IS_DELETE;
                l_TB_PRODUCT_CATEGORY.NAME = p_Data.NAME;
                l_TB_PRODUCT_CATEGORY.NOTE = p_Data.NOTE;
                l_TB_PRODUCT_CATEGORY.PIC_PATH = l_FullFilePath1 == "" ? p_Data.PIC_PATH : l_FullFilePath1;
                l_TB_PRODUCT_CATEGORY.CATEGORY_GUID = p_Data.CATEGORY_GUID;
                _PageContext.ServiceManager.TB_PRODUCT_CATEGORYService.Update(l_TB_PRODUCT_CATEGORY);

                SetResultMessage("產品類別：" + p_Data.NAME + " 修改完成");
                return RedirectToAction("AdminProCategory");

            }

         

            SetResultMessage("資料有誤，請重新確認");
            return View("AdminProCategory");
        }
    }
}