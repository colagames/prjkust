﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVMLib;
using TVMLib.Controllers;
using TVMLib.dl;
using TVMLib.dl.Login;

namespace prjKUST.Controllers
{
    public class AdminLoginController : BaseController
    {
        // GET: AdminLogin
        public ActionResult AdminLogin()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginVM p_Data)
        {
            if (ModelState.IsValidField("ACCOUNT") && ModelState.IsValidField("PASSWORD"))
            {
                TB_USER l_TB_USER = _PageContext.ServiceManager.TB_USERService.SearchAdminByAccAndPwd(p_Data.ACCOUNT, p_Data.PASSWORD);
                if (l_TB_USER != null)
                {
                    //登入成功
                    Utility.GenUserInfoCookieAndSession(l_TB_USER);
                   
                    return RedirectToAction("Index", "AdminIndex");
                }
                else
                {
                    SetResultMessage("帳號或密碼有誤，請重新確認。");
                }
            }

            SetResultMessage("請確認登入資訊");
            return RedirectToAction("AdminLogin");
        }
    }
}