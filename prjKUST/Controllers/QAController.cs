﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVMLib.Controllers;
using TVMLib.dl;
using TVMLib.dl.QA;

namespace prjKUST.Controllers
{
    public class QAController : BaseController
    {
        // GET: QA
        public ActionResult QA()
        {

            List<TB_QA> l_TB_QA = _PageContext.ServiceManager.TB_QAService.SearchCategoryGroup();

            List<QAVM> l_QAVM = new List<QAVM>();

            foreach (var item in l_TB_QA)
            {
                QAVM l_data = new QAVM();
                l_data.CATEGORY = item.CATEGORY;
                l_data.l_TB_QA = _PageContext.ServiceManager.TB_QAService.SearchByCATEGORY(l_data.CATEGORY);
                l_QAVM.Add(l_data);
            }

            return View(l_QAVM);
        }


        public ActionResult Search(string p_KeyWord)
        {

            List<TB_QA> l_TB_QA = _PageContext.ServiceManager.TB_QAService.SearchCategoryGroupByKeyWord(p_KeyWord);

            List<QAVM> l_QAVM = new List<QAVM>();

            foreach (var item in l_TB_QA)
            {
                QAVM l_data = new QAVM();
                l_data.CATEGORY = item.CATEGORY;
                l_data.l_TB_QA = _PageContext.ServiceManager.TB_QAService.SearchByKeyword(l_data.CATEGORY, p_KeyWord);
                l_QAVM.Add(l_data);
            }
            return View("QA",l_QAVM);
        }
    }
}