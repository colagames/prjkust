﻿using prjKUST.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVMLib.Controllers;
using TVMLib.dl;

namespace prjKUST.Controllers
{ 
    public class AboutusController : BaseController
    {
        // GET: About
        public ActionResult Aboutus()
        {
            TB_ABOUTUS l_TB_ABOUTUS = _PageContext.ServiceManager.TB_ABOUTUSService.SearchByGUID("admin_init");
            return View(l_TB_ABOUTUS);
        }
    }
}