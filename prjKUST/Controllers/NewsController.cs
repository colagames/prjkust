﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVMLib.Controllers;
using TVMLib.dl;

namespace prjKUST.Controllers
{
    public class NewsController : BaseController
    {
        // GET: News
        public ActionResult News()
        {
            return View(_PageContext.ServiceManager.TB_NEWSService.Search());
        }

        public ActionResult Detail(string p_NEWS_GUID)
        {
            if (!string.IsNullOrEmpty(p_NEWS_GUID))
            {
                TB_NEWS l_TB_NEWS = _PageContext.ServiceManager.TB_NEWSService.SearchByGUID(p_NEWS_GUID);

                if (l_TB_NEWS != null)
                {
                    return View(l_TB_NEWS);
                }
                else {
                    SetResultMessage("查無相關消息，請確認");
                    return RedirectToAction("News");
                }
              
            }
            else
            {
                SetResultMessage("資料有誤，請確認");
                return RedirectToAction("News");
            }



        }
    }
}