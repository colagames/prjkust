﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVMLib;
using TVMLib.Controllers;
using TVMLib.dl;
using TVMLib.dl.Index;

namespace prjKUST.Controllers
{
    public class IndexController : BaseController
    {
        // GET: Index
        public ActionResult Index()
        {
            IndexVM l_Data = new IndexVM();
            l_Data.l_HotProductVM = Utility.getHotProduct(_PageContext);
            l_Data.l_TB_NEWS = _PageContext.ServiceManager.TB_NEWSService.Search();
            l_Data.l_TB_PRODUCT_CATEGORY = _PageContext.ServiceManager.TB_PRODUCT_CATEGORYService.Search();
            return View(l_Data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Index(IndexVM p_Data)
        {
        
            if (ModelState.IsValidField("CNAME") && ModelState.IsValidField("CEMAIL") && ModelState.IsValidField("CMAIN"))
            {
                TB_CONTACT l_Data = new TB_CONTACT();
                l_Data.NAME = p_Data.CNAME;
                l_Data.MAIN = p_Data.CMAIN;
                l_Data.EMAIL = p_Data.CEMAIL;
                l_Data.CREATE_TIME = DateTime.Now;
                l_Data.CONTACT_GUID = Guid.NewGuid().ToString();
                _PageContext.ServiceManager.TB_CONTACTService.Create(l_Data);
                SetResultMessage("留言成功，我們將會儘速聯絡您，謝謝！");
                return RedirectToAction("Index");
            }

            SetResultMessage("留言資料有誤，請確認。");
            return RedirectToAction("Index", p_Data);
        }
    }
}