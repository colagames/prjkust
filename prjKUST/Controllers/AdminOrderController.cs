﻿using prjKUST.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVMLib.Controllers;
using TVMLib.dl.Order;

namespace prjKUST.Controllers
{
    [AuthorizeAdminFilter]
    public class AdminOrderController : BaseController
    {
        // GET: AdminOrder
        public ActionResult AdminOrder()
        {
            return View(_PageContext.ServiceManager.TB_ORDERService.SearchAllOrderAndDetail(""));
        }

        public ActionResult Search(string p_KeyWord)
        {
            List<OrderAndDetailVM> l_OrderAndDetailVM = _PageContext.ServiceManager.TB_ORDERService.SearchAllOrderAndDetail(p_KeyWord);
         
            return View("AdminOrder", l_OrderAndDetailVM);
        }

        public ActionResult AdminOrderDetail(string p_ORDER_GUID)
        {
            OrderAndDetailVM l_OrderAndDetailVM = _PageContext.ServiceManager.TB_ORDERService.SearchAllOrderAndDetailByORDER_GUID(p_ORDER_GUID);

            return View("AdminOrderDetail", l_OrderAndDetailVM);
        }
    }
}