﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace prjKUST.Filters
{
    /// <summary>
    /// AJAX AntiForgeryToken
    /// Reference: http://kevintsengtw.blogspot.com/2013/09/aspnet-mvc-csrf-ajax-antiforgerytoken.html
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class AjaxValidateAntiForgeryTokenAttribute : FilterAttribute, IAuthorizationFilter
    {
        /// <summary>
        /// 實作 IAuthorizationFilter 的 OnAuthorization
        /// </summary>
        /// <param name="p_FilterContext"></param>
        public void OnAuthorization(AuthorizationContext p_FilterContext)
        {
            try
            {
                if (p_FilterContext.HttpContext.Request.IsAjaxRequest())
                {
                    ValidateRequestHeader(p_FilterContext.HttpContext.Request);
                }
                else
                {
                    p_FilterContext.HttpContext.Response.StatusCode = 404;
                    p_FilterContext.Result = new HttpNotFoundResult();
                }
            }
            catch (HttpAntiForgeryException p_EX)
            {
                NLog.LogManager.GetCurrentClassLogger().Error(p_EX);
                throw new HttpAntiForgeryException("Anti forgery token cookie not found");
            }
        }

        private void ValidateRequestHeader(HttpRequestBase p_Request)
        {
            string l_CookieToken = String.Empty;
            string l_FormToken = String.Empty;
            string l_TokenValue = p_Request.Headers["RequestVerificationToken"];
            if (!String.IsNullOrEmpty(l_TokenValue))
            {
                string[] tokens = l_TokenValue.Split(':');
                if (tokens.Length == 2)
                {
                    l_CookieToken = tokens[0].Trim();
                    l_FormToken = tokens[1].Trim();
                }
            }
            AntiForgery.Validate(l_CookieToken, l_FormToken);
        }

    }
}