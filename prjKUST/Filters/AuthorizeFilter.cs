﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TVMLib.dl;
using System.Reflection;
using TVMLib;
using TVMLib.common;
using System.Web.Routing;

namespace prjKUST.Filters
{
    /// <summary>
    /// 權限控制
    /// </summary>
    public class AuthorizeAdminFilter : AuthorizeAttribute
    {
        /// <summary>
        /// 取得 PageContext 物件
        /// </summary>
        private TVMLib.common.PageContext _PageContext { get; set; }

        /// <summary>
        /// 驗證狀態
        /// </summary>
        protected bool _Status { get; set; }

        /// <summary>
        /// 實作 IAuthorizationFilter 的 OnAuthorization方法
        /// </summary>
        /// <param name="p_FilterContext">AuthorizationContext 物件</param>
        public override void OnAuthorization(AuthorizationContext p_FilterContext)
        {
            _Status = false; //預設不通過

            if (p_FilterContext.ActionDescriptor.GetCustomAttributes(typeof(SkipAuthorizeFilterAttribute), false).Any())
                return;

            HttpCookie l_AuthCookie = p_FilterContext.HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (l_AuthCookie != null)
            {
                l_AuthCookie.HttpOnly = true;
                FormsAuthenticationTicket l_AuthTicket = FormsAuthentication.Decrypt(l_AuthCookie.Value);
                if (p_FilterContext.HttpContext.Session[l_AuthTicket.Name] != null)
                {
                    TB_USER l_CurrentUser = p_FilterContext.HttpContext.Session[l_AuthTicket.Name] as TB_USER; //取得會員資料
                    if (l_CurrentUser.IS_ADMIN)
                        _Status = true;
                }
            }

            if (!_Status)
            {
                //var l_JsonResult = new JsonResult();
                //l_JsonResult.Data = new { ErrorMessage = "權限不足" };
                //l_JsonResult.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
                //p_FilterContext.Result = l_JsonResult;     

                p_FilterContext.Controller.TempData[SealedGlobalPage.RESULT_MESSAGE] = "權限未被啟用，請在確認。";
                p_FilterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "AdminLogin", action = "AdminLogin" }));
            }

            //if (!_Status)
            //{
            //    if (!CheckIsJsonResult(p_FilterContext))
            //    {
            //        bool l_FromAjax = p_FilterContext.HttpContext.Request.Headers.Get("X-Requested-With") == "XMLHttpRequest";
            //        p_FilterContext.Result = new RedirectResult(l_LibUtility.GetUrlByControlAndActName((!l_FromAjax ? "Index" : "_AjaxNotAuthPartial"), "Login"));
            //    }
            //    else
            //    {
            //        var l_JsonResult = new JsonResult();
            //        l_JsonResult.Data = new { ErrorMessage = "權限不足" };
            //        l_JsonResult.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            //        p_FilterContext.Result = l_JsonResult;
            //    }

            //從Startup.Auth
            //p_FilterContext.Result = new HttpUnauthorizedResult(); 

            //從WebConfig
            //p_FilterContext.Result = new RedirectResult(p_Root + FormsAuthentication.LoginUrl); 
            //}
        }

        /// <summary>
        /// 檢查回傳類型是否為JsonResult
        /// </summary>
        /// <param name="p_FilterContext"></param>
        /// <returns></returns>
        private bool CheckIsJsonResult(AuthorizationContext p_FilterContext)
        {
            bool l_Result = false;
            try
            {
                string l_CurAction = p_FilterContext.ActionDescriptor.ActionName;
                int l_CutActAttrCount = p_FilterContext.ActionDescriptor.GetCustomAttributes(false).Count();
                var l_Methods = p_FilterContext.Controller.GetType().GetMethods().Where(m => m.Name == l_CurAction && m.CustomAttributes.Count() == l_CutActAttrCount);
                l_Result = l_Methods.FirstOrDefault().ReturnType.Name == nameof(JsonResult);
            }
            catch (Exception p_EX)
            {
                NLog.LogManager.GetCurrentClassLogger().Error(p_EX);
            }
            return l_Result;
        }



    }
}