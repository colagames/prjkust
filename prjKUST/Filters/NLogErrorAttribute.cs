﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace prjKUST.Filters
{
    /// <summary>
    /// NLOG錯誤記錄 (在FilterConfig進行套用)
    /// </summary>
    public class NLogErrorAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext p_filterContext)
        {
            NLog.LogManager.GetCurrentClassLogger().Error(p_filterContext.Exception);
            base.OnException(p_filterContext);
        }
    }
}