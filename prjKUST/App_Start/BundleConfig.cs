﻿using System.Web;
using System.Web.Optimization;

namespace prjKUST
{
    public class BundleConfig
    {
        // 如需統合的詳細資訊，請瀏覽 https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/twzipcode").Include(
               "~/Scripts/jquery.twzipcode.min.js"));


            // 使用開發版本的 Modernizr 進行開發並學習。然後，當您
            // 準備好可進行生產時，請使用 https://modernizr.com 的建置工具，只挑選您需要的測試。
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/allCustomScript").Include(
                                            //"~/Scriptsbootstrap//bootstrap.min.js",
                                            //"~/Scripts/jquery.mb.YTPlayer.min.js",
                                            //"~/Scripts/appear.js",
                                            //"~/Scripts/jquery.simple-text-rotator.min.js",
                                            //"~/Scripts/jqBootstrapValidation.js",
                                            //"http://maps.google.com/maps/api/js?sensor=true",
                                            //"~/Scripts/gmaps.js",
                                            //"~/Scripts/isotope.pkgd.min.js",
                                            //"~/Scripts/imagesloaded.pkgd.js",
                                            //"~/Scripts/jquery.flexslider-min.js",
                                            //"~/Scripts/jquery.magnific-popup.min.js",
                                            //"~/Scripts/jquery.fitvids.js",
                                            //"~/Scripts/smoothscroll.js",
                                            //"~/Scripts/wow.min.js",
                                            //"~/Scripts/owl.carousel.min.js",
                                            //"~/Scripts/contact.js",
                                            //"~/Scripts/custom.js"
                      ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                                        //"~/Content/bootstrap.min.css",
                                        //"~/Content/simpletextrotator.css",
                                        //"~/Content/font-awesome.min.css",
                                        //"~/Content/et-line-font.css",
                                        //"~/Content/magnific-popup.css",
                                        //"~/Content/flexslider.css",
                                        //"~/Content/owl.carousel.css",
                                        //"~/Content/animate.css",
                                        //"~/Content/style.css"
                      ));



        }
    }
}
