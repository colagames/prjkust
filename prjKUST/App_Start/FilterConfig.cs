﻿using prjKUST.Filters;
using System.Web;
using System.Web.Mvc;

namespace prjKUST
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection p_Filters)
        {
            p_Filters.Add(new NLogErrorAttribute());
            p_Filters.Add(new HandleErrorAttribute());
            //p_Filters.Add(new AuthorizeFilter());
         
        }
    }
}
