﻿using BaseLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVMLib.Service;

namespace TVMLib
{
    public class ServiceManager
    {
        private BaseContext _BaseContext;

        public ServiceManager(BaseContext p_context)
        {
            _BaseContext = p_context;
        }

        #region 請於下方區塊 註冊客製Service物件

        private TB_USERService _TB_USERService;
        public TB_USERService TB_USERService
        {
            get
            {
                if (_TB_USERService == null)
                    _TB_USERService = new TB_USERService(_BaseContext);
                return _TB_USERService;
            }
        }

        private TB_NEWSService _TB_NEWSService;
        public TB_NEWSService TB_NEWSService
        {
            get
            {
                if (_TB_NEWSService == null)
                    _TB_NEWSService = new TB_NEWSService(_BaseContext);
                return _TB_NEWSService;
            }
        }

        private TB_PRODUCT_CATEGORYService _TB_PRODUCT_CATEGORYService;
        public TB_PRODUCT_CATEGORYService TB_PRODUCT_CATEGORYService
        {
            get
            {
                if (_TB_PRODUCT_CATEGORYService == null)
                    _TB_PRODUCT_CATEGORYService = new TB_PRODUCT_CATEGORYService(_BaseContext);
                return _TB_PRODUCT_CATEGORYService;
            }
        }
        private TB_PRODUCTService _TB_PRODUCTService;
        public TB_PRODUCTService TB_PRODUCTService
        {
            get
            {
                if (_TB_PRODUCTService == null)
                    _TB_PRODUCTService = new TB_PRODUCTService(_BaseContext);
                return _TB_PRODUCTService;
            }
        }

        private TB_INQUIRYService _TB_INQUIRYService;
        public TB_INQUIRYService TB_INQUIRYService
        {
            get
            {
                if (_TB_INQUIRYService == null)
                    _TB_INQUIRYService = new TB_INQUIRYService(_BaseContext);
                return _TB_INQUIRYService;
            }
        }

        private TB_PROFILEService _TB_PROFILEService;
        public TB_PROFILEService TB_PROFILEService
        {
            get
            {
                if (_TB_PROFILEService == null)
                    _TB_PROFILEService = new TB_PROFILEService(_BaseContext);
                return _TB_PROFILEService;
            }
        }
        private TB_CONTACTService _TB_CONTACTService;
        public TB_CONTACTService TB_CONTACTService
        {
            get
            {
                if (_TB_CONTACTService == null)
                    _TB_CONTACTService = new TB_CONTACTService(_BaseContext);
                return _TB_CONTACTService;
            }
        }

        private TB_QAService _TB_QAService;
        public TB_QAService TB_QAService
        {
            get
            {
                if (_TB_QAService == null)
                    _TB_QAService = new TB_QAService(_BaseContext);
                return _TB_QAService;
            }
        }

        private TB_ORDERService _TB_ORDERService;
        public TB_ORDERService TB_ORDERService
        {
            get
            {
                if (_TB_ORDERService == null)
                    _TB_ORDERService = new TB_ORDERService(_BaseContext);
                return _TB_ORDERService;
            }
        }

        private TB_ORDER_DETAILService _TB_ORDER_DETAILService;
        public TB_ORDER_DETAILService TB_ORDER_DETAILService
        {
            get
            {
                if (_TB_ORDER_DETAILService == null)
                    _TB_ORDER_DETAILService = new TB_ORDER_DETAILService(_BaseContext);
                return _TB_ORDER_DETAILService;
            }
        }
        private TB_SLIDERService _TB_SLIDERService;
        public TB_SLIDERService TB_SLIDERService
        {
            get
            {
                if (_TB_SLIDERService == null)
                    _TB_SLIDERService = new TB_SLIDERService(_BaseContext);
                return _TB_SLIDERService;
            }
        }
        private TB_ABOUTUSService _TB_ABOUTUSService;
        public TB_ABOUTUSService TB_ABOUTUSService
        {
            get
            {
                if (_TB_ABOUTUSService == null)
                    _TB_ABOUTUSService = new TB_ABOUTUSService(_BaseContext);
                return _TB_ABOUTUSService;
            }
        }
        
        #endregion
    }
}
