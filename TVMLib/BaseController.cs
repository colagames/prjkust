﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Reflection;
using TVMLib.common;
using TVMLib.dl;

namespace TVMLib.Controllers
{
    public class BaseController : Controller
    {

        protected PageContext _PageContext;
   
        public BaseController(TB_USER p_User = null)
        {
            //這裡不需要特意 singleton 因為在建構時 _context一定是Null，故至getContext判斷Context的Session內是否為空，如為空在實體化出來
            this._PageContext = SealedGlobalPage.getContext(new ViewPage());
            ViewBag.PRODUCT_MENU = Utility.getProductMenu(this._PageContext);
            ViewBag.ORDER = Utility.OrderAndDetail;
        }


        /// <summary>
        /// 設置系統訊息
        /// </summary>
        protected void SetResultMessage(string p_Message)
        {
            TempData[SealedGlobalPage.RESULT_MESSAGE] = p_Message;
        }

        /// <summary>
        /// 抓取系統訊息
        /// </summary>
        protected string GetResultMessage()
        {
            string l_result = string.Empty;
            l_result = Session[SealedGlobalPage.RESULT_MESSAGE] == null ? "" : Session[SealedGlobalPage.RESULT_MESSAGE].ToString();
            return l_result;
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //string actionName = filterContext.ActionDescriptor.ActionName;
            //string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            //if (controllerName != "Login")
            //{
            //    if (Session[SealedGlobalPage.SESSIONKEY_LOGIN] == null)
            //    {
            //        filterContext.Result = new RedirectResult(Url.Action("Index", "Login", new { msg = "請先登入帳號" }));
            //    }
            //    else
            //    {
            //        _CINFO_ACCOUNT = Session[SealedGlobalPage.SESSIONKEY_LOGIN] as CINFO_ACCOUNT;
            //        //refresh
            //        Session.Remove(SealedGlobalPage.SESSIONKEY_LOGIN);
            //        Session.Add(SealedGlobalPage.SESSIONKEY_LOGIN, _CINFO_ACCOUNT);
            //        getAuthorization(filterContext.Controller, controllerName);
            //    }
            //}

            //base.OnActionExecuting(filterContext);
        }


    }
}