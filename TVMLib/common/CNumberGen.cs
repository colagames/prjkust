using System;
using System.Data;
using System.Collections;

using System.Collections.Generic;
using TVMLib.dl;
using System.Text;

namespace TVMLib.common
{
    /// <summary>
    /// CNumberGen 的摘要描述。
    /// </summary>
    public class CNumberGen
    {
        public static string TypeName
        {
            get
            {
                return "TVMLib.common.CNumberGen";
            }
        }
        private string queryBySql( string p_strType, string p_strKey, PageContext p_context, int p_int狀態)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_TEMP where fType=@fType and fKey=@fKey ");
            p_context._Dbm.AddParameter("@fType", p_strType);
            p_context._Dbm.AddParameter("@fKey", p_strKey);
            List<TB_TEMP> l_TB_TEMP = p_context._Dbm.ExecuteReader<TB_TEMP>(sb.ToString());


            int l_intCode = 1;
            string l_strSql = "";
            if (l_TB_TEMP.Count == 0)
            {
                l_strSql = "INSERT INTO TB_TEMP(fid,fType,fName,fKey,fCode) VALUES (0";
                l_strSql += " ,'" + p_strType + "'";
                l_strSql += " ,'"+ p_strType + "' ";
                l_strSql += " ,'" + p_strKey + "'";
                l_strSql += " ,'" + l_intCode.ToString("000") + "')";
                p_context._Dbm.EexcuteSqlNonquery(l_strSql);
            }
            else
            {
                if (p_int狀態 == 0)
                {
                    l_intCode = Convert.ToInt32(l_TB_TEMP[0].fCode.ToString());     //List<>直接給[0].欄位名稱即可
                }
                else
                {
                    l_intCode = Convert.ToInt32(l_TB_TEMP[0].fCode.ToString()) + 1;
                    l_strSql = "UPDATE TB_TEMP SET fCode='" + l_intCode.ToString("000") + "' ";
                    l_strSql += "WHERE fType='" + p_strType + "' AND fKey='" + p_strKey + "'";
                    p_context._Dbm.EexcuteSqlNonquery(l_strSql);
                }
            }
            //return p_strKey + Convert.ToString(l_intCode, 16);//轉成16進位∼∼不讓人看出多少單子
            return p_strKey + l_intCode.ToString("000");
        }


        /// <summary>
        /// 取得訂單編號
        /// </summary>
        /// <param name="p_context"></param>
        /// <param name="p_int狀態"></param>
        /// <param name="p_strStoreID"></param>
        /// <returns></returns>
        public string getOrderNo(PageContext p_context, int p_int狀態)
        {
            //string l_strFKEY = DateTime.Today.ToString("yyyyMMdd");
            string l_strFKEY = DateTime.Today.ToString("yyyyMMdd");    
   

            return queryBySql( "進貨編號", l_strFKEY, p_context, p_int狀態) + "A";
        }


    }
}
