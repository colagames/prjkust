﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TVMLib.dl;

namespace TVMLib.common
{
    /// <summary>
    /// Email處理
    /// </summary>
    public class EmailHandle
    {
        private string _MailGuid { get; set; }
        private string _Subject { get; set; }
        private string _Content { get; set; }
        public string ErrMsg { get; set; }
        public bool IsSuccess { get; set; }


        public EmailHandle()
        {
        }
        public EmailHandle(string p_MailGuid)
        {
            _MailGuid = p_MailGuid;
        }

        /// <summary>
        /// 發送信件
        /// </summary>
        /// <param name="p_MailTo">發送對象電子郵件位址</param>
        /// <param name="p_Subject">主旨</param>
        /// <param name="p_Body">內文</param>
        /// <param name="p_AttachmentPaths">附件Path List</param>
        /// <param name="l_Msg">T錯誤訊息</param>
        /// <param name="p_UserData">使用者資料</param>
        /// <param name="p_BodyIsHtml">內文是否為Html格式(預設為true)</param>
        /// <returns></returns>
        public bool SendMail(List<string> p_MailTo, string p_Subject, string p_Body, List<string> p_AttachmentPaths,
                             ref string l_Msg, TB_USER p_UserData, bool p_BodyIsHtml = true)
        {
            if (p_MailTo == null)
            {
                l_Msg = "發送信件失敗：發送EMAIL ADDRESS資料為NULL";
                NLog.LogManager.GetCurrentClassLogger().Error(l_Msg);
                return false;
            }
            if (p_MailTo.Count == 0)
            {
                l_Msg = "發送信件失敗：沒有可發送的EMAIL ADDRESS";
                NLog.LogManager.GetCurrentClassLogger().Warn(l_Msg);
                return false;
            }

            MailMessage l_MailMsg = new MailMessage();
            bool l_IsSuccess = false;

            p_AttachmentPaths = p_AttachmentPaths ?? new List<string>();

            try
            {
                l_MailMsg.To.Add(string.Join(",", p_MailTo));
                l_MailMsg.From = new MailAddress(LoadMailServerConfig.MailFrom, LoadMailServerConfig.MailSender, System.Text.Encoding.UTF8);
                //郵件標題 
                l_MailMsg.Subject = p_Subject;
                //郵件標題編碼  
                l_MailMsg.SubjectEncoding = System.Text.Encoding.UTF8;
                //郵件內容
                l_MailMsg.Body = p_Body;
                l_MailMsg.IsBodyHtml = p_BodyIsHtml;
                l_MailMsg.BodyEncoding = System.Text.Encoding.UTF8;//郵件內容編碼 
                l_MailMsg.Priority = MailPriority.Normal;//郵件優先級 

                //有夾帶檔案
                foreach (var l_Item in p_AttachmentPaths)
                {
                    Attachment l_File = new Attachment(l_Item.Trim());
                    //加入信件的夾帶檔案
                    l_MailMsg.Attachments.Add(l_File);
                }

                //建立 SmtpClient 物件 並設定 Gmail的smtp主機及Port
                using (SmtpClient l_SmtpClient = new SmtpClient(LoadMailServerConfig.MailHost, LoadMailServerConfig.MailPort))
                {
                    //設定帳號密碼
                    l_SmtpClient.Credentials = new System.Net.NetworkCredential(LoadMailServerConfig.MailAccount, LoadMailServerConfig.MailPassword);
                    //啟用 SSL
                    l_SmtpClient.EnableSsl = LoadMailServerConfig.MailEnableSSL;
                    l_SmtpClient.Send(l_MailMsg);

                    //釋放每個附件，才不會Lock住
                    if (l_MailMsg.Attachments != null && l_MailMsg.Attachments.Count > 0)
                    {
                        for (int i = 0; i < l_MailMsg.Attachments.Count; i++)
                        {
                            l_MailMsg.Attachments[i].Dispose();
                        }
                    }
                };

                l_IsSuccess = true;
            }
            catch (Exception p_EX)
            {
                NLog.LogManager.GetCurrentClassLogger().Error(p_EX);
                l_Msg = p_EX.ToString();
                l_IsSuccess = false;
            }
            finally
            {
                l_MailMsg.Dispose();
            }

            return l_IsSuccess;
        }
    }
}
