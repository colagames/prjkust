﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVMLib.common
{
    /// <summary>
    /// 取得Mail Server設定 (從Web.config)
    /// </summary>
    public class LoadMailServerConfig
    {
        /// <summary>
        /// SMTP 伺服器
        /// </summary>
        public static string MailHost
        {
            get
            {
                try
                {
                    return ConfigurationManager.AppSettings["MailHost"].ToString() ?? string.Empty;
                }
                catch (NullReferenceException p_EX)
                {
                    NLog.LogManager.GetCurrentClassLogger().Error(p_EX);
                    throw new NullReferenceException("執行Excel匯出並讀取 WebConfig 的 MailHost 參數時發生沒有參考的錯誤、請確認 WebConfig 是否有加入");
                }
            }
        }
        /// <summary>
        /// 是否使用安全驗證
        /// </summary>
        public static bool MailEnableSSL
        {
            get
            {
                bool l_IsEnable;
                string l_Value = null;
                try
                {
                    l_Value = ConfigurationManager.AppSettings["MailEnableSSL"].ToString();
                }
                catch (NullReferenceException p_EX)
                {
                    NLog.LogManager.GetCurrentClassLogger().Error(p_EX);
                    throw new NullReferenceException("執行Excel匯出並讀取 WebConfig 的 MailEnableSSL 參數時發生沒有參考的錯誤、請確認 WebConfig 是否有加入");
                }
                bool.TryParse(l_Value, out l_IsEnable);
                return l_IsEnable;
            }
        }
        /// <summary>
        /// SMTP 伺服器 PORT
        /// </summary>
        public static int MailPort
        {
            get
            {
                int l_Port;
                string l_Value = null;
                try
                {
                    l_Value = ConfigurationManager.AppSettings["MailPort"].ToString();
                }
                catch (NullReferenceException p_EX)
                {
                    NLog.LogManager.GetCurrentClassLogger().Error(p_EX);
                    throw new NullReferenceException("執行Excel匯出並讀取 WebConfig 的 MailPort 參數時發生沒有參考的錯誤、請確認 WebConfig 是否有加入");
                }
                int.TryParse(l_Value, out l_Port);
                return l_Port;
            }
        }
        /// <summary>
        /// 寄件人位址
        /// </summary>
        public static string MailFrom
        {
            get
            {
                try
                {
                    return ConfigurationManager.AppSettings["MailFrom"].ToString() ?? string.Empty;
                }
                catch (NullReferenceException p_EX)
                {
                    NLog.LogManager.GetCurrentClassLogger().Error(p_EX);
                    throw new NullReferenceException("執行Excel匯出並讀取 WebConfig 的 MailFrom 參數時發生沒有參考的錯誤、請確認 WebConfig 是否有加入");
                }
            }
        }
        /// <summary>
        /// 寄件人名稱
        /// </summary>
        public static string MailSender
        {
            get
            {
                try
                {
                    return ConfigurationManager.AppSettings["MailSender"].ToString() ?? string.Empty;
                }
                catch (NullReferenceException p_EX)
                {
                    NLog.LogManager.GetCurrentClassLogger().Error(p_EX);
                    throw new NullReferenceException("執行Excel匯出並讀取 WebConfig 的 MailSender 參數時發生沒有參考的錯誤、請確認 WebConfig 是否有加入");
                }
            }
        }
        /// <summary>
        /// 帳號
        /// </summary>
        public static string MailAccount
        {
            get
            {
                try
                {
                    return ConfigurationManager.AppSettings["MailAccount"].ToString() ?? string.Empty;
                }
                catch (NullReferenceException p_EX)
                {
                    NLog.LogManager.GetCurrentClassLogger().Error(p_EX);
                    throw new NullReferenceException("執行Excel匯出並讀取 WebConfig 的 MailAccount 參數時發生沒有參考的錯誤、請確認 WebConfig 是否有加入");
                }
            }
        }
        /// <summary>
        /// 密碼
        /// </summary>
        public static string MailPassword
        {
            get
            {
                try
                {
                    return ConfigurationManager.AppSettings["MailPassword"].ToString() ?? string.Empty;
                }
                catch (NullReferenceException p_EX)
                {
                    NLog.LogManager.GetCurrentClassLogger().Error(p_EX);
                    throw new NullReferenceException("執行Excel匯出並讀取 WebConfig 的 MailPassword 參數時發生沒有參考的錯誤、請確認 WebConfig 是否有加入");
                }
            }
        }
    }
}
