﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TVMLib.common;

namespace TVMLib.common
{
    public class SealedGlobalPage
    {

        //如果有session key 則 照此方式加入
        public static readonly string TYPE_NAME = "TYPE_NAME"; //PageContext 註冊的 KEY    
        public static readonly string RESULT_MESSAGE = "RESULT_MESSAGE";//產生系統訊息的 KEY

        #region Context相關

        /// <summary>
        /// getContext
        /// </summary>
        /// <param name="p_page"> Web Page</param>
        /// <returns>Context</returns>
        public static PageContext getContext(ViewPage p_page)
        {
            PageContext l_context = (PageContext)p_page.Session[SealedGlobalPage.TYPE_NAME];

            if (l_context == null && p_page.Parent != null)
            {
                l_context = (PageContext)((ViewPage)p_page.Parent).Session[SealedGlobalPage.TYPE_NAME];
            }
            if (l_context == null)
            {
                l_context = new PageContext();
                setContext(p_page, l_context);
            }
            return l_context;
        }

        /// <summary>
        /// setContext 把Context放置Session & Application
        /// </summary>
        /// <param name="p_page">Web Page</param>
        /// <param name="p_context">Context</param>
        public static void setContext(ViewPage p_page, PageContext p_context)
        {
            p_page.Session.Add(SealedGlobalPage.TYPE_NAME, p_context);
        }


        #endregion


    }
}
