﻿using System;
using System.Data;
using System.Collections;
using System.IO;
using System.Text;
using NLog;
using System.Configuration;
using BaseLib;

namespace TVMLib.common
{
    /// <summary>
    /// PageContext 的摘要描述
    /// </summary>
    public class PageContext : BaseContext
    {
        #region 資料庫連線區段 (本區塊如非必要，請勿修正)
        public PageContext()
        {

            string l_Conn = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

            try
            {
                this._Dbm = new DatabaseManager(l_Conn);
            }
            catch
            {
                throw new Exception("資料庫連接錯誤，請確認路徑：App_Data\\Data 內是否有OrderSystemDB.mdb");
            }
        }

        public PageContext(string p_connectstring)
        {
            try
            {
                this._Dbm = new DatabaseManager(p_connectstring);

            }
            catch
            {
                throw new Exception("資料庫連接錯誤，請確認連線字串：" + p_connectstring);
            }
        }

        #endregion

        #region 服務模式管理簿 註冊區段 ServiceManager (本區塊如非必要，請勿修正)

        private ServiceManager _ServiceManager = null;
        public ServiceManager ServiceManager
        {
            get
            {
                if (_ServiceManager == null)
                {
                    _ServiceManager = new ServiceManager(this);
                }

                return _ServiceManager;
            }
        }

        #endregion

        #region Log機制 註冊區段  Logger(NLog)

        private static Logger _Logger;
        public Logger Logger
        {
            get
            {
                if (_Logger == null)
                {
                    _Logger = NLog.LogManager.GetCurrentClassLogger();
                }
                return _Logger;
            }
        }

        #endregion

    }
}