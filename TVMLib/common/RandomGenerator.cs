﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace TVMLib.common
{
    /// <summary>
    /// 使用 RNGCryptoServiceProvider 產生由密碼編譯服務供應者 (CSP) 提供的亂數產生器。
    /// Reference : https://blog.miniasp.com/post/2008/05/13/Random-vs-RNGCryptoServiceProvider.aspx
    /// </summary>
    public class RandomGenerator
    {
        private static RNGCryptoServiceProvider _Rngp = new RNGCryptoServiceProvider();
        private static byte[] _RB = new byte[4];

        /// <summary>
        /// 產生一個非負數的亂數
        /// </summary>
        public static int Next()
        {
            _Rngp.GetBytes(_RB);
            int l_Value = BitConverter.ToInt32(_RB, 0);
            if (l_Value < 0) l_Value = -l_Value;
            return l_Value;
        }
        /// <summary>
        /// 產生一個非負數且最大值 max 以下的亂數
        /// </summary>
        /// <param name="p_Max">最大值</param>
        public static int Next(int p_Max)
        {
            _Rngp.GetBytes(_RB);
            int l_Value = BitConverter.ToInt32(_RB, 0);
            l_Value = l_Value % (p_Max + 1);
            if (l_Value < 0) l_Value = -l_Value;
            return l_Value;
        }
        /// <summary>
        /// 產生一個非負數且最小值在 min 以上最大值在 max 以下的亂數
        /// </summary>
        /// <param name="p_Min">最小值</param>
        /// <param name="p_Max">最大值</param>
        public static int Next(int p_Min, int p_Max)
        {
            int l_Value = Next(p_Max - p_Min) + p_Min;
            return l_Value;
        }
    }
}
