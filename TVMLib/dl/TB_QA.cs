﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVMLib.dl
{
    public class TB_QA
    {
        ///<summary>
        ///GUID
        ///</summary>
        [Display(Name = "GUID")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string QA_GUID { get; set; }

        ///<summary>
        ///類別
        ///</summary>
        [Display(Name = "類別")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string CATEGORY { get; set; }

        ///<summary>
        ///標題
        ///</summary>
        [Display(Name = "標題")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string TITLE { get; set; }

        ///<summary>
        ///內容
        ///</summary>
        [Display(Name = "內容")]
        [StringLength(500, ErrorMessage = "{0}長度不可超過16。")]
        public string MAIN { get; set; }

        ///<summary>
        ///建立時間
        ///</summary>
        [Display(Name = "建立時間")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? CREATE_TIME { get; set; }

        ///<summary>
        ///建立者
        ///</summary>
        [Display(Name = "建立者")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string CREATE_USER { get; set; }

        ///<summary>
        ///編輯時間
        ///</summary>
        [Display(Name = "編輯時間")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? EDIT_TIME { get; set; }

        ///<summary>
        ///編輯者
        ///</summary>
        [Display(Name = "編輯者")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string EDIT_USER { get; set; }

    }
}
