﻿using BaseLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TVMLib.dl
{
    public class TB_SLIDERFactory : BaseFactory
    {
        public TB_SLIDERFactory(BaseContext p_context)
        {
            SetContext(p_context);
        }

        public List<TB_SLIDER> Search()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_SLIDER Order by CREATE_TIME");
            return _Context._Dbm.ExecuteReader<TB_SLIDER>(sb.ToString());
        }

        public void Insert(TB_SLIDER p_data)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Insert Into [TB_SLIDER] ([SLIDER_GUID],[TITLE_SUB],[TITLE_MAIN]," +
                "[SLIDER_PATH]," +
                "[CREATE_TIME],[CREATE_USER]) Values ");
            sb.Append(" ( ");
            sb.Append("  @SLIDER_GUID");
            sb.Append(" ,@TITLE_SUB");
            sb.Append(" ,@TITLE_MAIN");
            sb.Append(" ,@SLIDER_PATH");
            sb.Append(" ,@DESCRIPTION");
            sb.Append(" ,@CREATE_TIME");
            sb.Append(" ,@CREATE_USER");
       
            sb.Append(" ) ");
            _Context._Dbm.AddParameter<TB_SLIDER>(p_data);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());

        }


        public void Update(TB_SLIDER p_data)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Update TB_SLIDER Set ");
            sb.Append(" TITLE_SUB = @TITLE_SUB");
            sb.Append(" ,TITLE_MAIN = @TITLE_MAIN");
            sb.Append(" ,SLIDER_PATH = @SLIDER_PATH");
            sb.Append(" Where SLIDER_GUID= @SLIDER_GUID");
            _Context._Dbm.AddParameter("@TITLE_SUB", p_data.TITLE_SUB);
            _Context._Dbm.AddParameter("@TITLE_MAIN", p_data.TITLE_MAIN);
            _Context._Dbm.AddParameter("@SLIDER_PATH", p_data.SLIDER_PATH);         
            _Context._Dbm.AddParameter("@SLIDER_GUID", p_data.SLIDER_GUID);          
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());
        }

        

        public void DeleteByGuid(string p_guid)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Delete From TB_SLIDER Where SLIDER_GUID = @SLIDER_GUID");
            _Context._Dbm.AddParameter("@SLIDER_GUID", p_guid);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());
        }
        
    }
}
