﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVMLib.dl
{
    public class TB_CONTACT
    {
        ///<summary>
		///CONTACT_GUID
		///</summary>
		[Display(Name = "PK")]     
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string CONTACT_GUID { get; set; }

        ///<summary>
        ///NAME
        ///</summary>
        [Display(Name = "姓名")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string NAME { get; set; }

        ///<summary>
        ///電子信箱
        ///</summary>
        [DisplayName("電子信箱")]
        [DataType(DataType.EmailAddress, ErrorMessage = "請輸入正確的電子信箱")]  //要求欄位是Email格式，與[EmailAddress]相同
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(100, ErrorMessage = "{0}長度不可超過100。")]
        [RegularExpression(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "EMAIL格式錯誤")]
        public string EMAIL { get; set; }

        ///<summary>
        ///留言內容
        ///</summary>
        [Display(Name = "留言內容")]
        [Required(ErrorMessage = "{0}欄位必填")]     
        public string MAIN { get; set; }


        ///<summary>
        ///CREATE_TIME
        ///</summary>
        [Display(Name = "建立時間")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? CREATE_TIME { get; set; }
    }
}
