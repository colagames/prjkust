﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVMLib.dl
{
    public class TB_TEMP
    {
        public int fid { get; set; }
        public string fType { get; set; }
        public string fName { get; set; }
        public string fKey { get; set; }
        public string fCode { get; set; }
    }
}
