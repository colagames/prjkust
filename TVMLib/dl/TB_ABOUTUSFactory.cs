﻿using BaseLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TVMLib.dl
{
    public class TB_ABOUTUSFactory : BaseFactory
    {
        public TB_ABOUTUSFactory(BaseContext p_context)
        {
            SetContext(p_context);
        }

        public List<TB_ABOUTUS> Search()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_ABOUTUS where IS_ACTIVE = 1 Order by CREATE_TIME");
            return _Context._Dbm.ExecuteReader<TB_ABOUTUS>(sb.ToString());
        }

        public void Insert(TB_ABOUTUS p_data)
        {
            //StringBuilder sb = new StringBuilder();
            //sb.Append("Insert Into [TB_ABOUTUS] ([ABOUTUS_GUID],[TITLE],[MAIN]," +
            //    "[IS_ACTIVE],[CREATE_TIME],[CREATE_USER],[EDIT_TIME],[EDIT_USER]) Values ");
            //sb.Append(" ( ");
            //sb.Append("  @ABOUTUS_GUID");
            //sb.Append(" ,@TITLE");
            //sb.Append(" ,@MAIN");
            //sb.Append(" ,@IS_ACTIVE");           
            //sb.Append(" ,@CREATE_TIME");
            //sb.Append(" ,@CREATE_USER");
            //sb.Append(" ,@EDIT_TIME");
            //sb.Append(" ,@EDIT_USER");
            //sb.Append(" ) ");
            //_Context._Dbm.AddParameter<TB_ABOUTUS>(p_data);
            //_Context._Dbm.EexcuteSqlNonquery(sb.ToString());

           
        }

       

        internal TB_ABOUTUS SearchByGUID(string p_ABOUTUS_GUID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_ABOUTUS where ABOUTUS_GUID=@ABOUTUS_GUID");
            _Context._Dbm.AddParameter("@ABOUTUS_GUID", p_ABOUTUS_GUID);
            List<TB_ABOUTUS> l_TB_ABOUTUS = _Context._Dbm.ExecuteReader<TB_ABOUTUS>(sb.ToString());
            if (l_TB_ABOUTUS.Count > 0)
            {
                return l_TB_ABOUTUS[0];
            }
            return null;
        }

        public void Update(TB_ABOUTUS p_data)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Update TB_ABOUTUS Set ");
            sb.Append(" ABOUTUS_MAIN1 = @ABOUTUS_MAIN1");
            sb.Append(" ,ABOUTUS_MAIN2 = @ABOUTUS_MAIN2");
            sb.Append(" ,ABOUTUS_LINK = @ABOUTUS_LINK");
            sb.Append(" ,ABOUTUS_LINK_TEXT = @ABOUTUS_LINK_TEXT");            
            sb.Append(" Where ABOUTUS_GUID= @ABOUTUS_GUID");
            _Context._Dbm.AddParameter("@ABOUTUS_MAIN1", p_data.ABOUTUS_MAIN1);
            _Context._Dbm.AddParameter("@ABOUTUS_MAIN2", p_data.ABOUTUS_MAIN2);
            _Context._Dbm.AddParameter("@ABOUTUS_LINK", p_data.ABOUTUS_LINK);
            _Context._Dbm.AddParameter("@ABOUTUS_LINK_TEXT", p_data.ABOUTUS_LINK_TEXT);
            _Context._Dbm.AddParameter("@ABOUTUS_GUID", p_data.ABOUTUS_GUID);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());
        }

        

        public void DeleteByGuid(string p_guid)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Delete From TB_ABOUTUS Where ABOUTUS_GUID = @ABOUTUS_GUID");
            _Context._Dbm.AddParameter("@ABOUTUS_GUID", p_guid);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());
        }
    }
}
