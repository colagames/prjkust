﻿using BaseLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TVMLib.dl
{
    public class TB_ORDER_DETAILFactory : BaseFactory
    {
        public TB_ORDER_DETAILFactory(BaseContext p_context)
        {
            SetContext(p_context);
        }

        public List<TB_ORDER_DETAIL> Search()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_ORDER_DETAIL order by CREATE_TIME");
            return _Context._Dbm.ExecuteReader<TB_ORDER_DETAIL>(sb.ToString());
        }

        public void Insert(TB_ORDER_DETAIL p_data)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Insert Into [TB_ORDER_DETAIL] ([ORDER_DETAIL_GUID],[ORDER_GUID],[PRODUCT_GUID]," +
                "[PRICE],[UNIT],[SUB_TOTAL]) Values ");
            sb.Append(" ( ");
            sb.Append("  @ORDER_DETAIL_GUID");
            sb.Append(" ,@ORDER_GUID");
            sb.Append(" ,@PRODUCT_GUID");
            sb.Append(" ,@PRICE");
            sb.Append(" ,@UNIT");           
            sb.Append(" ,@SUB_TOTAL");          
          
            sb.Append(" ) ");
            _Context._Dbm.AddParameter("@ORDER_DETAIL_GUID", p_data.ORDER_DETAIL_GUID);
            _Context._Dbm.AddParameter("@ORDER_GUID", p_data.ORDER_GUID);
            _Context._Dbm.AddParameter("@PRODUCT_GUID", p_data.PRODUCT_GUID);
            _Context._Dbm.AddParameter("@PRICE", p_data.PRICE);
            _Context._Dbm.AddParameter("@UNIT", p_data.UNIT);
            _Context._Dbm.AddParameter("@SUB_TOTAL", p_data.SUB_TOTAL);
 
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());

           
        }

      
        internal TB_ORDER_DETAIL SearchByGUID(string p_ORDER_DETAIL_GUID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_ORDER_DETAIL where ORDER_DETAIL_GUID=@ORDER_DETAIL_GUID");
            _Context._Dbm.AddParameter("@ORDER_DETAIL_GUID", p_ORDER_DETAIL_GUID);
            List<TB_ORDER_DETAIL> l_TB_ORDER_DETAIL = _Context._Dbm.ExecuteReader<TB_ORDER_DETAIL>(sb.ToString());
            if (l_TB_ORDER_DETAIL.Count > 0)
            {
                return l_TB_ORDER_DETAIL[0];
            }
            return null;
        }

        public void Update(TB_ORDER_DETAIL p_data)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Update TB_ORDER_DETAIL Set ");
            sb.Append(" PRICE = @PRICE");
            sb.Append(" ,UNIT = @UNIT");
            sb.Append(" ,SUB_TOTAL = @SUB_TOTAL");          
            sb.Append(" Where ORDER_DETAIL_GUID= @ORDER_DETAIL_GUID");
            _Context._Dbm.AddParameter("@PRICE", p_data.PRICE);
            _Context._Dbm.AddParameter("@UNIT", p_data.UNIT);
            _Context._Dbm.AddParameter("@SUB_TOTAL", p_data.SUB_TOTAL);

            _Context._Dbm.AddParameter("@ORDER_DETAIL_GUID", p_data.ORDER_DETAIL_GUID);
 
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());
        }

        

        public void DeleteByGuid(string p_guid)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Delete From TB_ORDER_DETAIL Where ORDER_DETAIL_GUID = @ORDER_DETAIL_GUID");
            _Context._Dbm.AddParameter("@ORDER_DETAIL_GUID", p_guid);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());
        }

        internal List<TB_ORDER_DETAIL> SearchByORDER_GUID(string p_ORDER_GUID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_ORDER_DETAIL where ORDER_GUID=@ORDER_GUID");
            _Context._Dbm.AddParameter("@ORDER_GUID", p_ORDER_GUID);
            return _Context._Dbm.ExecuteReader<TB_ORDER_DETAIL>(sb.ToString());
            
           
        }
    }
}
