﻿using BaseLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TVMLib.dl
{
    public class TB_QAFactory : BaseFactory
    {
        public TB_QAFactory(BaseContext p_context)
        {
            SetContext(p_context);
        }

        public List<TB_QA> Search()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_QA Order by CREATE_TIME");
            return _Context._Dbm.ExecuteReader<TB_QA>(sb.ToString());
        }

        public void Insert(TB_QA p_data)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Insert Into [TB_QA] ([QA_GUID],[CATEGORY],[TITLE],[MAIN]," +
                "[CREATE_TIME],[CREATE_USER],[EDIT_TIME],[EDIT_USER]) Values ");
            sb.Append(" ( ");
            sb.Append("  @QA_GUID");
            sb.Append(" ,@CATEGORY");
            sb.Append(" ,@TITLE");
            sb.Append(" ,@MAIN");
            sb.Append(" ,@CREATE_TIME");
            sb.Append(" ,@CREATE_USER");
            sb.Append(" ,@EDIT_TIME");
            sb.Append(" ,@EDIT_USER");
            sb.Append(" ) ");
            _Context._Dbm.AddParameter<TB_QA>(p_data);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());


        }

        internal List<TB_QA> SearchCategoryGroupByKeyWord(string p_KeyWord)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select CATEGORY from TB_QA where TITLE like '%'+@p_Keyword1+'%'   group by CATEGORY");
            _Context._Dbm.AddParameter("@p_Keyword1", p_KeyWord);
            _Context._Dbm.AddParameter("@p_Keyword2", p_KeyWord);
            return _Context._Dbm.ExecuteReader<TB_QA>(sb.ToString());
        }

        internal List<TB_QA> SearchByCatehoryAndKeyWord(string p_CATEGORY, string p_KeyWord)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_QA where CATEGORY=@p_CATEGORY and  MAIN  like '%'+@p_KeyWord2+'%'  ");
            _Context._Dbm.AddParameter("@p_CATEGORY", p_CATEGORY);
            _Context._Dbm.AddParameter("@p_KeyWord2", p_KeyWord);

            return _Context._Dbm.ExecuteReader<TB_QA>(sb.ToString());
        }

        internal List<TB_QA> SearchByCATEGORY(string p_CATEGORY)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_QA where CATEGORY=@CATEGORY");
            _Context._Dbm.AddParameter("@CATEGORY", p_CATEGORY);
            return _Context._Dbm.ExecuteReader<TB_QA>(sb.ToString());
        
        }

        internal List<TB_QA> SearchByKeyWord(string p_KeyWord)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_QA where TITLE like '%'+@p_KeyWord1+'%' or MAIN  like '%'+@p_KeyWord2+'%' or CATEGORY like '%'+@p_KeyWord3+'%' ");
            _Context._Dbm.AddParameter("@p_KeyWord1", p_KeyWord);
            _Context._Dbm.AddParameter("@p_KeyWord2", p_KeyWord);
            _Context._Dbm.AddParameter("@p_KeyWord3", p_KeyWord);
            return _Context._Dbm.ExecuteReader<TB_QA>(sb.ToString());


        }

        internal TB_QA SearchByGUID(string p_QA_GUID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_QA where QA_GUID=@QA_GUID");
            _Context._Dbm.AddParameter("@QA_GUID", p_QA_GUID);
            List<TB_QA> l_TB_QA = _Context._Dbm.ExecuteReader<TB_QA>(sb.ToString());
            if (l_TB_QA.Count > 0)
            {
                return l_TB_QA[0];
            }
            return null;
        }

        internal List<TB_QA> SearchCategoryGroup()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select CATEGORY from TB_QA group by CATEGORY");
            return _Context._Dbm.ExecuteReader<TB_QA>(sb.ToString());
        }

        public void Update(TB_QA p_data)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Update TB_QA Set ");
            sb.Append(" CATEGORY = @CATEGORY");
            sb.Append(" ,TITLE = @TITLE");
            sb.Append(" ,MAIN = @MAIN");
            sb.Append(" ,EDIT_TIME = @EDIT_TIME");
            sb.Append(" ,EDIT_USER = @EDIT_USER");
            sb.Append(" Where QA_GUID= @QA_GUID");
            _Context._Dbm.AddParameter("@CATEGORY", p_data.CATEGORY);
            _Context._Dbm.AddParameter("@TITLE", p_data.TITLE);
            _Context._Dbm.AddParameter("@MAIN", p_data.MAIN);
            _Context._Dbm.AddParameter("@QA_GUID", p_data.QA_GUID);
            _Context._Dbm.AddParameter("@EDIT_TIME", p_data.EDIT_TIME);
            _Context._Dbm.AddParameter("@EDIT_USER", p_data.EDIT_USER);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());
        }



        public void DeleteByGuid(string p_guid)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Delete From TB_QA Where QA_GUID = @QA_GUID");
            _Context._Dbm.AddParameter("@QA_GUID", p_guid);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());
        }

    }
}
