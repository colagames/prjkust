﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVMLib.dl
{
    public class TB_ORDER_DETAIL
    {
        ///<summary>
		///ORDER_DETAIL_GUID
		///</summary>
		[Display(Name = "ORDER_DETAIL_GUID")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string ORDER_DETAIL_GUID { get; set; }

        ///<summary>
        ///ORDER_GUID
        ///</summary>
        [Display(Name = "ORDER_GUID")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string ORDER_GUID { get; set; }

        ///<summary>
        ///PRODUCT_GUID
        ///</summary>
        [Display(Name = "PRODUCT_GUID")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string PRODUCT_GUID { get; set; }

        ///<summary>
        ///單價
        ///</summary>
        [Display(Name = "單價")]
        [Required(ErrorMessage = "{0}欄位必填")]
        public Double PRICE { get; set; }

        ///<summary>
        ///數量
        ///</summary>
        [Display(Name = "數量")]
        [Required(ErrorMessage = "{0}欄位必填")]
        public int UNIT { get; set; }

        ///<summary>
        ///小計
        ///</summary>
        [Display(Name = "小計")]
        [Required(ErrorMessage = "{0}欄位必填")]
        public double SUB_TOTAL { get; set; }


        public TB_PRODUCT l_TB_PRODUCT { get; set; }
    }
}
