﻿using BaseLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVMLib.dl.AdminInquiry;
using TVMLib.dl.AdminProduct;
using TVMLib.dl.Product;

namespace TVMLib.dl
{
    public class TB_INQUIRYFactory : BaseFactory
    {
        public TB_INQUIRYFactory(BaseContext p_context)
        {
            SetContext(p_context);
        }

        public List<TB_INQUIRY> Search()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_INQUIRY Order by CREATE_TIME");
            return _Context._Dbm.ExecuteReader<TB_INQUIRY>(sb.ToString());
        }

        public void Insert(TB_INQUIRY p_data)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Insert Into [TB_INQUIRY] ([INQUIRY_GUID],[PRODUCT_GUID],[USER_GUID]," +
                "[NAME],[TEL],[EMAIL],[MAIN]," +
                "[CREATE_TIME],[CREATE_USER],[EDIT_TIME],[EDIT_USER]) Values ");
            sb.Append(" ( ");
            sb.Append("  @INQUIRY_GUID");
            sb.Append(" ,@PRODUCT_GUID");
            sb.Append(" ,@USER_GUID");
            sb.Append(" ,@NAME");
            sb.Append(" ,@TEL");
            sb.Append(" ,@EMAIL");
            sb.Append(" ,@MAIN");
            sb.Append(" ,@CREATE_TIME");
            sb.Append(" ,@CREATE_USER");
            sb.Append(" ,@EDIT_TIME");
            sb.Append(" ,@EDIT_USER");
            sb.Append(" ) ");
            _Context._Dbm.AddParameter("@INQUIRY_GUID", p_data.INQUIRY_GUID);
            _Context._Dbm.AddParameter("@PRODUCT_GUID", p_data.PRODUCT_GUID);
            _Context._Dbm.AddParameter("@USER_GUID", p_data.USER_GUID == null ? "" : p_data.USER_GUID);
            _Context._Dbm.AddParameter("@NAME", p_data.NAME);
            _Context._Dbm.AddParameter("@TEL", p_data.TEL);
            _Context._Dbm.AddParameter("@EMAIL", p_data.EMAIL);
            _Context._Dbm.AddParameter("@MAIN", p_data.MAIN);
            _Context._Dbm.AddParameter("@CREATE_USER", p_data.CREATE_USER);
            _Context._Dbm.AddParameter("@CREATE_TIME", p_data.CREATE_TIME);
            _Context._Dbm.AddParameter("@EDIT_TIME", p_data.EDIT_TIME);
            _Context._Dbm.AddParameter("@EDIT_USER", p_data.EDIT_USER);

            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());


        }

        internal List<InquiryVM> SearchInquiryVM()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_INQUIRY Order by CREATE_TIME");
            return _Context._Dbm.ExecuteReader<InquiryVM>(sb.ToString());
        }

        internal List<TB_INQUIRY> SearchByKeyWord(string p_KeyWord)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_INQUIRY where NAME like '%'+@p_KeyWord+'%' ");
            _Context._Dbm.AddParameter("@p_KeyWord", p_KeyWord);
            return _Context._Dbm.ExecuteReader<TB_INQUIRY>(sb.ToString());

        }

        internal TB_INQUIRY SearchByGUID(string p_INQUIRY_GUID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_INQUIRY where INQUIRY_GUID=@INQUIRY_GUID");
            _Context._Dbm.AddParameter("@INQUIRY_GUID", p_INQUIRY_GUID);
            List<TB_INQUIRY> l_TB_INQUIRY = _Context._Dbm.ExecuteReader<TB_INQUIRY>(sb.ToString());
            if (l_TB_INQUIRY.Count > 0)
            {
                return l_TB_INQUIRY[0];
            }
            return null;
        }

        public void Update(TB_INQUIRY p_data)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Update TB_INQUIRY Set ");
            sb.Append(" RE_PRICE = @RE_PRICE");
            sb.Append(" ,RE_COUNT = @RE_COUNT");
            sb.Append(" ,RE_TOTAL = @RE_TOTAL");
            sb.Append(" ,RE_PROCESS = @RE_PROCESS");
            sb.Append(" ,EDIT_TIME = @EDIT_TIME");
            sb.Append(" ,EDIT_USER = @EDIT_USER");
            sb.Append(" Where INQUIRY_GUID= @INQUIRY_GUID");
            _Context._Dbm.AddParameter("@RE_PRICE", p_data.RE_PRICE);
            _Context._Dbm.AddParameter("@RE_COUNT", p_data.RE_COUNT);
            _Context._Dbm.AddParameter("@RE_TOTAL", p_data.RE_TOTAL);
            _Context._Dbm.AddParameter("@RE_PROCESS", p_data.RE_PROCESS);
            _Context._Dbm.AddParameter("@EDIT_TIME", p_data.EDIT_TIME);
            _Context._Dbm.AddParameter("@EDIT_USER", p_data.EDIT_USER);
            _Context._Dbm.AddParameter("@INQUIRY_GUID", p_data.INQUIRY_GUID);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());
        }



        public void DeleteByGuid(string p_guid)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Delete From TB_INQUIRY Where INQUIRY_GUID = @INQUIRY_GUID");
            _Context._Dbm.AddParameter("@INQUIRY_GUID", p_guid);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());
        }

        public AdminInquiryVM SearchAdminInquiryVMByGUID(string p_INQUIRY_GUID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_INQUIRY where INQUIRY_GUID=@INQUIRY_GUID");
            _Context._Dbm.AddParameter("@INQUIRY_GUID", p_INQUIRY_GUID);
            List<AdminInquiryVM> l_TB_INQUIRY = _Context._Dbm.ExecuteReader<AdminInquiryVM>(sb.ToString());
            if (l_TB_INQUIRY.Count > 0)
            {
                return l_TB_INQUIRY[0];
            }
            return null;
        }

    }
}
