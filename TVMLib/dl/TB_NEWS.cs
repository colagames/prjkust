﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVMLib.dl
{
    public class TB_NEWS
    {
        ///<summary>
		///NEWS_GUID
		///</summary>
		[Display(Name = "PK")]     
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string NEWS_GUID { get; set; }

        ///<summary>
        ///TITLE
        ///</summary>
        [Display(Name = "消息標題")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string TITLE { get; set; }

        ///<summary>
        ///MAIN
        ///</summary>
        [Display(Name = "消息內容")]
        [Required(ErrorMessage = "{0}欄位必填")]     
        public string MAIN { get; set; }

        ///<summary>
        ///IS_ACTIVE
        ///</summary>
        [Display(Name = "是否上架")]
        [Required(ErrorMessage = "{0}欄位必填")]
        public bool IS_ACTIVE { get; set; }

        ///<summary>
        ///CREATE_TIME
        ///</summary>
        [Display(Name = "建立時間")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? CREATE_TIME { get; set; }

        ///<summary>
        ///CREATE_USER
        ///</summary>
        [Display(Name = "建立者")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string CREATE_USER { get; set; }

        ///<summary>
        ///EDIT_TIME
        ///</summary>
        [Display(Name = "修改時間")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? EDIT_TIME { get; set; }

        ///<summary>
        ///EDIT_USER
        ///</summary>
        [Display(Name = "修改者")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}", ApplyFormatInEditMode = true)]
        public string EDIT_USER { get; set; }

    }
}
