﻿using BaseLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TVMLib.dl
{
    public class TB_CONTACTFactory : BaseFactory
    {
        public TB_CONTACTFactory(BaseContext p_context)
        {
            SetContext(p_context);
        }

        public List<TB_CONTACT> Search()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_CONTACT Order by CREATE_TIME");
            return _Context._Dbm.ExecuteReader<TB_CONTACT>(sb.ToString());
        }

        public void Insert(TB_CONTACT p_data)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Insert Into [TB_CONTACT] ([CONTACT_GUID],[NAME],[EMAIL],[MAIN]," +
                "[CREATE_TIME]) Values ");
            sb.Append(" ( ");
            sb.Append("  @CONTACT_GUID");
            sb.Append(" ,@NAME");
            sb.Append(" ,@EMAIL");
            sb.Append(" ,@MAIN");                    
            sb.Append(" ,@CREATE_TIME");
           
            sb.Append(" ) ");
            _Context._Dbm.AddParameter<TB_CONTACT>(p_data);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());

           
        }

        internal List<TB_CONTACT> SearchByKeyWord(string p_KeyWord)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_CONTACT where NAME like '%'+@p_KeyWord+'%' ");
            _Context._Dbm.AddParameter("@p_KeyWord", p_KeyWord);
            return _Context._Dbm.ExecuteReader<TB_CONTACT>(sb.ToString());
          
            
        }

        internal TB_CONTACT SearchByGUID(string p_CONTACT_GUID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_CONTACT where CONTACT_GUID=@CONTACT_GUID");
            _Context._Dbm.AddParameter("@CONTACT_GUID", p_CONTACT_GUID);
            List<TB_CONTACT> l_TB_CONTACT = _Context._Dbm.ExecuteReader<TB_CONTACT>(sb.ToString());
            if (l_TB_CONTACT.Count > 0)
            {
                return l_TB_CONTACT[0];
            }
            return null;
        }

        public void Update(TB_CONTACT p_data)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Update TB_CONTACT Set ");
            sb.Append(" NAME = @NAME");
            sb.Append(" ,EMAIL = @EMAIL");
            sb.Append(" ,MAIN = @MAIN");      
            sb.Append(" Where CONTACT_GUID= @CONTACT_GUID");    
            _Context._Dbm.AddParameter("@MAIN", p_data.MAIN);
            _Context._Dbm.AddParameter("@CONTACT_GUID", p_data.CONTACT_GUID);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());
        }

        

        public void DeleteByGuid(string p_guid)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Delete From TB_CONTACT Where CONTACT_GUID = @CONTACT_GUID");
            _Context._Dbm.AddParameter("@CONTACT_GUID", p_guid);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());
        }
    }
}
