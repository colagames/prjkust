﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVMLib.dl.Login
{
    public class ForgetVM
    {
        ///<summary>
        ///電子信箱
        ///</summary>
        [DisplayName("電子信箱")]
        [DataType(DataType.EmailAddress, ErrorMessage = "請輸入正確的電子信箱")]  //要求欄位是Email格式，與[EmailAddress]相同
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(100, ErrorMessage = "{0}長度不可超過100。")]
        [RegularExpression(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "EMAIL格式錯誤")]
        public string EMAIL { get; set; }
    }
}
