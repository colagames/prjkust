﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TVMLib.dl.Login
{
    public class RegisteredVM
    {

   


        ///<summary>
        ///使用者姓名
        ///</summary>
        [DisplayName("使用者姓名")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(100, ErrorMessage = "{0}長度不可超過100。")]
        public string NAME { get; set; }


        ///<summary>
        ///帳號
        ///</summary>
        [DisplayName("帳號")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string ACCOUNT { get; set; }

        ///<summary>
        ///密碼
        ///</summary>
        [DisplayName("密碼")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        [DataType(DataType.Password)]   //表示此欄位為密碼欄位，所以輸入時會產生隱碼
        [RegularExpression(@"[a-zA-Z]+[a-zA-Z0-9]*$", ErrorMessage = "密碼僅能有英文或數字，且開頭需為英文字母！")]
        public string PASSWORD { get; set; }

        ///<summary>
        ///電話
        ///</summary>
        [DisplayName("電話")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string TEL { get; set; }

        ///<summary>
        ///電子信箱
        ///</summary>
        [DisplayName("電子信箱")]
        [DataType(DataType.EmailAddress, ErrorMessage = "請輸入正確的電子信箱")]  //要求欄位是Email格式，與[EmailAddress]相同
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(100, ErrorMessage = "{0}長度不可超過100。")]
        [RegularExpression(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "EMAIL格式錯誤")]
        public string EMAIL { get; set; }

        ///<summary>
        ///是否有效
        ///</summary>
        [DisplayName("是否有效")]
        [Required(ErrorMessage = "{0}欄位必填")]
        public bool IS_ACTIVE { get; set; }

        ///<summary>
        ///是否為管理者
        ///</summary>
        [DisplayName("是否為管理者")]
        [Required(ErrorMessage = "{0}欄位必填")]
        public bool IS_ADMIN { get; set; }


        ///<summary>
        ///城市
        ///</summary>
        [DisplayName("城市")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(10, ErrorMessage = "{0}長度不可超過10。")]
        public string CITY { get; set; }

        ///<summary>
        ///區域
        ///</summary>
        [DisplayName("區域")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(10, ErrorMessage = "{0}長度不可超過10。")]
        public string AREA { get; set; }

        ///<summary>
        ///郵遞區號
        ///</summary>
        [DisplayName("郵遞區號")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(10, ErrorMessage = "{0}長度不可超過10。")]
        public string ZIP { get; set; }

        ///<summary>
        ///地址
        ///</summary>
        [DisplayName("地址")]     
        [StringLength(500, ErrorMessage = "{0}長度不可超過500。")]
        public string ADDRESS { get; set; }
        

        [Required(ErrorMessage = "請再輸入密碼")]
        [DisplayName("密碼確認")]
        [StringLength(30, ErrorMessage = "長度請勿超過20字")]
        [MinLength(6, ErrorMessage = "請輸入6個字以上")]
        [RegularExpression(@"^[a-zA-Z0-9]*$", ErrorMessage = "密碼僅能有英文或數字")]
        [Compare(nameof(PASSWORD), ErrorMessage = "密碼不符合")]
        public string PASSWORDCHECK { get; set; }

        
    }
}