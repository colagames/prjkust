﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVMLib.dl.Login
{
    public class LoginVM
    {
        ///<summary>
        ///帳號
        ///</summary>
        [DisplayName("帳號")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string ACCOUNT { get; set; }

        ///<summary>
        ///密碼
        ///</summary>
        [DisplayName("密碼")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [PasswordPropertyText]   //表示此欄位為密碼欄位，所以輸入時會產生隱碼
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        //[RegularExpression(@"[a-zA-Z]+[a-zA-Z0-9]*$", ErrorMessage = "密碼僅能有英文或數字，且開頭需為英文字母！")]
        public string PASSWORD { get; set; }
    }
}
