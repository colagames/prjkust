﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TVMLib.Validate;

namespace TVMLib.dl.AdminProfile
{
    public class AdminProfileVM
    {
        ///<summary>
		///PROFILE_GUID
		///</summary>
		[Display(Name = "PK")]     
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string PROFILE_GUID { get; set; }

        ///<summary>
        ///TITLE
        ///</summary>
        [Display(Name = "檔案名稱")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string TITLE { get; set; }



        [Display(Name = "檔案位置")]
        [ValidateFileAttribute]
        public HttpPostedFileBase PATH_UPLOAD { get; set; }

        ///<summary>
        ///檔案位置
        ///</summary>
        [Display(Name = "檔案位置")]
        public String PATH { get; set; }

        ///<summary>
        ///IS_ACTIVE
        ///</summary>
        [Display(Name = "是否上架")]
        [Required(ErrorMessage = "{0}欄位必填")]
        public bool IS_ACTIVE { get; set; }

    }
}
