﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVMLib.dl
{
    public class TB_PROFILE
    {
        ///<summary>
		///PROFILE_GUID
		///</summary>
		[Display(Name = "PK")]     
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string PROFILE_GUID { get; set; }

        ///<summary>
        ///TITLE
        ///</summary>
        [Display(Name = "檔案名稱")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string TITLE { get; set; }

        ///<summary>
        ///檔案位置
        ///</summary>
        [Display(Name = "檔案位置")]
        public String PATH { get; set; }

        ///<summary>
        ///IS_ACTIVE
        ///</summary>
        [Display(Name = "是否上架")]
        [Required(ErrorMessage = "{0}欄位必填")]
        public bool IS_ACTIVE { get; set; }

        ///<summary>
        ///CREATE_TIME
        ///</summary>
        [Display(Name = "建立時間")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? CREATE_TIME { get; set; }

        ///<summary>
        ///CREATE_USER
        ///</summary>
        [Display(Name = "建立者")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string CREATE_USER { get; set; }

    }
}
