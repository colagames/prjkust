﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TVMLib.Validate;

namespace TVMLib.dl.AdminSLIDER
{
    public class AdminSliderVM
    {
        ///<summary>
		///SLIDER_GUID
		///</summary>
		[Display(Name = "PK")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string SLIDER_GUID { get; set; }

        ///<summary>
        ///小標題
        ///</summary>
        [Display(Name = "小標題")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string TITLE_SUB { get; set; }

        ///<summary>
        ///大標題
        ///</summary>
        [Display(Name = "大標題")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string TITLE_MAIN { get; set; }

        ///<summary>
        ///輪播圖
        ///</summary>
        [Display(Name = "輪播圖")]
        public String SLIDER_PATH { get; set; }

        [Display(Name = "輪播圖")]
        [ValidateFilePhoto]
        public HttpPostedFileBase SLIDER_PATH_UPLOAD { get; set; }
    }
}
