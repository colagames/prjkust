﻿using BaseLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TVMLib.dl
{
    public class TB_USERFactory : BaseFactory
    {
        public TB_USERFactory(BaseContext p_context)
        {
            SetContext(p_context);
        }

        public List<TB_USER> Search()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_USER");
            return _Context._Dbm.ExecuteReader<TB_USER>(sb.ToString());
        }

        public void Insert(TB_USER p_data)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Insert Into [TB_USER] ([USER_GUID],[NAME]," +
                "[ACCOUNT],[PASSWORD],[TEL],[EMAIL],[CITY],[AREA],[ZIP],[ADDRESS],[IS_ACTIVE],[IS_ADMIN]," +
                "[CREATE_TIME],[CREATE_USER],[EDIT_TIME],[EDIT_USER]) Values ");
            sb.Append(" ( ");
            sb.Append("  @USER_GUID");
            sb.Append(" ,@NAME");
            sb.Append(" ,@ACCOUNT");
            sb.Append(" ,@PASSWORD");
            sb.Append(" ,@TEL");
            sb.Append(" ,@EMAIL");
            sb.Append(" ,@CITY");
            sb.Append(" ,@AREA");
            sb.Append(" ,@ZIP");
            sb.Append(" ,@ADDRESS");
            sb.Append(" ,@IS_ACTIVE");
            sb.Append(" ,@IS_ADMIN");
            sb.Append(" ,@CREATE_TIME");
            sb.Append(" ,@CREATE_USER");
            sb.Append(" ,@EDIT_TIME");
            sb.Append(" ,@EDIT_USER");
            sb.Append(" ) ");
            _Context._Dbm.AddParameter<TB_USER>(p_data);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());


        }

        internal TB_USER SearchByAccAndPwd(string p_ACCOUNT, string p_PASSWORD)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_USER where ACCOUNT=@p_ACCOUNT and PASSWORD=@p_PASSWORD and IS_ACTIVE=1");
            _Context._Dbm.AddParameter("@p_ACCOUNT", p_ACCOUNT);
            _Context._Dbm.AddParameter("@p_PASSWORD", p_PASSWORD);
            List<TB_USER> l_TB_USER = _Context._Dbm.ExecuteReader<TB_USER>(sb.ToString());
            if (l_TB_USER.Count > 0)
            {
                return l_TB_USER[0];
            }
            return null;
        }

        internal TB_USER SearchAdminByAccAndPwd(string p_ACCOUNT, string p_PASSWORD)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_USER where ACCOUNT=@p_ACCOUNT and PASSWORD=@p_PASSWORD and IS_ACTIVE=1 and IS_ADMIN = 1");
            _Context._Dbm.AddParameter("@p_ACCOUNT", p_ACCOUNT);
            _Context._Dbm.AddParameter("@p_PASSWORD", p_PASSWORD);
            List<TB_USER> l_TB_USER = _Context._Dbm.ExecuteReader<TB_USER>(sb.ToString());
            if (l_TB_USER.Count > 0)
            {
                return l_TB_USER[0];
            }
            return null;
        }

        internal TB_USER SearchByEmail(string p_Email, bool p_FilterIsDelete = true)
        {
            StringBuilder l_SB = new StringBuilder();
            l_SB.Append("select * from TB_USER where EMAIL=@EMAIL");
            if (p_FilterIsDelete)
            {
                l_SB.Append(" and TB_USER.IS_ACTIVE=1");
            }
            l_SB.Append(" and TB_USER.IS_ADMIN=0");
            _Context._Dbm.AddParameter("@EMAIL", p_Email.Replace("'", "''"));
            List<TB_USER> l_Query = _Context._Dbm.ExecuteReader<TB_USER>(l_SB.ToString());
            if (l_Query != null && l_Query.Any())
                return l_Query[0];
            else
                return null;
        }


        //使用者只有下架，沒有刪除
        //public void DeleteByGuid(string p_guid)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("Delete From TB_USER Where GUID = @GUID");
        //    _Context._Dbm.AddParameter("@GUID", p_guid);
        //    _Context._Dbm.EexcuteSqlNonquery(sb.ToString());
        //}

        internal TB_USER SearchByGUID(string p_USER_GUID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_USER where USER_GUID=@USER_GUID");
            _Context._Dbm.AddParameter("@USER_GUID", p_USER_GUID);
            List<TB_USER> l_TB_USER = _Context._Dbm.ExecuteReader<TB_USER>(sb.ToString());
            if (l_TB_USER.Count > 0)
            {
                return l_TB_USER[0];
            }
            return null;
        }

        public void Update(TB_USER p_data)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Update TB_USER Set ");
            sb.Append(" NAME=@NAME");
            sb.Append(" ,ACCOUNT=@ACCOUNT");
            sb.Append(" ,PASSWORD=@PASSWORD");
            sb.Append(" ,TEL=@TEL");
            sb.Append(" ,EMAIL=@EMAIL");
            sb.Append(" ,CITY=@CITY");
            sb.Append(" ,AREA=@AREA");
            sb.Append(" ,ZIP=@ZIP");
            sb.Append(" ,ADDRESS=@ADDRESS");
            sb.Append(" ,IS_ACTIVE=@IS_ACTIVE");
            sb.Append(" ,IS_ADMIN=@IS_ADMIN");
            sb.Append(" ,EDIT_TIME=@EDIT_TIME");
            sb.Append(" ,EDIT_USER=@EDIT_USER");
            sb.Append(" Where USER_GUID= @USER_GUID");

            _Context._Dbm.AddParameter("@NAME", p_data.NAME);
            _Context._Dbm.AddParameter("@ACCOUNT", p_data.ACCOUNT);
            _Context._Dbm.AddParameter("@PASSWORD", p_data.PASSWORD);
            _Context._Dbm.AddParameter("@TEL", p_data.TEL);
            _Context._Dbm.AddParameter("@EMAIL", p_data.EMAIL);
            _Context._Dbm.AddParameter("@CITY", p_data.CITY);
            _Context._Dbm.AddParameter("@AREA", p_data.AREA);
            _Context._Dbm.AddParameter("@ZIP", p_data.ZIP);
            _Context._Dbm.AddParameter("@ADDRESS", p_data.ADDRESS);
            _Context._Dbm.AddParameter("@IS_ACTIVE", p_data.IS_ACTIVE);
            _Context._Dbm.AddParameter("@IS_ADMIN", p_data.IS_ADMIN);
            _Context._Dbm.AddParameter("@EDIT_TIME", p_data.EDIT_TIME);
            _Context._Dbm.AddParameter("@EDIT_USER", p_data.EDIT_USER);
            _Context._Dbm.AddParameter("@USER_GUID", p_data.USER_GUID);

            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());
        }

        public void UpdateIsActiveByUSER_GUID(bool p_IS_ACTIVE, string p_USER_GUID, DateTime p_EDIT_TINE, string p_EDIT_USER)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Update TB_USER Set ");
            sb.Append(" IS_ACTIVE = @IS_ACTIVE");
            sb.Append(" ,EDIT_TIME = @EDIT_TIME");
            sb.Append(" ,EDIT_USER = @EDIT_USER");
            sb.Append(" Where USER_GUID= @USER_GUID");
            _Context._Dbm.AddParameter("@IS_ACTIVE", p_IS_ACTIVE);
            _Context._Dbm.AddParameter("@USER_GUID", p_USER_GUID);
            _Context._Dbm.AddParameter("@EDIT_TIME", p_EDIT_TINE);
            _Context._Dbm.AddParameter("@EDIT_USER", p_EDIT_USER);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());
        }

    }
}
