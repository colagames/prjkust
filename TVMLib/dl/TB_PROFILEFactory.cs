﻿using BaseLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TVMLib.dl
{
    public class TB_PROFILEFactory : BaseFactory
    {
        public TB_PROFILEFactory(BaseContext p_context)
        {
            SetContext(p_context);
        }

        public List<TB_PROFILE> Search()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_PROFILE Order by CREATE_TIME");
            return _Context._Dbm.ExecuteReader<TB_PROFILE>(sb.ToString());
        }

        public void Insert(TB_PROFILE p_data)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Insert Into [TB_PROFILE] ([PROFILE_GUID],[TITLE],[PATH]," +
                "[IS_ACTIVE],[CREATE_TIME],[CREATE_USER]) Values ");
            sb.Append(" ( ");
            sb.Append("  @PROFILE_GUID");
            sb.Append(" ,@TITLE");
            sb.Append(" ,@PATH");
            sb.Append(" ,@IS_ACTIVE");           
            sb.Append(" ,@CREATE_TIME");
            sb.Append(" ,@CREATE_USER");
            sb.Append(" ) ");
            _Context._Dbm.AddParameter<TB_PROFILE>(p_data);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());

           
        }

        internal List<TB_PROFILE> SearchByKeyWord(string p_KeyWord)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_PROFILE where TITLE like '%'+@p_KeyWord+'%' ");
            _Context._Dbm.AddParameter("@p_KeyWord", p_KeyWord);
            return _Context._Dbm.ExecuteReader<TB_PROFILE>(sb.ToString());
          
            
        }

        internal TB_PROFILE SearchByGUID(string p_PROFILE_GUID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_PROFILE where PROFILE_GUID=@PROFILE_GUID");
            _Context._Dbm.AddParameter("@PROFILE_GUID", p_PROFILE_GUID);
            List<TB_PROFILE> l_TB_PROFILE = _Context._Dbm.ExecuteReader<TB_PROFILE>(sb.ToString());
            if (l_TB_PROFILE.Count > 0)
            {
                return l_TB_PROFILE[0];
            }
            return null;
        }

        public void Update(TB_PROFILE p_data)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Update TB_PROFILE Set ");
            sb.Append(" TITLE = @TITLE");
            sb.Append(" ,PATH = @PATH");
            sb.Append(" ,IS_ACTIVE = @IS_ACTIVE");

            sb.Append(" Where PROFILE_GUID= @PROFILE_GUID");
            _Context._Dbm.AddParameter("@TITLE", p_data.TITLE);
            _Context._Dbm.AddParameter("@PATH", p_data.PATH);
            _Context._Dbm.AddParameter("@IS_ACTIVE", p_data.IS_ACTIVE);
            _Context._Dbm.AddParameter("@PROFILE_GUID", p_data.PROFILE_GUID);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());
        }

        

        public void DeleteByGuid(string p_guid)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Delete From TB_PROFILE Where PROFILE_GUID = @PROFILE_GUID");
            _Context._Dbm.AddParameter("@PROFILE_GUID", p_guid);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());
        }
    }
}
