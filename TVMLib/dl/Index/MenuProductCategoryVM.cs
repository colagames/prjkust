﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVMLib.dl.Index
{
    public class MenuProductCategoryVM
    {
        ///<summary>
        ///類別名稱
        ///</summary>
        [DisplayName("類別名稱")]
        public string NAME { get; set; }

        ///<summary>
        ///GUID
        ///</summary>
        [DisplayName("GUID")]

        public string CATEGORY_GUID { get; set; }

        public List<MenuProductVM> l_MenuProductVM { get; set; }
    }
}
