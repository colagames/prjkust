﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVMLib.dl.Index
{
    public class MenuProductVM
    {
    

        ///<summary>
        ///帳號
        ///</summary>
        [DisplayName("連結名稱")]
        public string NAME { get; set; }

        ///<summary>
        ///連結URL
        ///</summary>
        [DisplayName("連結URL")]

        public string URL { get; set; }

        ///<summary>
        ///PRODUCT_GUID
        ///</summary>
        [DisplayName("GUID")]

        public string PRODUCT_GUID { get; set; }
    }
}
