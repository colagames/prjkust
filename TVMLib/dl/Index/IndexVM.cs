﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVMLib.dl.Index
{
    public class IndexVM
    {
        ///<summary>
        ///姓名
        ///</summary>
        [Display(Name = "姓名")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string CNAME { get; set; }

        ///<summary>
        ///電子信箱
        ///</summary>
        [DisplayName("電子信箱")]
        [DataType(DataType.EmailAddress, ErrorMessage = "請輸入正確的電子信箱")]  //要求欄位是Email格式，與[EmailAddress]相同
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(100, ErrorMessage = "{0}長度不可超過100。")]
        [RegularExpression(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "EMAIL格式錯誤")]
        public string CEMAIL { get; set; }

        ///<summary>
        ///聯絡內容
        ///</summary>
        [Display(Name = "聯絡內容")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]

        public string CMAIN { get; set; }


        public List<HotProductVM> l_HotProductVM { get; set; }
        public List<TB_NEWS> l_TB_NEWS { get; set; }


        public List<TB_PRODUCT_CATEGORY> l_TB_PRODUCT_CATEGORY { get; set; }

    }
}
