﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVMLib.dl.QA
{
    public class QAVM
    {

       
        [Display(Name = "類別")]
        public string CATEGORY { get; set; }

        public List<TB_QA> l_TB_QA { get; set; }

    }
}
