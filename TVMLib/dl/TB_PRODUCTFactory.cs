﻿using BaseLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVMLib.dl.AdminProduct;
using TVMLib.dl.Product;

namespace TVMLib.dl
{
    public class TB_PRODUCTFactory : BaseFactory
    {
        public TB_PRODUCTFactory(BaseContext p_context)
        {
            SetContext(p_context);
        }

        public List<TB_PRODUCT> Search()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_PRODUCT Order by CREATE_TIME");
            return _Context._Dbm.ExecuteReader<TB_PRODUCT>(sb.ToString());
        }

        public void Insert(TB_PRODUCT p_data)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Insert Into [TB_PRODUCT] ([PRODUCT_GUID],[CATEGORY_GUID],[NAME]," +
                "[PRICE],[DESCRIPTION],[PIC1],[PIC2],[PIC3],[PIC4],[PIC5],[PIC6],[IS_INQUIRY],[IS_DELETE]," +
                "[CREATE_TIME],[CREATE_USER],[EDIT_TIME],[EDIT_USER]) Values ");
            sb.Append(" ( ");
            sb.Append("  @PRODUCT_GUID");
            sb.Append(" ,@CATEGORY_GUID");
            sb.Append(" ,@NAME");
            sb.Append(" ,@PRICE");
            sb.Append(" ,@DESCRIPTION");
            sb.Append(" ,@PIC1");
            sb.Append(" ,@PIC2");
            sb.Append(" ,@PIC3");
            sb.Append(" ,@PIC4");
            sb.Append(" ,@PIC5");
            sb.Append(" ,@PIC6");
            sb.Append(" ,@IS_INQUIRY");     
            sb.Append(" ,@IS_DELETE");     
            sb.Append(" ,@CREATE_TIME");
            sb.Append(" ,@CREATE_USER");
            sb.Append(" ,@EDIT_TIME");
            sb.Append(" ,@EDIT_USER");
            sb.Append(" ) ");
            _Context._Dbm.AddParameter<TB_PRODUCT>(p_data);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());

           
        }

       

        internal List<AdminProductVM> SearchAllAdminProductVMByGUID()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select TB_PRODUCT.*,TB_PRODUCT_CATEGORY.NAME as CATEGORY_NAME  from TB_PRODUCT " +
                "inner join TB_PRODUCT_CATEGORY on TB_PRODUCT_CATEGORY.CATEGORY_GUID = TB_PRODUCT.CATEGORY_GUID " +
                "Order by CREATE_TIME");
            return _Context._Dbm.ExecuteReader<AdminProductVM>(sb.ToString());
        }

      

        internal List<TB_PRODUCT> SearchByp_CategoryGUID(string p_CATEGORY_GUID,bool p_IsDeleteOrNot)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_PRODUCT where IS_DELETE=@p_IsDeleteOrNot and  CATEGORY_GUID=@CATEGORY_GUID");
            _Context._Dbm.AddParameter("@p_IsDeleteOrNot", p_IsDeleteOrNot);
            _Context._Dbm.AddParameter("@CATEGORY_GUID", p_CATEGORY_GUID);


            return _Context._Dbm.ExecuteReader<TB_PRODUCT>(sb.ToString());
        }

        internal List<TB_PRODUCT> SearchByKeyWord(string p_KeyWord)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_PRODUCT where NAME like '%'+@p_KeyWord+'%' ");
            _Context._Dbm.AddParameter("@p_KeyWord", p_KeyWord);
            return _Context._Dbm.ExecuteReader<TB_PRODUCT>(sb.ToString());
          
            
        }

        internal TB_PRODUCT SearchByGUID(string p_PRODUCT_GUID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_PRODUCT where PRODUCT_GUID=@PRODUCT_GUID");
            _Context._Dbm.AddParameter("@PRODUCT_GUID", p_PRODUCT_GUID);
            List<TB_PRODUCT> l_TB_PRODUCT = _Context._Dbm.ExecuteReader<TB_PRODUCT>(sb.ToString());
            if (l_TB_PRODUCT.Count > 0)
            {
                return l_TB_PRODUCT[0];
            }
            return null;
        }

        public void Update(TB_PRODUCT p_data)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Update TB_PRODUCT Set ");
            sb.Append(" NAME = @NAME");
            sb.Append(" ,PRICE = @PRICE");
            sb.Append(" ,DESCRIPTION = @DESCRIPTION");
            sb.Append(" ,PIC1 = @PIC1");
            sb.Append(" ,PIC2 = @PIC2");
            sb.Append(" ,PIC3 = @PIC3");
            sb.Append(" ,PIC4 = @PIC4");
            sb.Append(" ,PIC5 = @PIC5");
            sb.Append(" ,PIC6 = @PIC6");
            sb.Append(" ,IS_INQUIRY = @IS_INQUIRY");
            sb.Append(" ,IS_DELETE = @IS_DELETE");
            sb.Append(" ,EDIT_TIME = @EDIT_TIME");
            sb.Append(" ,EDIT_USER = @EDIT_USER");
            sb.Append(" ,CATEGORY_GUID = @CATEGORY_GUID");
            sb.Append(" Where PRODUCT_GUID= @PRODUCT_GUID");
            _Context._Dbm.AddParameter("@NAME", p_data.NAME);
            _Context._Dbm.AddParameter("@PRICE", p_data.PRICE);
            _Context._Dbm.AddParameter("@DESCRIPTION", p_data.DESCRIPTION);
            _Context._Dbm.AddParameter("@PIC1", p_data.PIC1==null?"": p_data.PIC1);
            _Context._Dbm.AddParameter("@PIC2", p_data.PIC2 == null ? "" : p_data.PIC2);
            _Context._Dbm.AddParameter("@PIC3", p_data.PIC3 == null ? "" : p_data.PIC3);
            _Context._Dbm.AddParameter("@PIC4", p_data.PIC4 == null ? "" : p_data.PIC4);
            _Context._Dbm.AddParameter("@PIC5", p_data.PIC5 == null ? "" : p_data.PIC5);
            _Context._Dbm.AddParameter("@PIC6", p_data.PIC6 == null ? "" : p_data.PIC6);
            _Context._Dbm.AddParameter("@IS_INQUIRY", p_data.IS_INQUIRY);
            _Context._Dbm.AddParameter("@IS_DELETE", p_data.IS_DELETE);
            _Context._Dbm.AddParameter("@CATEGORY_GUID", p_data.CATEGORY_GUID);
            _Context._Dbm.AddParameter("@PRODUCT_GUID", p_data.PRODUCT_GUID);
            _Context._Dbm.AddParameter("@EDIT_TIME", p_data.EDIT_TIME);
            _Context._Dbm.AddParameter("@EDIT_USER", p_data.EDIT_USER);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());
        }

        

        public void DeleteByGuid(string p_guid)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Delete From TB_PRODUCT Where PRODUCT_GUID = @PRODUCT_GUID");
            _Context._Dbm.AddParameter("@PRODUCT_GUID", p_guid);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());
        }


        internal AdminProductVM SearchAdminProductVMByGUID(string p_PRODUCT_GUID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select TB_PRODUCT.*,TB_PRODUCT_CATEGORY.NAME as CATEGORY_NAME  from TB_PRODUCT " +
               "inner join TB_PRODUCT_CATEGORY on TB_PRODUCT_CATEGORY.CATEGORY_GUID = TB_PRODUCT.CATEGORY_GUID " +
               "where TB_PRODUCT.PRODUCT_GUID = @PRODUCT_GUID " +
               "Order by CREATE_TIME");
            _Context._Dbm.AddParameter("@PRODUCT_GUID", p_PRODUCT_GUID);
            List<AdminProductVM> l_TB_PRODUCT = _Context._Dbm.ExecuteReader<AdminProductVM>(sb.ToString());
            if (l_TB_PRODUCT.Count > 0)
            {
                return l_TB_PRODUCT[0];
            }
            return null;
        }

        internal ProductVM SearchProductVMByGUID(string p_RRODUCT_GUID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select TB_PRODUCT.*,TB_PRODUCT_CATEGORY.NAME as CATEGORY_NAME  from TB_PRODUCT " +
               "inner join TB_PRODUCT_CATEGORY on TB_PRODUCT_CATEGORY.CATEGORY_GUID = TB_PRODUCT.CATEGORY_GUID " +
               "where TB_PRODUCT.PRODUCT_GUID = @PRODUCT_GUID " +
               "Order by CREATE_TIME");
            _Context._Dbm.AddParameter("@PRODUCT_GUID", p_RRODUCT_GUID);
            List<ProductVM> l_TB_PRODUCT = _Context._Dbm.ExecuteReader<ProductVM>(sb.ToString());
            if (l_TB_PRODUCT.Count > 0)
            {
                return l_TB_PRODUCT[0];
            }
            return null;
        }
    }
}
