﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVMLib.dl
{
    public class TB_ABOUTUS
    {
        ///<summary>
		///ABOUTUS_GUID
		///</summary>
		[Display(Name = "PK")]     
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string ABOUTUS_GUID { get; set; }

        ///<summary>
        ///關於我們內容(上)
        ///</summary>
        [Display(Name = "關於我們內容(上)")]
        [Required(ErrorMessage = "{0}欄位必填")]     
        public string ABOUTUS_MAIN1 { get; set; }


        ///<summary>
        ///關於我們內容(下)
        ///</summary>
        [Display(Name = "關於我們內容(下)")]
        [Required(ErrorMessage = "{0}欄位必填")]
        public string ABOUTUS_MAIN2 { get; set; }

        ///<summary>
        ///影片連結
        ///</summary>
        [Display(Name = "影片連結")]
        [Required(ErrorMessage = "{0}欄位必填")]
        public string ABOUTUS_LINK { get; set; }

        ///<summary>
        ///影片連結文字
        ///</summary>
        [Display(Name = "影片連結文字")]
        [Required(ErrorMessage = "{0}欄位必填")]
        public string ABOUTUS_LINK_TEXT { get; set; }


    }
}
