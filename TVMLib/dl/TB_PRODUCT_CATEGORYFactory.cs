﻿using BaseLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVMLib.dl.Index;

namespace TVMLib.dl
{
    public class TB_PRODUCT_CATEGORYFactory : BaseFactory
    {
        public TB_PRODUCT_CATEGORYFactory(BaseContext p_context)
        {
            SetContext(p_context);
        }

        public List<TB_PRODUCT_CATEGORY> Search()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_PRODUCT_CATEGORY Where IS_DELETE = '0' Order by CREATE_TIME");
            return _Context._Dbm.ExecuteReader<TB_PRODUCT_CATEGORY>(sb.ToString());
        }

        public List<HotProductVM> SearchHotProductTop9Category()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select TB_PRODUCT.*,TB_PRODUCT_CATEGORY.NAME as CATEGORY_NAME from TB_PRODUCT ");
            sb.Append(" inner join TB_PRODUCT_CATEGORY  on TB_PRODUCT_CATEGORY.CATEGORY_GUID = TB_PRODUCT.CATEGORY_GUID ");
            sb.Append(" Where TB_PRODUCT.IS_DELETE = '0' and TB_PRODUCT.PRODUCT_GUID = (select  PRODUCT_GUID from( ");
            sb.Append(" select top 9  count(PRODUCT_GUID) as cnt, PRODUCT_GUID ");
            sb.Append(" from TB_ORDER_DETAIL ");
            sb.Append(" group by PRODUCT_GUID ");
            sb.Append(" order by cnt desc) as TB1) ");
            sb.Append(" Order by CREATE_TIME ");
            return _Context._Dbm.ExecuteReader<HotProductVM>(sb.ToString());
        }

        public void Insert(TB_PRODUCT_CATEGORY p_data)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Insert Into [TB_PRODUCT_CATEGORY] ([CATEGORY_GUID],PIC_PATH,[NAME],[NOTE],[IS_DELETE]," +
                "[CREATE_TIME],[CREATE_USER],[EDIT_TIME],[EDIT_USER]) Values ");
            sb.Append(" ( ");
            sb.Append("  @CATEGORY_GUID");
            sb.Append(" ,@PIC_PATH");
            sb.Append(" ,@NAME");
            sb.Append(" ,@NOTE");
            sb.Append(" ,@IS_DELETE");     
            sb.Append(" ,@CREATE_TIME");
            sb.Append(" ,@CREATE_USER");
            sb.Append(" ,@EDIT_TIME");
            sb.Append(" ,@EDIT_USER");
            sb.Append(" ) ");
            _Context._Dbm.AddParameter<TB_PRODUCT_CATEGORY>(p_data);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());

           
        }

        internal List<TB_PRODUCT_CATEGORY> SearchByKeyWord(string p_KeyWord)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_PRODUCT_CATEGORY where NAME like '%'+@p_KeyWord+'%' ");
            _Context._Dbm.AddParameter("@p_KeyWord", p_KeyWord);
            return _Context._Dbm.ExecuteReader<TB_PRODUCT_CATEGORY>(sb.ToString());
          
            
        }

        internal TB_PRODUCT_CATEGORY SearchByGUID(string p_CATEGORY_GUID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_PRODUCT_CATEGORY where CATEGORY_GUID=@CATEGORY_GUID");
            _Context._Dbm.AddParameter("@CATEGORY_GUID", p_CATEGORY_GUID);
            List<TB_PRODUCT_CATEGORY> l_TB_PRODUCT_CATEGORY = _Context._Dbm.ExecuteReader<TB_PRODUCT_CATEGORY>(sb.ToString());
            if (l_TB_PRODUCT_CATEGORY.Count > 0)
            {
                return l_TB_PRODUCT_CATEGORY[0];
            }
            return null;
        }

        public void Update(TB_PRODUCT_CATEGORY p_data)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Update TB_PRODUCT_CATEGORY Set ");
            sb.Append(" NAME = @NAME");

            sb.Append(" ,PIC_PATH = @PIC_PATH");
            sb.Append(" ,NOTE = @NOTE");
            sb.Append(" ,IS_DELETE = @IS_DELETE");
            sb.Append(" ,EDIT_TIME = @EDIT_TIME");
            sb.Append(" ,EDIT_USER = @EDIT_USER");
            sb.Append(" Where CATEGORY_GUID= @CATEGORY_GUID");
            _Context._Dbm.AddParameter("@NAME", p_data.NAME);
            _Context._Dbm.AddParameter("@PIC_PATH", p_data.PIC_PATH);
            _Context._Dbm.AddParameter("@NOTE", p_data.NOTE);
   
            _Context._Dbm.AddParameter("@IS_DELETE", p_data.IS_DELETE);
            _Context._Dbm.AddParameter("@CATEGORY_GUID", p_data.CATEGORY_GUID);
            _Context._Dbm.AddParameter("@EDIT_TIME", p_data.EDIT_TIME);
            _Context._Dbm.AddParameter("@EDIT_USER", p_data.EDIT_USER);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());
        }

        

        public void DeleteByGuid(string p_guid)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Delete From TB_PRODUCT_CATEGORY Where CATEGORY_GUID = @CATEGORY_GUID");
            _Context._Dbm.AddParameter("@CATEGORY_GUID", p_guid);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());
        }
    }
}
