﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TVMLib.Validate;

namespace TVMLib.dl.AdminInquiry
{
    public class AdminInquiryVM
    {
        ///<summary>
        ///INQUIRY_GUID
        ///</summary>
        [Display(Name = "PK")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string INQUIRY_GUID { get; set; }
        
        ///<summary>
        ///單價
        ///</summary>
        [Display(Name = "回覆單價")]
        public Double RE_PRICE { get; set; }


        ///<summary>
        ///數量
        ///</summary>
        [Display(Name = "回覆數量")]
        public Int32 RE_COUNT { get; set; }

        ///<summary>
        ///總計
        ///</summary>
        [Display(Name = "總計")]
        public Int32 RE_TOTAL { get; set; }

        ///<summary>
        ///處理內容
        ///</summary>
        [Display(Name = "處理內容")]
        public String RE_PROCESS { get; set; }


      
        ///<summary>
        ///詢價內容
        ///</summary>
        [DisplayName("詢價內容")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(500, ErrorMessage = "{0}長度不可超過50。")]
        public string MAIN { get; set; }

      
    }
}
