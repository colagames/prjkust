﻿using BaseLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TVMLib.dl
{
    public class TB_ORDERFactory : BaseFactory
    {
        public TB_ORDERFactory(BaseContext p_context)
        {
            SetContext(p_context);
        }

        public List<TB_ORDER> Search()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_ORDER Order by CREATE_TIME");
            return _Context._Dbm.ExecuteReader<TB_ORDER>(sb.ToString());
        }

        public void Insert(TB_ORDER p_data)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Insert Into [TB_ORDER] ([ORDER_GUID],[ORDER_NO],[TOTAL_AMOUNT],[USER_GUID]," +
                "[CITY],[AREA],[ZIPCODE],[ADDRESS],[CREATE_TIME],[CREATE_USER],[EDIT_TIME],[EDIT_USER]) Values ");
            sb.Append(" ( ");
            sb.Append("  @ORDER_GUID");
            sb.Append(" ,@ORDER_NO");
            sb.Append(" ,@TOTAL_AMOUNT");
            sb.Append(" ,@USER_GUID");
            sb.Append(" ,@CITY");
            sb.Append(" ,@AREA");
            sb.Append(" ,@ZIPCODE");
            sb.Append(" ,@ADDRESS");
            sb.Append(" ,@CREATE_TIME");
            sb.Append(" ,@CREATE_USER");
            sb.Append(" ,@EDIT_TIME");
            sb.Append(" ,@EDIT_USER");
            sb.Append(" ) ");
            _Context._Dbm.AddParameter<TB_ORDER>(p_data);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());


        }

        internal List<TB_ORDER> SearchByProductOrOrderNameKeyWord(string p_KeyWord)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select TB_ORDER.* from TB_ORDER " +
                " inner join TB_USER on TB_USER.USER_GUID = TB_ORDER.USER_GUID" +
                " where TB_USER.NAME like '%'+@p_KeyWord1+'%' ");
            _Context._Dbm.AddParameter("@p_KeyWord1", p_KeyWord);
            _Context._Dbm.AddParameter("@p_KeyWord2", p_KeyWord);
            return _Context._Dbm.ExecuteReader<TB_ORDER>(sb.ToString());


        }

        internal TB_ORDER SearchByGUID(string p_ORDER_GUID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_ORDER where ORDER_GUID=@ORDER_GUID");
            _Context._Dbm.AddParameter("@ORDER_GUID", p_ORDER_GUID);
            List<TB_ORDER> l_TB_ORDER = _Context._Dbm.ExecuteReader<TB_ORDER>(sb.ToString());
            if (l_TB_ORDER.Count > 0)
            {
                return l_TB_ORDER[0];
            }
            return null;
        }

        internal List<TB_ORDER> SearchByOrderNo(string p_KeyWord)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_ORDER where ORDER_NO like '%'+@ORDER_NO+'%'");
            _Context._Dbm.AddParameter("@ORDER_NO", p_KeyWord);
            return _Context._Dbm.ExecuteReader<TB_ORDER>(sb.ToString());
        }

        public void Update(TB_ORDER p_data)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Update TB_ORDER Set ");
            sb.Append(" TOTAL_AMOUNT = @TOTAL_AMOUNT");
            sb.Append(" ,CITY = @CITY");
            sb.Append(" ,AREA = @AREA");
            sb.Append(" ,ZIPCODE = @ZIPCODE");
            sb.Append(" ,ADDRESS = @ADDRESS");
            sb.Append(" ,EDIT_TIME = @EDIT_TIME");
            sb.Append(" ,EDIT_USER = @EDIT_USER");
            sb.Append(" Where ORDER_GUID= @ORDER_GUID");
            _Context._Dbm.AddParameter("@TOTAL_AMOUNT", p_data.TOTAL_AMOUNT);
            _Context._Dbm.AddParameter("@CITY", p_data.CITY);
            _Context._Dbm.AddParameter("@AREA", p_data.AREA);
            _Context._Dbm.AddParameter("@ZIPCODE", p_data.ZIPCODE);
            _Context._Dbm.AddParameter("@ADDRESS", p_data.ADDRESS);
            _Context._Dbm.AddParameter("@ORDER_GUID", p_data.ORDER_GUID);
            _Context._Dbm.AddParameter("@EDIT_TIME", p_data.EDIT_TIME);
            _Context._Dbm.AddParameter("@EDIT_USER", p_data.EDIT_USER);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());
        }



        public void DeleteByGuid(string p_guid)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Delete From TB_ORDER Where ORDER_GUID = @ORDER_GUID");
            _Context._Dbm.AddParameter("@ORDER_GUID", p_guid);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());
        }
    }
}
