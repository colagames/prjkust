﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TVMLib.Validate;

namespace TVMLib.dl.AdminProCategory
{
    public class AdminProCategoryVM
    {
        ///<summary>
		///CATEGORY_GUID
		///</summary>
		[Display(Name = "PK")]     
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string CATEGORY_GUID { get; set; }


        [Display(Name = "圖片1")]
        [ValidateFilePhoto]
        public HttpPostedFileBase PIC_PATH_UPLOAD { get; set; }

        ///<summary>
        ///CATEGORY_GUID
        ///</summary>
        [Display(Name = "圖檔")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(500, ErrorMessage = "{0}長度不可超過500。")]
        public string PIC_PATH { get; set; }

        

        ///<summary>
        ///類別名稱
        ///</summary>
        [Display(Name = "類別名稱")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string NAME { get; set; }

        ///<summary>
        ///類別介紹
        ///</summary>
        [Display(Name = "類別介紹")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(509, ErrorMessage = "{0}長度不可超過50。")]
        public string NOTE { get; set; }
        ///<summary>
        ///是否刪除
        ///</summary>
        [Display(Name = "是否刪除")]
        [Required(ErrorMessage = "{0}欄位必填")]
        public bool IS_DELETE { get; set; }

        ///<summary>
        ///CREATE_TIME
        ///</summary>
        [Display(Name = "建立時間")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? CREATE_TIME { get; set; }

        ///<summary>
        ///CREATE_USER
        ///</summary>
        [Display(Name = "建立者")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string CREATE_USER { get; set; }

        ///<summary>
        ///EDIT_TIME
        ///</summary>
        [Display(Name = "修改時間")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? EDIT_TIME { get; set; }

        ///<summary>
        ///EDIT_USER
        ///</summary>
        [Display(Name = "修改者")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}", ApplyFormatInEditMode = true)]
        public string EDIT_USER { get; set; }

    }
}
