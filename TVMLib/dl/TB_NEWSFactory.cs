﻿using BaseLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TVMLib.dl
{
    public class TB_NEWSFactory : BaseFactory
    {
        public TB_NEWSFactory(BaseContext p_context)
        {
            SetContext(p_context);
        }

        public List<TB_NEWS> Search()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_NEWS where IS_ACTIVE = 1 Order by CREATE_TIME");
            return _Context._Dbm.ExecuteReader<TB_NEWS>(sb.ToString());
        }

        public void Insert(TB_NEWS p_data)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Insert Into [TB_NEWS] ([NEWS_GUID],[TITLE],[MAIN]," +
                "[IS_ACTIVE],[CREATE_TIME],[CREATE_USER],[EDIT_TIME],[EDIT_USER]) Values ");
            sb.Append(" ( ");
            sb.Append("  @NEWS_GUID");
            sb.Append(" ,@TITLE");
            sb.Append(" ,@MAIN");
            sb.Append(" ,@IS_ACTIVE");           
            sb.Append(" ,@CREATE_TIME");
            sb.Append(" ,@CREATE_USER");
            sb.Append(" ,@EDIT_TIME");
            sb.Append(" ,@EDIT_USER");
            sb.Append(" ) ");
            _Context._Dbm.AddParameter<TB_NEWS>(p_data);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());

           
        }

        internal List<TB_NEWS> SearchByKeyWord(string p_KeyWord)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_NEWS where TITLE like '%'+@p_KeyWord+'%' ");
            _Context._Dbm.AddParameter("@p_KeyWord", p_KeyWord);
            return _Context._Dbm.ExecuteReader<TB_NEWS>(sb.ToString());
          
            
        }

        internal TB_NEWS SearchByGUID(string p_NEWS_GUID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from TB_NEWS where NEWS_GUID=@NEWS_GUID");
            _Context._Dbm.AddParameter("@NEWS_GUID", p_NEWS_GUID);
            List<TB_NEWS> l_TB_NEWS = _Context._Dbm.ExecuteReader<TB_NEWS>(sb.ToString());
            if (l_TB_NEWS.Count > 0)
            {
                return l_TB_NEWS[0];
            }
            return null;
        }

        public void Update(TB_NEWS p_data)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Update TB_NEWS Set ");
            sb.Append(" TITLE = @TITLE");
            sb.Append(" ,MAIN = @MAIN");
            sb.Append(" ,IS_ACTIVE = @IS_ACTIVE");
            sb.Append(" ,EDIT_TIME = @EDIT_TIME");
            sb.Append(" ,EDIT_USER = @EDIT_USER");
            sb.Append(" Where NEWS_GUID= @NEWS_GUID");
            _Context._Dbm.AddParameter("@TITLE", p_data.TITLE);
            _Context._Dbm.AddParameter("@MAIN", p_data.MAIN);
            _Context._Dbm.AddParameter("@IS_ACTIVE", p_data.IS_ACTIVE);
            _Context._Dbm.AddParameter("@NEWS_GUID", p_data.NEWS_GUID);
            _Context._Dbm.AddParameter("@EDIT_TIME", p_data.EDIT_TIME);
            _Context._Dbm.AddParameter("@EDIT_USER", p_data.EDIT_USER);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());
        }

        

        public void DeleteByGuid(string p_guid)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Delete From TB_NEWS Where NEWS_GUID = @NEWS_GUID");
            _Context._Dbm.AddParameter("@NEWS_GUID", p_guid);
            _Context._Dbm.EexcuteSqlNonquery(sb.ToString());
        }
    }
}
