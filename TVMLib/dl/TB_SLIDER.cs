﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVMLib.dl
{
    public class TB_SLIDER
    {
        ///<summary>
		///SLIDER_GUID
		///</summary>
		[Display(Name = "PK")]     
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string SLIDER_GUID { get; set; }

        ///<summary>
        ///小標題
        ///</summary>
        [Display(Name = "小標題")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string TITLE_SUB { get; set; }

        ///<summary>
        ///大標題
        ///</summary>
        [Display(Name = "大標題")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string TITLE_MAIN { get; set; }

        ///<summary>
        ///輪播圖
        ///</summary>
        [Display(Name = "輪播圖")]
        public String SLIDER_PATH { get; set; }

      
        ///<summary>
        ///CREATE_TIME
        ///</summary>
        [Display(Name = "建立時間")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime CREATE_TIME { get; set; }

        ///<summary>
        ///CREATE_USER
        ///</summary>
        [Display(Name = "建立者")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string CREATE_USER { get; set; }

     
    }
}
