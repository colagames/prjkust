﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVMLib.dl.Order
{
    public class OrderAndDetailVM
    {
        ///<summary>
		///ORDER_GUID
		///</summary>
		[Display(Name = "ORDER_GUID")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string ORDER_GUID { get; set; }

        ///<summary>
        ///訂單號碼
        ///</summary>
        [Display(Name = "訂單號碼")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string ORDER_NO { get; set; }

        ///<summary>
        ///總計
        ///</summary>
        [Display(Name = "總計")]
        public double TOTAL_AMOUNT { get; set; }

        ///<summary>
        ///USER_GUID
        ///</summary>
        [Display(Name = "USER_GUID")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string USER_GUID { get; set; }

        ///<summary>
        ///城市
        ///</summary>
        [Display(Name = "城市")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string CITY { get; set; }

        ///<summary>
        ///行政區
        ///</summary>
        [Display(Name = "行政區")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string AREA { get; set; }

        ///<summary>
        ///郵遞區號
        ///</summary>
        [Display(Name = "郵遞區號")]
        [StringLength(10, ErrorMessage = "{0}長度不可超過10。")]
        public string ZIPCODE { get; set; }

        ///<summary>
        ///地址
        ///</summary>
        [Display(Name = "地址")]
        [StringLength(500, ErrorMessage = "{0}長度不可超過500。")]
        public string ADDRESS { get; set; }

        ///<summary>
        ///建立時間
        ///</summary>
        [Display(Name = "建立時間")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime CREATE_TIME { get; set; }

        ///<summary>
        ///建立者
        ///</summary>
        [Display(Name = "建立者")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string CREATE_USER { get; set; }

        ///<summary>
        ///修改時間
        ///</summary>
        [Display(Name = "修改時間")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime EDIT_TIME { get; set; }

        ///<summary>
        ///EDIT_USER
        ///</summary>
        [Display(Name = "")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string EDIT_USER { get; set; }


        public List<TB_ORDER_DETAIL> l_TB_ORDER_DETAIL { get; set; }

        public TB_USER l_TB_USER { get; set; }
     
    }
}
