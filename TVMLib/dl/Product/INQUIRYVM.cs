﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVMLib.dl.Product
{
    public class InquiryVM
    {
        ///<summary>
        ///INQUIRY_GUID
        ///</summary>
        [Display(Name = "PK")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string INQUIRY_GUID { get; set; }
        ///<summary>
		///產品GUID
		///</summary>
		[Display(Name = "產品GUID")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string PRODUCT_GUID { get; set; }

        ///<summary>
        ///會員USER_GUID
        ///</summary>
        [Display(Name = "會員USER_GUID")]
        public string USER_GUID { get; set; }

        ///<summary>
        ///單價
        ///</summary>
        [Display(Name = "單價")]
        public Double RE_PRICE { get; set; }


        ///<summary>
        ///數量
        ///</summary>
        [Display(Name = "數量")]
        public Int32 RE_COUNT { get; set; }

        ///<summary>
        ///總計
        ///</summary>
        [Display(Name = "總計")]
        public Int32 RE_TOTAL { get; set; }

        ///<summary>
        ///處理內容
        ///</summary>
        [Display(Name = "處理內容")]
        public String RE_PROCESS { get; set; }


        ///<summary>
        ///姓名
        ///</summary>
        [Display(Name = "姓名")]
        [Required(ErrorMessage = "{0}欄位必填")]
        public String NAME { get; set; }

        ///<summary>
        ///電話
        ///</summary>
        [DisplayName("電話")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string TEL { get; set; }

        ///<summary>
        ///電子信箱
        ///</summary>
        [DisplayName("電子信箱")]
        [DataType(DataType.EmailAddress, ErrorMessage = "請輸入正確的電子信箱")]  //要求欄位是Email格式，與[EmailAddress]相同
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(100, ErrorMessage = "{0}長度不可超過100。")]
        [RegularExpression(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "EMAIL格式錯誤")]
        public string EMAIL { get; set; }

        ///<summary>
        ///詢價內容
        ///</summary>
        [DisplayName("詢價內容")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(500, ErrorMessage = "{0}長度不可超過50。")]
        public string MAIN { get; set; }

        ///<summary>
        ///CREATE_TIME
        ///</summary>
        [Display(Name = "建立時間")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? CREATE_TIME { get; set; }

        ///<summary>
        ///CREATE_USER
        ///</summary>
        [Display(Name = "建立者")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string CREATE_USER { get; set; }

        ///<summary>
        ///EDIT_TIME
        ///</summary>
        [Display(Name = "修改時間")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? EDIT_TIME { get; set; }

        ///<summary>
        ///EDIT_USER
        ///</summary>
        [Display(Name = "修改者")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}", ApplyFormatInEditMode = true)]
        public string EDIT_USER { get; set; }


        public List<ProductImageVM> l_ProductImageVM { get; set; }

        public TB_PRODUCT l_TB_PRODUCT { get; set; }

    }
}
