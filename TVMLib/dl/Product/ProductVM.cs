﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVMLib.dl.Product
{
    public class ProductVM
    {
        ///<summary>
		///PRODUCT_GUID
		///</summary>
		[Display(Name = "PK")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string PRODUCT_GUID { get; set; }

        ///<summary>
        ///CATEGORY_GUID
        ///</summary>
        [Display(Name = "產品類別")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string CATEGORY_GUID { get; set; }

        ///<summary>
        ///產品名稱
        ///</summary>
        [Display(Name = "產品名稱")]
        [Required(ErrorMessage = "{0}欄位必填")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string NAME { get; set; }

        ///<summary>
        ///單價
        ///</summary>
        [Display(Name = "單價")]
        [Required(ErrorMessage = "{0}欄位必填")]
        public Double PRICE { get; set; }

        ///<summary>
        ///產品描述
        ///</summary>
        [Display(Name = "產品描述")]
        public String DESCRIPTION { get; set; }


        ///<summary>
        ///圖片1
        ///</summary>
        [Display(Name = "圖片1")]
        public String PIC1 { get; set; }

        ///<summary>
        ///圖片2
        ///</summary>
        [Display(Name = "圖片2")]
        public String PIC2 { get; set; }
        ///<summary>
        ///圖片3
        ///</summary>
        [Display(Name = "圖片3")]
        public String PIC3 { get; set; }
        ///<summary>
        ///圖片4
        ///</summary>
        [Display(Name = "圖片4")]
        public String PIC4 { get; set; }
        ///<summary>
        ///圖片5
        ///</summary>
        [Display(Name = "圖片5")]
        public String PIC5 { get; set; }
        ///<summary>
        ///圖片6
        ///</summary>
        [Display(Name = "圖片6")]
        public String PIC6 { get; set; }

        ///<summary>
        ///是否開放詢價功能
        ///</summary>
        [Display(Name = "是否開放詢價功能")]
        [Required(ErrorMessage = "{0}欄位必填")]
        public bool IS_INQUIRY { get; set; }

        ///<summary>
        ///是否刪除
        ///</summary>
        [Display(Name = "是否刪除")]
        [Required(ErrorMessage = "{0}欄位必填")]
        public bool IS_DELETE { get; set; }

        ///<summary>
        ///CREATE_TIME
        ///</summary>
        [Display(Name = "建立時間")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? CREATE_TIME { get; set; }

        ///<summary>
        ///CREATE_USER
        ///</summary>
        [Display(Name = "建立者")]
        [StringLength(50, ErrorMessage = "{0}長度不可超過50。")]
        public string CREATE_USER { get; set; }

        ///<summary>
        ///EDIT_TIME
        ///</summary>
        [Display(Name = "修改時間")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? EDIT_TIME { get; set; }

        ///<summary>
        ///EDIT_USER
        ///</summary>
        [Display(Name = "修改者")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}", ApplyFormatInEditMode = true)]
        public string EDIT_USER { get; set; }



        public List<ProductImageVM> l_ProductImageVM { get; set; }

        public string  CATEGORY_NAME { get; set; }

        /// <summary>
        /// 詢價用
        /// </summary>
        public TB_INQUIRY l_TB_INQUIRY { get; set; }


        /// <summary>
        /// 數量
        /// </summary>
        [Display(Name = "數量")]
        [Range(1, 1000)]
        [Required(ErrorMessage = "{0}欄位填")]
        public int UNIT { get; set; }
    }
}
