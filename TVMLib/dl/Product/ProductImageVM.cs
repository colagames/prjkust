﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVMLib.dl.Product
{
    public class ProductImageVM
    {
        ///<summary>
		///名稱
		///</summary>
		[Display(Name = "名稱")]
        public string NAME { get; set; }

        ///<summary>
        ///圖片路徑
        ///</summary>
        [Display(Name = "圖片路徑")]
        public string URL { get; set; }
    }
}
