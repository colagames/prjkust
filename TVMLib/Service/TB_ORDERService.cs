﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseLib;
using TVMLib.dl;
using BaseLib.Interface;
using TVMLib.common;
using TVMLib.dl.Order;

namespace TVMLib.Service
{
    public class TB_ORDERService : BaseService, IService<TB_ORDER>
    {

        private TB_ORDERFactory _TB_ORDERFactory;
        private TB_USERFactory _TB_USERFactory;
        private TB_PRODUCTFactory _TB_PRODUCTFactory;
        private TB_ORDER_DETAILFactory _TB_ORDER_DETAILFactory;

        public TB_ORDERService(BaseContext p_Context) : base(p_Context)
        {
            if (_TB_ORDERFactory == null)
            {
                _TB_ORDERFactory = new TB_ORDERFactory(p_Context);
            }
            if (_TB_USERFactory == null)
            {
                _TB_USERFactory = new TB_USERFactory(p_Context);
            }
            if (_TB_PRODUCTFactory == null)
            {
                _TB_PRODUCTFactory = new TB_PRODUCTFactory(p_Context);
            }
            if (_TB_ORDER_DETAILFactory == null)
            {
                _TB_ORDER_DETAILFactory = new TB_ORDER_DETAILFactory(p_Context);
            }
        }

        public OrderAndDetailVM SearchAllOrderAndDetailByORDER_GUID(string p_ORDER_GUID)
        {
            OrderAndDetailVM l_OrderAndDetailVM = new OrderAndDetailVM();
            TB_ORDER l_TB_ORDER = new TB_ORDER();


            l_TB_ORDER = _TB_ORDERFactory.SearchByGUID(p_ORDER_GUID);

            OrderAndDetailVM l_data = new OrderAndDetailVM();
            l_data.ADDRESS = l_TB_ORDER.ADDRESS;
            l_data.AREA = l_TB_ORDER.AREA;
            l_data.CITY = l_TB_ORDER.CITY;
            l_data.CREATE_TIME = l_TB_ORDER.CREATE_TIME;
            l_data.CREATE_USER = l_TB_ORDER.CREATE_USER;
            l_data.EDIT_TIME = l_TB_ORDER.EDIT_TIME;
            l_data.EDIT_USER = l_TB_ORDER.EDIT_USER;
            l_data.ORDER_GUID = l_TB_ORDER.ORDER_GUID;
            l_data.ORDER_NO = l_TB_ORDER.ORDER_NO;
            l_data.TOTAL_AMOUNT = l_TB_ORDER.TOTAL_AMOUNT;
            l_data.USER_GUID = l_TB_ORDER.USER_GUID;
            l_data.ZIPCODE = l_TB_ORDER.ZIPCODE;
            l_data.l_TB_USER = _TB_USERFactory.SearchByGUID(l_TB_ORDER.USER_GUID);
            l_data.l_TB_ORDER_DETAIL = _TB_ORDER_DETAILFactory.SearchByORDER_GUID(l_TB_ORDER.ORDER_GUID);

            foreach (var order_detail in l_data.l_TB_ORDER_DETAIL)
            {
                order_detail.l_TB_PRODUCT = _TB_PRODUCTFactory.SearchByGUID(order_detail.PRODUCT_GUID);
            }
            l_OrderAndDetailVM = l_data;


            return l_OrderAndDetailVM;
        }

        public void Create(TB_ORDER p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_ORDERFactory.Insert(p_Datas);
            _BaseContext._Dbm.Commit();

        }



        public void Delete(TB_ORDER p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_ORDERFactory.DeleteByGuid(p_Datas.ORDER_GUID);
            _BaseContext._Dbm.Commit();
        }



        public List<TB_ORDER> Search()
        {
            return _TB_ORDERFactory.Search();
        }

        public void Update(TB_ORDER p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_ORDERFactory.Update(p_Datas);
            _BaseContext._Dbm.Commit();
        }

        public TB_ORDER SearchByGUID(string p_GUID)
        {
            return _TB_ORDERFactory.SearchByGUID(p_GUID);
        }
        public List<OrderAndDetailVM> SearchAllOrderAndDetail(string p_KeyWord)
        {
            List<OrderAndDetailVM> l_OrderAndDetailVM = new List<OrderAndDetailVM>();
            List<TB_ORDER> l_TB_ORDER = new List<TB_ORDER>();
            if (!string.IsNullOrEmpty(p_KeyWord))
            {
                l_TB_ORDER = _TB_ORDERFactory.SearchByOrderNo(p_KeyWord);
            }
            else
            {
                l_TB_ORDER = _TB_ORDERFactory.Search();
            }


            foreach (var item in l_TB_ORDER)
            {
                OrderAndDetailVM l_data = new OrderAndDetailVM();
                l_data.ADDRESS = item.ADDRESS;
                l_data.AREA = item.AREA;
                l_data.CITY = item.CITY;
                l_data.CREATE_TIME = item.CREATE_TIME;
                l_data.CREATE_USER = item.CREATE_USER;
                l_data.EDIT_TIME = item.EDIT_TIME;
                l_data.EDIT_USER = item.EDIT_USER;
                l_data.ORDER_GUID = item.ORDER_GUID;
                l_data.ORDER_NO = item.ORDER_NO;
                l_data.TOTAL_AMOUNT = item.TOTAL_AMOUNT;
                l_data.USER_GUID = item.USER_GUID;
                l_data.ZIPCODE = item.ZIPCODE;
                l_data.l_TB_USER = _TB_USERFactory.SearchByGUID(item.USER_GUID);
                l_data.l_TB_ORDER_DETAIL = _TB_ORDER_DETAILFactory.SearchByORDER_GUID(item.ORDER_GUID);

                foreach (var order_detail in l_data.l_TB_ORDER_DETAIL)
                {
                    order_detail.l_TB_PRODUCT = _TB_PRODUCTFactory.SearchByGUID(order_detail.PRODUCT_GUID);
                }
                l_OrderAndDetailVM.Add(l_data);

            }


            return l_OrderAndDetailVM;
        }

        public void CreateOrderAndDetail(OrderAndDetailVM p_OrderAndDetailVM)
        {
            _BaseContext._Dbm.BeginTransaction();

            TB_ORDER l_TB_ORDER = new TB_ORDER();
            l_TB_ORDER.ADDRESS = p_OrderAndDetailVM.ADDRESS;
            l_TB_ORDER.AREA = p_OrderAndDetailVM.AREA;
            l_TB_ORDER.CITY = p_OrderAndDetailVM.CITY;
            l_TB_ORDER.CREATE_TIME = p_OrderAndDetailVM.CREATE_TIME;
            l_TB_ORDER.CREATE_USER = p_OrderAndDetailVM.CREATE_USER;
            l_TB_ORDER.EDIT_TIME = p_OrderAndDetailVM.EDIT_TIME;
            l_TB_ORDER.EDIT_USER = p_OrderAndDetailVM.EDIT_USER;
            l_TB_ORDER.ORDER_GUID = p_OrderAndDetailVM.ORDER_GUID;
            l_TB_ORDER.ORDER_NO = p_OrderAndDetailVM.ORDER_NO;
            l_TB_ORDER.USER_GUID = p_OrderAndDetailVM.USER_GUID;
            l_TB_ORDER.ZIPCODE = p_OrderAndDetailVM.ZIPCODE;
            l_TB_ORDER.TOTAL_AMOUNT = p_OrderAndDetailVM.TOTAL_AMOUNT;
            _TB_ORDERFactory.Insert(l_TB_ORDER);

            foreach (var item in p_OrderAndDetailVM.l_TB_ORDER_DETAIL)
            {
                item.ORDER_GUID = l_TB_ORDER.ORDER_GUID;
                _TB_ORDER_DETAILFactory.Insert(item);

            }
            _BaseContext._Dbm.Commit();
        }
    }
}
