﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseLib;
using TVMLib.dl;
using BaseLib.Interface;
using TVMLib.dl.Index;

namespace TVMLib.Service
{
    public class TB_PRODUCT_CATEGORYService : BaseService, IService<TB_PRODUCT_CATEGORY>
    {

        private TB_PRODUCT_CATEGORYFactory _TB_PRODUCT_CATEGORYFactory;

        public TB_PRODUCT_CATEGORYService(BaseContext p_Context) : base(p_Context)
        {
            if (_TB_PRODUCT_CATEGORYFactory == null)
            {
                _TB_PRODUCT_CATEGORYFactory = new TB_PRODUCT_CATEGORYFactory(p_Context);
            }
        }

        public void Create(TB_PRODUCT_CATEGORY p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_PRODUCT_CATEGORYFactory.Insert(p_Datas);
            _BaseContext._Dbm.Commit();

        }

        public void Delete(TB_PRODUCT_CATEGORY p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_PRODUCT_CATEGORYFactory.Update(p_Datas);
            _BaseContext._Dbm.Commit();
        }

     

        public List<TB_PRODUCT_CATEGORY> Search()
        {
            return _TB_PRODUCT_CATEGORYFactory.Search();
        }

        public void Update(TB_PRODUCT_CATEGORY p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_PRODUCT_CATEGORYFactory.Update(p_Datas);
            _BaseContext._Dbm.Commit();
        }

      


        public TB_PRODUCT_CATEGORY SearchByGUID(string p_GUID)
        {
            return _TB_PRODUCT_CATEGORYFactory. SearchByGUID(p_GUID);
        }

        internal List<HotProductVM> SearchHotProductTop9Category()
        {
            return _TB_PRODUCT_CATEGORYFactory.SearchHotProductTop9Category();
        }
    }
}
