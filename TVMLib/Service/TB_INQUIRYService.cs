﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseLib;
using TVMLib.dl;
using BaseLib.Interface;
using TVMLib.dl.AdminProduct;
using TVMLib.dl.Product;
using TVMLib.dl.AdminInquiry;

namespace TVMLib.Service
{
    public class TB_INQUIRYService : BaseService, IService<TB_INQUIRY>
    {

        private TB_INQUIRYFactory _TB_INQUIRYFactory;

        public TB_INQUIRYService(BaseContext p_Context) : base(p_Context)
        {
            if (_TB_INQUIRYFactory == null)
            {
                _TB_INQUIRYFactory = new TB_INQUIRYFactory(p_Context);
            }
        }

        public List<InquiryVM> SearchInquiryVM()
        {
            return _TB_INQUIRYFactory.SearchInquiryVM();
        }

        public void Create(TB_INQUIRY p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_INQUIRYFactory.Insert(p_Datas);
            _BaseContext._Dbm.Commit();

        }

        public AdminInquiryVM SearchAdminInquiryVMByGUID(string p_INQUIRY_GUID)
        {
            return _TB_INQUIRYFactory.SearchAdminInquiryVMByGUID(p_INQUIRY_GUID);
        }

        public void Delete(TB_INQUIRY p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_INQUIRYFactory.DeleteByGuid(p_Datas.INQUIRY_GUID);
            _BaseContext._Dbm.Commit();
        }

     

        public List<TB_INQUIRY> Search()
        {
            return _TB_INQUIRYFactory.Search();
        }

        public void Update(TB_INQUIRY p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_INQUIRYFactory.Update(p_Datas);
            _BaseContext._Dbm.Commit();
        }

  
    }
}
