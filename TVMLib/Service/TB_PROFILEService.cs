﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseLib;
using TVMLib.dl;
using BaseLib.Interface;


namespace TVMLib.Service
{
    public class TB_PROFILEService : BaseService, IService<TB_PROFILE>
    {

        private TB_PROFILEFactory _TB_PROFILEFactory;

        public TB_PROFILEService(BaseContext p_Context) : base(p_Context)
        {
            if (_TB_PROFILEFactory == null)
            {
                _TB_PROFILEFactory = new TB_PROFILEFactory(p_Context);
            }
        }

        public void Create(TB_PROFILE p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_PROFILEFactory.Insert(p_Datas);
            _BaseContext._Dbm.Commit();

        }

        public void Delete(TB_PROFILE p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_PROFILEFactory.DeleteByGuid(p_Datas.PROFILE_GUID);
            _BaseContext._Dbm.Commit();
        }

     

        public List<TB_PROFILE> Search()
        {
            return _TB_PROFILEFactory.Search();
        }

        public void Update(TB_PROFILE p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_PROFILEFactory.Update(p_Datas);
            _BaseContext._Dbm.Commit();
        }

      

        public TB_PROFILE SearchByGUID(string p_GUID)
        {
            return _TB_PROFILEFactory. SearchByGUID(p_GUID);
        }
      
    }
}
