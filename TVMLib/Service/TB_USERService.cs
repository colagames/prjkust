﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseLib;
using TVMLib.dl;
using BaseLib.Interface;
using TVMLib.common;

namespace TVMLib.Service
{
    public class TB_USERService : BaseService, IService<TB_USER>
    {

        private TB_USERFactory _TB_USERFactory;

        public TB_USERService(BaseContext p_Context) : base(p_Context)
        {
            if (_TB_USERFactory == null)
            {
                _TB_USERFactory = new TB_USERFactory(p_Context);
            }
        }

        public void Create(TB_USER p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_USERFactory.Insert(p_Datas);
            _BaseContext._Dbm.Commit();

        }

        public TB_USER SearchAdminByAccAndPwd(string p_ACCOUNT, string p_PASSWORD)
        {
            return _TB_USERFactory.SearchAdminByAccAndPwd(p_ACCOUNT, p_PASSWORD);
        }

        public void Delete(TB_USER p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_USERFactory.UpdateIsActiveByUSER_GUID(false, p_Datas.USER_GUID, DateTime.Now, Utility.UserData.USER_GUID);
            _BaseContext._Dbm.Commit();
        }



        public List<TB_USER> Search()
        {
            return _TB_USERFactory.Search();
        }

        public void Update(TB_USER p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_USERFactory.Update(p_Datas);
            _BaseContext._Dbm.Commit();
        }

        public void UpdateIsActiveByUSER_GUID(bool IS_ACTIVE, string p_USER_GUID, DateTime p_EDIT_TIME, string p_EDIT_USER)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_USERFactory.UpdateIsActiveByUSER_GUID(IS_ACTIVE, p_USER_GUID, p_EDIT_TIME, p_EDIT_USER);
            _BaseContext._Dbm.Commit();
        }


        public TB_USER SearchByGUID(string p_GUID)
        {
            return _TB_USERFactory.SearchByGUID(p_GUID);
        }
        public TB_USER SearchByAccAndPwd(string p_ACCOUNT, string p_PASSWORD)
        {
            return _TB_USERFactory.SearchByAccAndPwd(p_ACCOUNT, p_PASSWORD);
        }

        public bool GetNewPwdByEmail(string p_Email)
        {
            EmailHandle l_EmailHandle;
            string l_ErrMsg = "";
            try
            {
                l_EmailHandle = new EmailHandle(null);

                TB_USER l_UserData = _TB_USERFactory.SearchByEmail(p_Email);
                TB_USER l_EditUser = new TB_USER() { NAME = "取得新密碼" };
                l_UserData.PASSWORD = Utility.GenRandomString(10, 10);
                if (!UpdateAccound(l_UserData, l_UserData.USER_GUID, ref l_ErrMsg, l_EditUser))
                    throw new Exception(l_ErrMsg);

                if (l_EmailHandle.SendMail(new List<string>() { p_Email },
                                           "KUST Innovative 雄大科技 申請新密碼",
                                           $"新密碼為：{l_UserData.PASSWORD}",
                                           null, ref l_ErrMsg, l_EditUser, p_BodyIsHtml: false))
                    NLog.LogManager.GetCurrentClassLogger().Trace($"發送索取新密碼信件成功，EMAIL：{p_Email}、IP：{Utility.GetClientIP()}");
                else
                    throw new Exception(l_ErrMsg);
              
                return true;
            }
            catch (Exception p_EX)
            {
                NLog.LogManager.GetCurrentClassLogger().Error(p_EX);
                return false;
            }
            finally
            {
                l_EmailHandle = null;
            }
        }

        private bool UpdateAccound(TB_USER p_Datas, string p_Guid, ref string p_Msg, TB_USER p_CurrentUser)
        {
            TB_USER l_Old = _TB_USERFactory.SearchByGUID(p_Guid);
            if (l_Old == null)
            {
                p_Msg = "無此使用者";
                return false;
            }

            l_Old.EDIT_USER = p_CurrentUser.NAME;
            l_Old.EDIT_TIME = DateTime.Now;

            try
            {
                _BaseContext._Dbm.BeginTransaction();

                l_Old.PASSWORD = p_Datas.PASSWORD;

                _TB_USERFactory.Update(l_Old);
                _BaseContext._Dbm.Commit();
                return true;
            }
            catch (Exception p_EX)
            {
                _BaseContext._Dbm.RollbackTransaction();
                NLog.LogManager.GetCurrentClassLogger().Error(p_EX);
                p_Msg = "重新發送密碼時發生異常";
                return false;
            }
        }
    }
}
