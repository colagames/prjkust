﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseLib;
using TVMLib.dl;
using BaseLib.Interface;


namespace TVMLib.Service
{
    public class TB_ABOUTUSService : BaseService, IService<TB_ABOUTUS>
    {

        private TB_ABOUTUSFactory _TB_ABOUTUSFactory;

        public TB_ABOUTUSService(BaseContext p_Context) : base(p_Context)
        {
            if (_TB_ABOUTUSFactory == null)
            {
                _TB_ABOUTUSFactory = new TB_ABOUTUSFactory(p_Context);
            }
        }

        public void Create(TB_ABOUTUS p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_ABOUTUSFactory.Insert(p_Datas);
            _BaseContext._Dbm.Commit();

        }

        public void Delete(TB_ABOUTUS p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_ABOUTUSFactory.DeleteByGuid(p_Datas.ABOUTUS_GUID);
            _BaseContext._Dbm.Commit();
        }

     

        public List<TB_ABOUTUS> Search()
        {
            return _TB_ABOUTUSFactory.Search();
        }

        public void Update(TB_ABOUTUS p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_ABOUTUSFactory.Update(p_Datas);
            _BaseContext._Dbm.Commit();
        }

        public TB_ABOUTUS SearchByGUID(string p_GUID)
        {
            return _TB_ABOUTUSFactory.SearchByGUID(p_GUID);
        }

    }
}
