﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseLib;
using TVMLib.dl;
using BaseLib.Interface;


namespace TVMLib.Service
{
    public class TB_QAService : BaseService, IService<TB_QA>
    {

        private TB_QAFactory _TB_QAFactory;

        public TB_QAService(BaseContext p_Context) : base(p_Context)
        {
            if (_TB_QAFactory == null)
            {
                _TB_QAFactory = new TB_QAFactory(p_Context);
            }
        }

        public void Create(TB_QA p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_QAFactory.Insert(p_Datas);
            _BaseContext._Dbm.Commit();

        }

        public List<TB_QA> SearchByCATEGORY(string p_CATEGORY)
        {
            return _TB_QAFactory.SearchByCATEGORY(p_CATEGORY);
        }

        public void Delete(TB_QA p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_QAFactory.DeleteByGuid(p_Datas.QA_GUID);
            _BaseContext._Dbm.Commit();
        }

        public List<TB_QA> SearchCategoryGroupByKeyWord(string p_KeyWord)
        {
            return _TB_QAFactory.SearchCategoryGroupByKeyWord(p_KeyWord);
        }

        public List<TB_QA> Search()
        {
            return _TB_QAFactory.Search();
        }

        public List<TB_QA> SearchByKeyword(string p_CATEGORY,string p_KeyWord)
        {
            return _TB_QAFactory.SearchByCatehoryAndKeyWord(p_CATEGORY, p_KeyWord);
        }

        public List<TB_QA> SearchCategoryGroup()
        {
            return _TB_QAFactory.SearchCategoryGroup();
        }




        public void Update(TB_QA p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_QAFactory.Update(p_Datas);
            _BaseContext._Dbm.Commit();
        }

        public TB_QA SearchByGUID(string p_GUID)
        {
            return _TB_QAFactory.SearchByGUID(p_GUID);
        }

    }
}
