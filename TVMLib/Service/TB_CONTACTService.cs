﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseLib;
using TVMLib.dl;
using BaseLib.Interface;


namespace TVMLib.Service
{
    public class TB_CONTACTService : BaseService, IService<TB_CONTACT>
    {

        private TB_CONTACTFactory _TB_CONTACTFactory;

        public TB_CONTACTService(BaseContext p_Context) : base(p_Context)
        {
            if (_TB_CONTACTFactory == null)
            {
                _TB_CONTACTFactory = new TB_CONTACTFactory(p_Context);
            }
        }

        public void Create(TB_CONTACT p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_CONTACTFactory.Insert(p_Datas);
            _BaseContext._Dbm.Commit();

        }

        public List<TB_CONTACT> SearchByKeyWord(string p_KeyWord)
        {
            return _TB_CONTACTFactory.SearchByKeyWord(p_KeyWord);
        }

        public void Delete(TB_CONTACT p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_CONTACTFactory.DeleteByGuid(p_Datas.CONTACT_GUID);
            _BaseContext._Dbm.Commit();
        }



        public List<TB_CONTACT> Search()
        {
            return _TB_CONTACTFactory.Search();
        }

        public void Update(TB_CONTACT p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_CONTACTFactory.Update(p_Datas);
            _BaseContext._Dbm.Commit();
        }

        public TB_CONTACT SearchByGUID(string p_GUID)
        {
            return _TB_CONTACTFactory.SearchByGUID(p_GUID);
        }

    }
}
