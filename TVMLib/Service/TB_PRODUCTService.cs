﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseLib;
using TVMLib.dl;
using BaseLib.Interface;
using TVMLib.dl.AdminProduct;
using TVMLib.dl.Product;

namespace TVMLib.Service
{
    public class TB_PRODUCTService : BaseService, IService<TB_PRODUCT>
    {

        private TB_PRODUCTFactory _TB_PRODUCTFactory;

        public TB_PRODUCTService(BaseContext p_Context) : base(p_Context)
        {
            if (_TB_PRODUCTFactory == null)
            {
                _TB_PRODUCTFactory = new TB_PRODUCTFactory(p_Context);
            }
        }

        public List<AdminProductVM> SearchAllAdminProductVMByGUID()
        {
            return _TB_PRODUCTFactory.SearchAllAdminProductVMByGUID();
        }

        public void Create(TB_PRODUCT p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_PRODUCTFactory.Insert(p_Datas);
            _BaseContext._Dbm.Commit();

        }

        public void Delete(TB_PRODUCT p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_PRODUCTFactory.Update(p_Datas);
            _BaseContext._Dbm.Commit();
        }

     

        public List<TB_PRODUCT> Search()
        {
            return _TB_PRODUCTFactory.Search();
        }

        public void Update(TB_PRODUCT p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_PRODUCTFactory.Update(p_Datas);
            _BaseContext._Dbm.Commit();
        }

      


        public TB_PRODUCT SearchByGUID(string p_GUID)
        {
            return _TB_PRODUCTFactory.SearchByGUID(p_GUID);
        }

        public AdminProductVM SearchAdminProductVMByGUID(string p_GUID)
        {
            return _TB_PRODUCTFactory.SearchAdminProductVMByGUID(p_GUID);
        }
        public List<TB_PRODUCT> SearchByCategoryGUID(string p_CategoryGUID,bool p_IsDeleteOrNot)
        {
            return _TB_PRODUCTFactory.SearchByp_CategoryGUID(p_CategoryGUID,p_IsDeleteOrNot);
        }
        public ProductVM SearchProductVMByGUID(string p_RRODUCT_GUID)
        {
            return _TB_PRODUCTFactory.SearchProductVMByGUID(p_RRODUCT_GUID);
        }
    }
}
