﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseLib;
using TVMLib.dl;
using BaseLib.Interface;


namespace TVMLib.Service
{
    public class TB_NEWSService : BaseService, IService<TB_NEWS>
    {

        private TB_NEWSFactory _TB_NEWSFactory;

        public TB_NEWSService(BaseContext p_Context) : base(p_Context)
        {
            if (_TB_NEWSFactory == null)
            {
                _TB_NEWSFactory = new TB_NEWSFactory(p_Context);
            }
        }

        public void Create(TB_NEWS p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_NEWSFactory.Insert(p_Datas);
            _BaseContext._Dbm.Commit();

        }

        public void Delete(TB_NEWS p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_NEWSFactory.DeleteByGuid(p_Datas.NEWS_GUID);
            _BaseContext._Dbm.Commit();
        }

     

        public List<TB_NEWS> Search()
        {
            return _TB_NEWSFactory.Search();
        }

        public void Update(TB_NEWS p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_NEWSFactory.Update(p_Datas);
            _BaseContext._Dbm.Commit();
        }

        public TB_NEWS SearchByGUID(string p_GUID)
        {
            return _TB_NEWSFactory.SearchByGUID(p_GUID);
        }

    }
}
