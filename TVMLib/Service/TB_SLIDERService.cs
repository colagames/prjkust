﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseLib;
using TVMLib.dl;
using BaseLib.Interface;

namespace TVMLib.Service
{
    public class TB_SLIDERService : BaseService, IService<TB_SLIDER>
    {

        private TB_SLIDERFactory _TB_SLIDERFactory;

        public TB_SLIDERService(BaseContext p_Context) : base(p_Context)
        {
            if (_TB_SLIDERFactory == null)
            {
                _TB_SLIDERFactory = new TB_SLIDERFactory(p_Context);
            }
        }

    

        public void Create(TB_SLIDER p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_SLIDERFactory.Insert(p_Datas);
            _BaseContext._Dbm.Commit();

        }

        public void Delete(TB_SLIDER p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_SLIDERFactory.Update(p_Datas);
            _BaseContext._Dbm.Commit();
        }

     

        public List<TB_SLIDER> Search()
        {
            return _TB_SLIDERFactory.Search();
        }

        public void Update(TB_SLIDER p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_SLIDERFactory.Update(p_Datas);
            _BaseContext._Dbm.Commit();
        }
        
    }
}
