﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseLib;
using TVMLib.dl;
using BaseLib.Interface;


namespace TVMLib.Service
{
    public class TB_ORDER_DETAILService : BaseService, IService<TB_ORDER_DETAIL>
    {

        private TB_ORDER_DETAILFactory _TB_ORDER_DETAILFactory;

        public TB_ORDER_DETAILService(BaseContext p_Context) : base(p_Context)
        {
            if (_TB_ORDER_DETAILFactory == null)
            {
                _TB_ORDER_DETAILFactory = new TB_ORDER_DETAILFactory(p_Context);
            }
        }

        public void Create(TB_ORDER_DETAIL p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_ORDER_DETAILFactory.Insert(p_Datas);
            _BaseContext._Dbm.Commit();

        }

      

        public void Delete(TB_ORDER_DETAIL p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_ORDER_DETAILFactory.DeleteByGuid(p_Datas.ORDER_GUID);
            _BaseContext._Dbm.Commit();
        }

     

        public List<TB_ORDER_DETAIL> Search()
        {
            return _TB_ORDER_DETAILFactory.Search();
        }

        public void Update(TB_ORDER_DETAIL p_Datas)
        {
            _BaseContext._Dbm.BeginTransaction();
            _TB_ORDER_DETAILFactory.Update(p_Datas);
            _BaseContext._Dbm.Commit();
        }

       

        public TB_ORDER_DETAIL SearchByGUID(string p_GUID)
        {
            return _TB_ORDER_DETAILFactory. SearchByGUID(p_GUID);
        }
       
        
    }
}
