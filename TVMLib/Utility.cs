﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TVMLib.common;
using TVMLib.dl;
using TVMLib.dl.Index;
using TVMLib.dl.Order;

namespace TVMLib
{
    public static class Utility
    {
        //public static List<SelectListItem> GenFARMLAND_USER_GUID_List(List<TB_USER> p_Datas, string p_DefaultValue = "")
        //{
        //    var l_SelectList = new List<SelectListItem>();

        //    foreach (var item in p_Datas)
        //    {
        //        l_SelectList.Add(new SelectListItem { Text = item.USER_NAME, Value = item.USER_GUID });
        //    }

        //    if (!string.IsNullOrEmpty(p_DefaultValue))
        //    {
        //        l_SelectList.Where(q => q.Value == p_DefaultValue).First().Selected = true;
        //    }

        //    return l_SelectList;


        //}



        /// <summary>
        /// 取得Client端IP
        /// </summary>
        /// <returns></returns>
        public static string GetClientIP()
        {
            try
            {
                //判斷Client端是否有設定代理伺服器
                if (HttpContext.Current.Request.ServerVariables["HTTP_VIA"] == null)
                    return HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                else
                    return HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            catch (Exception p_EX)
            {
                NLog.LogManager.GetCurrentClassLogger().Error(p_EX);
                return string.Empty;
            }
        }

        /// <summary>
        /// 產生前台登入資訊
        /// </summary>
        public static void GenLoginUserCookieAndSessionInfo(TB_USER p_TB_USER)
        {
            string l_SeesionID = HttpContext.Current.Session.SessionID;
            HttpContext.Current.Session.Add(l_SeesionID, p_TB_USER);
            CreateUserCookie(l_SeesionID);
        }

        /// <summary>
        /// 新增cookie
        /// </summary>
        /// <param name="p_SessionID"></param>
        /// <param name="p_Cookie_Data"></param>
        /// <returns></returns>
        private static void CreateUserCookie(string p_SessionID, string p_Cookie_Data = "")
        {
            FormsAuthenticationTicket l_Ticket = new FormsAuthenticationTicket(1,
                  p_SessionID,//使用者GUID
                  DateTime.Now,//核發日期
                  DateTime.Now.AddMinutes(30),//到期日期 30分鐘 
                  false,//永續性
                  p_Cookie_Data,//使用者定義的資料
                  FormsAuthentication.FormsCookiePath);
            string l_EncTicket = FormsAuthentication.Encrypt(l_Ticket);
            var l_Cookie = new HttpCookie(FormsAuthentication.FormsCookieName, l_EncTicket);
            l_Cookie.HttpOnly = true;
            HttpContext.Current.Response.Cookies.Add(l_Cookie);
        }

        /// <summary>
        /// 新增cookie
        /// </summary>
        /// <param name="p_SessionID"></param>
        /// <param name="p_Cookie_Data"></param>
        /// <returns></returns>
        //private static void CreateShopCookie(string p_SessionID, string p_Cookie_Data = "")
        //{
        //    FormsAuthenticationTicket l_Ticket = new FormsAuthenticationTicket(1,
        //           p_SessionID,//使用者GUID
        //           DateTime.Now,//核發日期
        //           DateTime.Now.AddMinutes(30),//到期日期 30分鐘 
        //           false,//永續性
        //           p_Cookie_Data,//使用者定義的資料
        //           FormsAuthentication.FormsCookiePath);
        //    string l_EncTicket = FormsAuthentication.Encrypt(l_Ticket);
        //    var l_Cookie = new HttpCookie(UserData.USER_GUID, l_EncTicket);
        //    l_Cookie.HttpOnly = true;
        //    HttpContext.Current.Response.Cookies.Add(l_Cookie);
        //}



        internal static string GenRandomString(int p_Min, int p_Max)
        {
            System.Text.StringBuilder l_SB = new System.Text.StringBuilder();
            char[] l_Chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            int l_Length = RandomGenerator.Next(p_Min, p_Max);
            for (int i = 0; i < l_Length; i++)
            {
                l_SB.Append(l_Chars[RandomGenerator.Next(l_Chars.Length - 1)]);
            }
            return l_SB.ToString();
        }

        public static TB_USER UserData
        {
            get
            {

                return LoginAndGetData() ?? new TB_USER();
                //return LoginAndGetData() ?? new TB_USER() { USER_GUID = "admin", NAME = "系統管理者" };
                //#if DEBUG

                //#else
                //                 return LoginAndGetData() ?? new TB_USER();
                //#endif
            }
        }


        public static OrderAndDetailVM OrderAndDetail
        {
            get
            {
                return GetOrderDetailData() ?? null;
            }
        }

        private static OrderAndDetailVM GetOrderDetailData()
        {
            if (!string.IsNullOrEmpty(UserData.USER_GUID))
            {

                if (HttpContext.Current.Session[UserData.USER_GUID] != null)
                {
                    return HttpContext.Current.Session[UserData.USER_GUID] as OrderAndDetailVM;
                }

                return null;
            }

            return null;
        }

        public static TB_USER LoginAndGetData()
        {
            HttpCookie l_AuthCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (l_AuthCookie != null)
            {
                l_AuthCookie.HttpOnly = true;
                FormsAuthenticationTicket l_AuthTicket = FormsAuthentication.Decrypt(l_AuthCookie.Value);
                if (HttpContext.Current.Session[l_AuthTicket.Name] != null)
                {
                    return HttpContext.Current.Session[l_AuthTicket.Name] as TB_USER;
                }
            }
            return null;
        }


        /// <summary>
        /// 儲存檔案(單檔)
        /// </summary>
        /// <param name="p_File">HttpPostedFileBase 物件</param>
        /// <param name="l_FullFilePath">完整路徑</param>
        /// <param name="p_PreName">前置檔案名稱</param>
        /// <returns></returns>
        public static bool SaveFile(HttpPostedFileBase p_File, out string l_FullFilePath, string p_OldFile, string p_PreName = null)
        {
            DateTime l_DtNow = DateTime.Now;
            l_FullFilePath = null;
            try
            {
                string l_FileName = string.Format("{0}{1}", (p_PreName != null ? p_PreName + "_" : ""), l_DtNow.ToString("yyyyMMddHHmmssffff"));
                string l_FileExtName = Path.GetExtension(p_File.FileName).ToLower();
                string l_BasePath = HttpContext.Current.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["FilesDirectory"].ToString().ToUpper()) + l_DtNow.ToString("yyyy") + "\\" + l_DtNow.ToString("MM") + "\\" + l_DtNow.ToString("dd") + "\\";
                if (!Directory.Exists(l_BasePath))
                    Directory.CreateDirectory(l_BasePath);
                l_FullFilePath = Path.Combine(l_BasePath, l_FileName + l_FileExtName);
                Thread.Sleep(100);
                p_File.SaveAs(l_FullFilePath);
                if (File.Exists(p_OldFile))
                {
                    File.Delete(p_OldFile);
                }
            }
            catch (Exception p_EX)
            {
                NLog.LogManager.GetCurrentClassLogger().Error(p_EX);
                return false;
            }
            return true;
        }

        public static List<SelectListItem> GenProductCategoryDDL(List<TB_PRODUCT_CATEGORY> p_Datas, string p_CATEGORY_GUID)
        {
            var l_SelectList = new List<SelectListItem>();

            foreach (var item in p_Datas)
            {
                l_SelectList.Add(new SelectListItem { Text = item.NAME, Value = item.CATEGORY_GUID, Selected = p_CATEGORY_GUID == "" ? false : (item.CATEGORY_GUID == p_CATEGORY_GUID ? true : false) });
            }
            return l_SelectList;
        }


        public static List<MenuProductCategoryVM> getProductMenu(PageContext p_PageContext)
        {


            List<MenuProductCategoryVM> menuProductCategoryVMs = new List<MenuProductCategoryVM>();
            List<TB_PRODUCT_CATEGORY> l_TB_PRODUCT_CATEGORY = p_PageContext.ServiceManager.TB_PRODUCT_CATEGORYService.Search();
            var requestContext = HttpContext.Current.Request.RequestContext;

            foreach (var item in l_TB_PRODUCT_CATEGORY)
            {
                MenuProductCategoryVM l_Data = new MenuProductCategoryVM();
                l_Data.CATEGORY_GUID = item.CATEGORY_GUID;
                l_Data.NAME = item.NAME;
                l_Data.l_MenuProductVM = new List<MenuProductVM>();
                List<TB_PRODUCT> l_TB_PRODUCT = p_PageContext.ServiceManager.TB_PRODUCTService.SearchByCategoryGUID(l_Data.CATEGORY_GUID, false);


                foreach (var itemDetail in l_TB_PRODUCT)
                {
                    MenuProductVM l_Detail = new MenuProductVM();
                    l_Detail.PRODUCT_GUID = itemDetail.PRODUCT_GUID;
                    l_Detail.NAME = itemDetail.NAME;
                    l_Detail.URL = new UrlHelper(requestContext).Action("ProductDetail", "Product", new { p_RRODUCT_GUID = itemDetail.PRODUCT_GUID }); ;
                    l_Data.l_MenuProductVM.Add(l_Detail);
                }


                menuProductCategoryVMs.Add(l_Data);
            }

            return menuProductCategoryVMs;
        }

        public static List<HotProductVM> getHotProduct(PageContext p_PageContext)
        {
            List<HotProductVM> l_HotProductVM = p_PageContext.ServiceManager.TB_PRODUCT_CATEGORYService.SearchHotProductTop9Category();
            return l_HotProductVM;
        }

        /// <summary>
        /// 產生後台登入資訊
        /// </summary>
        public static void GenUserInfoCookieAndSession(TB_USER p_TB_USER)
        {
            string l_SeesionID = HttpContext.Current.Session.SessionID;
            HttpContext.Current.Session.Add(l_SeesionID, p_TB_USER);
            CreateUserCookie(l_SeesionID);
        }

        /// <summary>
        /// 產生購物車購買資訊
        /// </summary>
        public static void GenOrderAndOrderDetailCookieAndSessionInfo(OrderAndDetailVM p_OrderAndDetailVM)
        {
            string l_SeesionID = UserData.USER_GUID;
            HttpContext.Current.Session.Add(l_SeesionID, p_OrderAndDetailVM);
            //CreateShopCookie(l_SeesionID);
        }


        /// <summary>
        /// 產生訂單編號
        /// </summary>
        public static string getOrderNo(PageContext p_PageContext)
        {
            CNumberGen l_CNumberGen = new CNumberGen();
            return l_CNumberGen.getOrderNo(p_PageContext, 1);
        }
    }
}
