﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVMLib.Enum
{
    public class SUPERVISION_TYPE
    {
        /// <summary>
        /// 自辦
        /// </summary>
        public const string 自辦 = "A";
        /// <summary>
        /// 委外
        /// </summary>
        public const string 委外 = "B";
     
    }
}
