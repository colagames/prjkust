﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVMLib.Enum
{
    public class PLAN_TYPE
    {
        /// <summary>
        /// 自辦未達1000
        /// </summary>
        public const string 自辦1000 = "SELF_1000";
        /// <summary>
        /// 委外未達1000
        /// </summary>
        public const string 委外1000 = "OUT_1000";
        /// <summary>
        /// 委外1000 - 5000
        /// </summary>
        public const string 委外5000 = "OUT_5000";
    }
}
