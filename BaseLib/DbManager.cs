using System;
using System.Collections;
using System.Configuration;
using System.Data.OleDb;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Reflection;
using System.Text;


namespace BaseLib
{

    /// <summary>
    /// 用來定義所有資料庫的操作，例如開啟、關閉等
    /// Singleton Pattern 可參考 https://dotblogs.com.tw/pin0513/2010/03/08/13931
    /// </summary>
    public class DatabaseManager
    {
        //資料庫連線物件
        private SqlConnection _db = null;
        //塞值用字典(key,value)方式
        private Dictionary<string, object> _mydic;

        public DatabaseManager(string p_connectString)
        {
            this._db = new SqlConnection(p_connectString);
        }

        private void CloseConnection()
        {
            this._db.Close();
        }
        private void openConnection()
        {
            if (this._db.State == ConnectionState.Closed)
            {

                this._db.Open();
            }

        }
        public void AddParameter(string p_name, object p_value)
        {
            //如果 _mydic 尚未實體化，則實體化 (Singleton) 
            if (_mydic == null)
            {
                _mydic = new Dictionary<string, object>();
            }
            _mydic.Add(p_name, p_value);
        }
        public void AddParameter<TResult>(Object p_className)
            where TResult : class
        {

            TResult entity = (TResult)p_className;
            PropertyInfo[] property = entity.GetType().GetProperties();
            if (_mydic == null)
            {
                _mydic = new Dictionary<string, object>();
            }

            foreach (PropertyInfo p in property)
            {
                Type propType = p.PropertyType;
                if (propType.Name != "HttpPostedFileBase")
                {
                    if (p.GetValue(entity, null) != null)
                    {
                        _mydic.Add("@" + p.Name, p.GetValue(entity, null));
                    }
                    else
                    {

                        TypeConverters.ITypeConverter typeConverter = TypeConverters.TypeConverterFactory.GetConvertType(propType);
                        object l_Value = typeConverter.Convert(p.GetValue(entity, null));
                        if (l_Value as DateTime? == System.Convert.ToDateTime("1 / 1 / 1753 12:00:00"))
                        {
                            p.SetValue(entity, l_Value, null);
                        }
                        else if (l_Value as DateTime? == DateTime.MinValue) {
                            p.SetValue(entity, System.Convert.ToDateTime("1 / 1 / 1753 12:00:00"), null);
                        }
                        else
                        {

                            p.SetValue(entity, Convert.ChangeType(l_Value, propType), null);
                        }

                        _mydic.Add("@" + p.Name, p.GetValue(entity, null));
                    }
                }
            }

        }



        private SqlTransaction _tran;
        private SqlTransaction _cmd;
        public void BeginTransaction()
        {
            openConnection();
            this._tran = this._db.BeginTransaction();
        }
        public void Commit()
        {
            try
            {
                this._tran.Commit();
            }
            catch (Exception ex)
            {
                this._tran.Rollback();
                string l_strError = "Database Commit Error，Error Message：/n" + ex.Message;
                //throw new Exception(l_strError);
            }
            finally
            {
                CloseConnection();
                this._tran = null;
            }
        }
        public void RollbackTransaction()
        {
            this._tran.Rollback();
            CloseConnection();
            this._tran = null;
        }
        public void EexcuteSqlNonquery(string p_statement)
        {
            ExecuteCmd(p_statement, _mydic);
        }

        private void ExecuteCmd(string p_statement, Dictionary<string, object> p_parameters)
        {
            SqlCommand cmd = new SqlCommand(p_statement, this._db);

            cmd.CommandText = p_statement;
            cmd.CommandType = CommandType.Text;

            if (p_parameters != null)
            {
                foreach (KeyValuePair<string, object> param in p_parameters)
                    cmd.Parameters.AddWithValue(param.Key, param.Value);
            }

            openConnection();

            try
            {
                if (this._tran != null)
                {
                    cmd.Transaction = this._tran;
                }
                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                CloseConnection();
                string l_strError = "Database ExecuteNonQuery Error，Error Message：/n" + ex.Message;
                throw new Exception(l_strError);
            }
            finally
            {
                if (this._tran == null)
                {
                    CloseConnection();
                }
                cmd.Dispose();

            }
            _mydic = null;
        }

        private IDataReader ExecuteQuery(string p_statement, Dictionary<string, object> p_parameters)
        {
            SqlCommand cmd = new SqlCommand(p_statement, this._db);
            SqlDataReader reader = null;

            cmd.CommandText = p_statement;
            cmd.CommandType = CommandType.Text;

            if (p_parameters != null)
            {
                foreach (KeyValuePair<string, object> l_param in p_parameters)
                    cmd.Parameters.AddWithValue(l_param.Key, l_param.Value ?? DBNull.Value);
            }

            openConnection();

            if (this._tran != null)
            {
                cmd.Transaction = this._tran;
            }
            reader = cmd.ExecuteReader(CommandBehavior.Default | CommandBehavior.SingleResult);
            cmd.Dispose();
            _mydic = null;
            return reader;
        }

        public List<TResult> ExecuteReader<TResult>(string p_sql)
            where TResult : class
        {
            IDataReader reader = null;
            try
            {
                openConnection();
                reader = ExecuteQuery(p_sql, _mydic);
                List<TResult> l_list = new List<TResult>();
                while (reader.Read())
                {
                    TResult l_entity = Activator.CreateInstance(typeof(TResult)) as TResult;
                    //利用 Reflection 的機制將TCollection與查詢資料做同步
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        PropertyInfo property = l_entity.GetType().GetProperty(reader.GetName(i));

                        if (property == null || reader.GetValue(i) == null || reader.GetValue(i) == DBNull.Value)
                        {
                            continue;
                        }

                        Type propType = property.PropertyType;
                        TypeConverters.ITypeConverter typeConverter = TypeConverters.TypeConverterFactory.GetConvertType(propType);

                        var value = Convert.ChangeType(typeConverter.Convert(reader.GetValue(i)), Nullable.GetUnderlyingType(propType) ?? propType);

                        property.SetValue(l_entity, value, null);

                    }

                    l_list.Add(l_entity);
                }

                return l_list;
            }
            catch (Exception ex)
            {

                string l_strError = "Database ExecuteQuery Error，Error Message：/n" + ex.Message;
                //throw new Exception(l_strError);
                List<TResult> l_list = new List<TResult>();
                return l_list;

            }
            finally
            {
                if (this._tran == null)
                {
                    CloseConnection();
                }
                reader.Close();
                //this._db.Close();

            }

            _mydic = null;
        }
    }
}
