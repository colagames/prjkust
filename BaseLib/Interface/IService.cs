﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace BaseLib.Interface
{
    public interface IService<T>
        where T : class
    {
        void Create(T p_Datas);
        List<T> Search();
        void Update(T p_Datas);
        void Delete(T p_Datas);
    }
}
