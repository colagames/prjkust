﻿using BaseLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseLib
{
    public class BaseService
   {
        protected BaseContext _BaseContext;

        public BaseService(BaseContext p_Context)
        {
            _BaseContext = p_Context;
        }
    }
}
