﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BaseLib.TypeConverters
{
    public class LongConverter : ITypeConverter
    {
        public object Convert(object ValueToConvert)
        {
            if (ValueToConvert == null || ValueToConvert == DBNull.Value)
                return (long?)null;

            return System.Convert.ToInt64(ValueToConvert);
        }
    }
}
