﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BaseLib.TypeConverters
{
    public class TypeConverterFactory
    {
        public static ITypeConverter GetConvertType<T>()
        {
            return GetConvertType(typeof(T));
       
        }

        public static ITypeConverter GetConvertType(Type p_type)
        {
            if (p_type == typeof(string))
                return (new StringConverter());
            if (p_type == typeof(int) || p_type == typeof(Nullable<int>))
                return (new IntegerConverter());
            if (p_type == typeof(long) || p_type == typeof(Nullable<long>))
                return (new LongConverter());
            if (p_type == typeof(short) || p_type == typeof(Nullable<short>))
                return (new ShortConverter());
            if (p_type == typeof(float) || p_type == typeof(Nullable<float>))
                return (new FloatConverter());
            if (p_type == typeof(double) || p_type == typeof(Nullable<double>))
                return (new DoubleConverter());
            if (p_type == typeof(decimal) || p_type == typeof(Nullable<decimal>))
                return (new DecimalConverter());
            if (p_type == typeof(bool) || p_type == typeof(Nullable<bool>))
                return (new BooleanConverter());
            if (p_type == typeof(char) || p_type == typeof(Nullable<char>))
                return (new CharConverter());
            if (p_type == typeof(DateTime) || p_type == typeof(Nullable<DateTime>))
                return (new DateTimeConverter());
            if (p_type == typeof(DateTimeOffset) || p_type == typeof(Nullable<DateTimeOffset>))
                return (new DateTimeOffsetConverter());
            if (p_type == typeof(Nullable<Guid>))
                return (new UniqueidentifierConverter());

            return null;
        }
    }
}
