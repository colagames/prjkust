﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BaseLib.TypeConverters
{
    public class ShortConverter : ITypeConverter
    {
        public object Convert(object ValueToConvert)
        {
            if (ValueToConvert == null || ValueToConvert == DBNull.Value)
                return (short?)null;

            return System.Convert.ToInt16(ValueToConvert);
        }
    }
}
