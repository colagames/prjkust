﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BaseLib.TypeConverters
{
    public class DoubleConverter : ITypeConverter
    {
        public object Convert(object ValueToConvert)
        {
            if (ValueToConvert == null || ValueToConvert == DBNull.Value)
                return (decimal?)null;

            return System.Convert.ToDouble(ValueToConvert);
        }
    }
}
