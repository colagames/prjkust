﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BaseLib.TypeConverters
{
    public class FloatConverter : ITypeConverter
    {
        public object Convert(object ValueToConvert)
        {
            if (ValueToConvert == null || ValueToConvert == DBNull.Value)
                return (float?)null;

            return System.Convert.ToSingle(ValueToConvert);
        }
    }
}
