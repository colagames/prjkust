﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BaseLib.TypeConverters
{
    public interface ITypeConverter
    {
        object Convert(object ValueToConvert);
    }
}
