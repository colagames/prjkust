﻿using System;
using System.Collections.Generic;
using System.Text;
using BaseLib.TypeConverters;

namespace BaseLib.TypeConverters
{
    public class DateTimeConverter : ITypeConverter
    {
        public object Convert(object ValueToConvert)
        {
            if (ValueToConvert == null || ValueToConvert == DBNull.Value)
                return System.Convert.ToDateTime("1 / 1 / 1753 12:00:00");

            return System.Convert.ToDateTime(ValueToConvert);
        }
    }
}
