﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseLib.TypeConverters
{
    public class UniqueidentifierConverter : ITypeConverter
    {
        public object Convert(object ValueToConvert)
        {
            if (ValueToConvert == null || ValueToConvert == DBNull.Value)
                return DBNull.Value;

            return SqlDbType.UniqueIdentifier;
        }
    }
}
