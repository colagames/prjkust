﻿using System;
using System.Collections.Generic;
using System.Text;
using BaseLib.TypeConverters;

namespace BaseLib.TypeConverters
{
    public class DateTimeOffsetConverter : ITypeConverter
    {
        public object Convert(object ValueToConvert)
        {
            if (ValueToConvert == null || ValueToConvert == DBNull.Value)
                return (DateTimeOffset?)null;

            return (DateTimeOffset)ValueToConvert;
        }
    }
}
